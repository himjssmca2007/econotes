package com.econote;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.econote.utils.Constants;
import com.skd.androidrecording.audio.AudioPlaybackManager;
import com.skd.androidrecording.video.PlaybackHandler;
import com.skd.androidrecording.visualizer.VisualizerView;
import com.skd.androidrecording.visualizer.renderer.BarGraphRenderer;

public class AudioPlaybackListActivity extends AppCompatActivity {
	public static String FileNameArg = "arg_filename";
	private static String fileName = null;
	private VisualizerView visualizerView;
	private AudioPlaybackManager playbackManager;
	private TextView record_again;
	private Context context;
	private Toolbar toolbar;
	private TextView messageBubble,hazardBubble,reminderBubble,activityBubble;

	private PlaybackHandler playbackHandler = new PlaybackHandler() {
		@Override
		public void onPreparePlayback() {
			runOnUiThread (new Runnable() {
		    	public void run() {
		    		playbackManager.showMediaController();
		    	}
		    });
		}
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.audio_play_list);
		context=this;
		Intent i = getIntent();
		if ((i != null) && (i.getExtras() != null)) {
			fileName = i.getExtras().getString(FileNameArg);
		}
		toolbar=(Toolbar)findViewById(R.id.toolbar);
		prepareActionBar(toolbar);
		visualizerView = (VisualizerView) findViewById(R.id.visualizerView);
		setupVisualizer();
		playbackManager = new AudioPlaybackManager(this,visualizerView,playbackHandler);
		playbackManager.setupPlayback(fileName);
		record_again=(TextView)findViewById(R.id.record_again);
		record_again.setOnClickListener(mClickListener);
		LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationReceiver, new IntentFilter(Constants.INTENT_FILTER_NOTIFICATION));
	}


	private void prepareActionBar(Toolbar toolbar)
	{
		toolbar.setTitle("");
		setSupportActionBar(toolbar);
		ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setHomeButtonEnabled(true);
	}

	View.OnClickListener mClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {

			switch (v.getId())
			{
				case R.id.record_again:
					onBackPressed();
					break;
			}

		}
	};

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		playbackManager.showMediaController();
		return false;
	}
	
	@Override
	protected void onPause()
	{
		super.onPause();
		playbackManager.pause();
		playbackManager.hideMediaController();
	}
	
	@Override
	protected void onDestroy() {
		playbackManager.dispose();
		playbackHandler = null;
		LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationReceiver);
		super.onDestroy();
	}
	
	private void setupVisualizer() {
		Paint paint = new Paint();
        paint.setStrokeWidth(5f);
        paint.setAntiAlias(true);
        paint.setColor(Color.argb(200, 227, 69, 53));
        BarGraphRenderer barGraphRendererBottom = new BarGraphRenderer(2, paint, false);
        visualizerView.addRenderer(barGraphRendererBottom);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_main, menu);
		return super.onCreateOptionsMenu(menu);
	}


	@Override
	public boolean onPrepareOptionsMenu(Menu menu)
	{
		MenuItem item = menu.findItem(R.id.action_chat);
		RelativeLayout messageView = (RelativeLayout) item.getActionView();
		messageBubble = (TextView) messageView.findViewById(R.id.notification_count);
		messageView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent i=new Intent(context,NotificationActivity.class);
				startActivity(i);
			}
		});

		item = menu.findItem(R.id.action_haz);
		RelativeLayout hazardView = (RelativeLayout) item.getActionView();
		hazardBubble = (TextView) hazardView.findViewById(R.id.notification_count);
		hazardView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent i=new Intent(context,NotificationActivity.class);
				startActivity(i);
			}
		});

		item = menu.findItem(R.id.action_alarm);
		RelativeLayout reminderView = (RelativeLayout) item.getActionView();
		reminderBubble = (TextView) reminderView.findViewById(R.id.notification_count);
		reminderView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent i=new Intent(context,NotificationActivity.class);
				startActivity(i);
			}
		});

		item = menu.findItem(R.id.action_notif);
		RelativeLayout activityView = (RelativeLayout) item.getActionView();
		activityBubble = (TextView) activityView.findViewById(R.id.notification_count);
		activityView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent i=new Intent(context,NotificationActivity.class);
				startActivity(i);
			}
		});
		showNotificationCount();
		return true;
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				onBackPressed();
				return true;
			case R.id.action_map:
			{
				Intent i=new Intent(AudioPlaybackListActivity.this,MainActivity.class);
				i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(i);
				finish();
				return true;
			}
		}
		return super.onOptionsItemSelected(item);
	}

	private BroadcastReceiver mNotificationReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			// Get extra data included in the Intent
			showNotificationCount();
		}
	};

	private void showNotificationCount()
	{
		if (messageBubble != null)
		{
			if(Constants.notificationMap!=null && Constants.notificationMap.get(Constants.KEY_MESSAGE)!=null && Constants.notificationMap.get(Constants.KEY_MESSAGE).size()>0)
			{
				messageBubble.setVisibility(View.VISIBLE);
				messageBubble.setText(""+Constants.notificationMap.get(Constants.KEY_MESSAGE).size());
			}
			else
				messageBubble.setVisibility(View.GONE);
		}
		if (hazardBubble != null)
		{
			if(Constants.notificationMap!=null && Constants.notificationMap.get(Constants.KEY_HAZARD)!=null && Constants.notificationMap.get(Constants.KEY_HAZARD).size()>0)
			{
				hazardBubble.setVisibility(View.VISIBLE);
				hazardBubble.setText(""+Constants.notificationMap.get(Constants.KEY_HAZARD).size());
			}
			else
				hazardBubble.setVisibility(View.GONE);
		}
		if (reminderBubble != null)
		{
			if(Constants.notificationMap!=null && Constants.notificationMap.get(Constants.KEY_REMINDER)!=null && Constants.notificationMap.get(Constants.KEY_REMINDER).size()>0)
			{
				reminderBubble.setVisibility(View.VISIBLE);
				reminderBubble.setText(""+Constants.notificationMap.get(Constants.KEY_REMINDER).size());
			}
			else
				reminderBubble.setVisibility(View.GONE);
		}
		if (activityBubble != null)
		{
			if(Constants.notificationMap!=null && Constants.notificationMap.get(Constants.KEY_ACTIVITY)!=null && Constants.notificationMap.get(Constants.KEY_ACTIVITY).size()>0)
			{
				activityBubble.setVisibility(View.VISIBLE);
				activityBubble.setText(""+Constants.notificationMap.get(Constants.KEY_ACTIVITY).size());
			}
			else
				activityBubble.setVisibility(View.GONE);
		}
	}




	@Override
	public void onBackPressed()
	{
		finish();
	}
}
