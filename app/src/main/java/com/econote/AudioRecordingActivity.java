package com.econote;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.econote.utils.Constants;
import com.econote.utils.GlobalAlertDialogs;
import com.econote.utils.GlobalTracker;
import com.econote.utils.StorageUtils;
import com.skd.androidrecording.audio.AudioRecordingHandler;
import com.skd.androidrecording.audio.AudioRecordingThread;
import com.skd.androidrecording.visualizer.VisualizerView;
import com.skd.androidrecording.visualizer.renderer.BarGraphRenderer;

public class AudioRecordingActivity extends AppCompatActivity
{
	private static String fileName = null;
	private TextView recordBtn, playBtn;
	private VisualizerView visualizerView;
	private AudioRecordingThread recordingThread;
	private boolean startRecording = true;
	private Context context;
	private Toolbar toolbar;
	private TextView messageBubble,hazardBubble,reminderBubble,activityBubble;
	private CountDownTimer timer;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.audio_rec);
		context=this;
		if (!StorageUtils.checkExternalStorageAvailable()) {
			GlobalAlertDialogs.createAlertSingle(AudioRecordingActivity.this,getString(R.string.noExtStorageAvailable),"OK",true);
			return;
		}
		toolbar=(Toolbar)findViewById(R.id.toolbar);
		prepareActionBar(toolbar);
		GlobalTracker.setScreen("AudioRecordingScreen");
		fileName = StorageUtils.getFileName(true);
		recordBtn = (TextView) findViewById(R.id.record);
		recordBtn.setOnClickListener(mClickListener);
		playBtn = (TextView) findViewById(R.id.play);
		playBtn.setOnClickListener(mClickListener);
		visualizerView = (VisualizerView) findViewById(R.id.visualizerView);
		setupVisualizer();
		LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationReceiver, new IntentFilter(Constants.INTENT_FILTER_NOTIFICATION));
	}


	private void prepareActionBar(Toolbar toolbar)
	{
		toolbar.setTitle("");
		setSupportActionBar(toolbar);
		ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setHomeButtonEnabled(true);
	}

	OnClickListener mClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {

			switch (v.getId())
			{
				case R.id.record:
					record();
					break;
				case R.id.play :
					play();
					break;
			}

		}
	};

	@Override
	protected void onPause() {
		super.onPause();
		recordStop();
	}
	
	@Override
	protected void onDestroy() {
		recordStop();
		releaseVisualizer();
		LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationReceiver);
		super.onDestroy();
	}
	
	private void setupVisualizer() {
		Paint paint = new Paint();
        paint.setStrokeWidth(5f);
        paint.setAntiAlias(true);
        paint.setColor(Color.argb(200, 227, 69, 53));
        BarGraphRenderer barGraphRendererBottom = new BarGraphRenderer(2,paint,false);
        visualizerView.addRenderer(barGraphRendererBottom);
	}
	
	private void releaseVisualizer() {
		visualizerView.release();
		visualizerView = null;
	}
	
	private void record() {
        if (startRecording)
		{
        	recordStart();
        }
        else
		{
        	recordStop();
        }
	}
	
	private void recordStart() {
		startRecording();
    	startRecording = false;
    	recordBtn.setText(R.string.stopRecordBtn);
    	playBtn.setEnabled(false);
		timer=new CountDownTimer(Constants.MAX_AUDIO_TIME,Constants.MAX_AUDIO_TIME) {

			public void onTick(long millisUntilFinished)
			{

			}

			public void onFinish()
			{
				recordStop();
			}
		}.start();
	}
	
	private void recordStop() {
		stopRecording();
		startRecording = true;
    	recordBtn.setText(R.string.recordBtn);
    	playBtn.setEnabled(true);
		if(timer!=null)
		{
			timer.cancel();
			timer=null;
		}
	}
	
	private void startRecording() {
	    recordingThread = new AudioRecordingThread(fileName, new AudioRecordingHandler() {
			@Override
			public void onFftDataCapture(final byte[] bytes) {
				runOnUiThread(new Runnable() {
					public void run() {
						if (visualizerView != null) {
							visualizerView.updateVisualizerFFT(bytes);
						}
					}
				});
			}

			@Override
			public void onRecordSuccess() {}

			@Override
			public void onRecordingError() {
				runOnUiThread(new Runnable() {
					public void run() {
						recordStop();
						GlobalAlertDialogs.createAlertSingle(AudioRecordingActivity.this, getString(R.string.recordingError), "OK", true);
					}
				});
			}

			@Override
			public void onRecordSaveError() {
				runOnUiThread(new Runnable() {
					public void run() {
						recordStop();
						GlobalAlertDialogs.createAlertSingle(AudioRecordingActivity.this, getString(R.string.saveRecordError), "OK", true);
					}
				});
			}
		});
	    recordingThread.start();
    }
    
    private void stopRecording() {
    	if (recordingThread != null) {
    		recordingThread.stopRecording();
    		recordingThread = null;
	    }
    }

    private void play()
	{
		Intent i = new Intent(AudioRecordingActivity.this, AudioPlaybackActivity.class);
		i.putExtra(AudioPlaybackActivity.FileNameArg,fileName);
		startActivityForResult(i,Constants.REQUEST_RECORD);
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_main, menu);
		return super.onCreateOptionsMenu(menu);
	}


	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {

		MenuItem item = menu.findItem(R.id.action_chat);
		RelativeLayout messageView = (RelativeLayout) item.getActionView();
		messageBubble = (TextView) messageView.findViewById(R.id.notification_count);
		messageView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent i=new Intent(context,NotificationActivity.class);
				startActivity(i);
			}
		});

		item = menu.findItem(R.id.action_haz);
		RelativeLayout hazardView = (RelativeLayout) item.getActionView();
		hazardBubble = (TextView) hazardView.findViewById(R.id.notification_count);
		hazardView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent i=new Intent(context,NotificationActivity.class);
				startActivity(i);
			}
		});

		item = menu.findItem(R.id.action_alarm);
		RelativeLayout reminderView = (RelativeLayout) item.getActionView();
		reminderBubble = (TextView) reminderView.findViewById(R.id.notification_count);
		reminderView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent i=new Intent(context,NotificationActivity.class);
				startActivity(i);
			}
		});

		item = menu.findItem(R.id.action_notif);
		RelativeLayout activityView = (RelativeLayout) item.getActionView();
		activityBubble = (TextView) activityView.findViewById(R.id.notification_count);
		activityView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent i=new Intent(context,NotificationActivity.class);
				startActivity(i);
			}
		});

		showNotificationCount();
		return true;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				onBackPressed();
				return true;
			case R.id.action_map:
			{
				Intent i=new Intent(AudioRecordingActivity.this,MainActivity.class);
				i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(i);
				finish();
				return true;
			}
		}
		return super.onOptionsItemSelected(item);
	}

	private BroadcastReceiver mNotificationReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			// Get extra data included in the Intent
			showNotificationCount();
		}
	};

	private void showNotificationCount()
	{
		if (messageBubble != null)
		{
			if(Constants.notificationMap!=null && Constants.notificationMap.get(Constants.KEY_MESSAGE)!=null && Constants.notificationMap.get(Constants.KEY_MESSAGE).size()>0)
			{
				messageBubble.setVisibility(View.VISIBLE);
				messageBubble.setText(""+Constants.notificationMap.get(Constants.KEY_MESSAGE).size());
			}
			else
				messageBubble.setVisibility(View.GONE);
		}
		if (hazardBubble != null)
		{
			if(Constants.notificationMap!=null && Constants.notificationMap.get(Constants.KEY_HAZARD)!=null && Constants.notificationMap.get(Constants.KEY_HAZARD).size()>0)
			{
				hazardBubble.setVisibility(View.VISIBLE);
				hazardBubble.setText(""+Constants.notificationMap.get(Constants.KEY_HAZARD).size());
			}
			else
				hazardBubble.setVisibility(View.GONE);
		}
		if (reminderBubble != null)
		{
			if(Constants.notificationMap!=null && Constants.notificationMap.get(Constants.KEY_REMINDER)!=null && Constants.notificationMap.get(Constants.KEY_REMINDER).size()>0)
			{
				reminderBubble.setVisibility(View.VISIBLE);
				reminderBubble.setText(""+Constants.notificationMap.get(Constants.KEY_REMINDER).size());
			}
			else
				reminderBubble.setVisibility(View.GONE);
		}
		if (activityBubble != null)
		{
			if(Constants.notificationMap!=null && Constants.notificationMap.get(Constants.KEY_ACTIVITY)!=null && Constants.notificationMap.get(Constants.KEY_ACTIVITY).size()>0)
			{
				activityBubble.setVisibility(View.VISIBLE);
				activityBubble.setText(""+Constants.notificationMap.get(Constants.KEY_ACTIVITY).size());
			}
			else
				activityBubble.setVisibility(View.GONE);
		}
	}


	@Override
	public void onBackPressed()
	{
		Intent i=new Intent();
		setResult(RESULT_CANCELED, i);
		finish();
	}



	@Override
	protected void onActivityResult(int requestCode, int resuleCode, Intent data) {
		super.onActivityResult(requestCode, resuleCode, data);

		if(requestCode==Constants.REQUEST_RECORD)
		{
			if (resuleCode == Activity.RESULT_OK)
			{
				String fileName=data.getStringExtra(AudioPlaybackActivity.FileNameArg);
				if(!TextUtils.isEmpty(fileName))
				{
					Intent i=new Intent();
					i.putExtra(AudioPlaybackActivity.FileNameArg,fileName);
					setResult(RESULT_OK,i);
					finish();
				}
			}
		}
	}
}
