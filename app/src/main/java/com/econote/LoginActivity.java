package com.econote;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.econote.utils.AppUtil;
import com.econote.utils.Constants;
import com.econote.utils.GlobalAlertDialogs;
import com.econote.utils.GlobalTracker;
import com.econote.utils.HttpCall;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.Arrays;


public class LoginActivity extends FragmentActivity implements GoogleApiClient.OnConnectionFailedListener,GoogleApiClient.ConnectionCallbacks {

    private Context context;
    private EditText mEmailView;
    private EditText mPasswordView;
    private ImageView fbbutton;
    private ImageView googlebutton;
    public static CallbackManager callbackmanager;
    private Button mEmailSignInButton;
    GoogleApiClient mGoogleApiClient;
    private static final int RC_SIGN_IN = 9001;
    private TextView register,forgot_password;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        context = this;
        GlobalTracker.setScreen("LoginScreen");
        if (PreferenceHandler.getLoginStatus(getApplicationContext()))
        {
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        }
        else
        {
            mEmailView = (EditText) findViewById(R.id.email);
            mPasswordView = (EditText) findViewById(R.id.password);
            mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
            mEmailSignInButton.setOnClickListener(mClickListener);
            fbbutton = (ImageView) findViewById(R.id.fb_login_button);
            googlebutton = (ImageView) findViewById(R.id.google_login_button);
            fbbutton.setOnClickListener(mClickListener);
            googlebutton.setOnClickListener(mClickListener);
            mEmailSignInButton.setOnClickListener(mClickListener);
            register=(TextView)findViewById(R.id.register);
            register.setOnClickListener(mClickListener);
            forgot_password=(TextView)findViewById(R.id.forgot_password);
            forgot_password.setOnClickListener(mClickListener);
            callbackmanager = CallbackManager.Factory.create();
            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestEmail()
                    .requestProfile()
                    .requestScopes(new Scope(Scopes.PLUS_ME))
                    .requestScopes(new Scope(Scopes.PLUS_LOGIN))
                    .build();

            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .enableAutoManage(this, LoginActivity.this)
                    .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                    .build();
        }


    }

    OnClickListener mClickListener=new View.OnClickListener()
    {
        @Override
        public void onClick(View v) {

            switch (v.getId())
            {
                case R.id.fb_login_button:
                    onFblogin();
                    break;
                case R.id.google_login_button:
                    signIn();
                    break;
                case R.id.email_sign_in_button:
                    if(mEmailView.getText().toString().trim().length()==0)
                    {
                        GlobalAlertDialogs.createAlertSingle(LoginActivity.this,"Please enter email.","OK",false);
                    }
                    else if(!Constants.validateEmail(mEmailView.getText().toString().trim()))
                    {
                        GlobalAlertDialogs.createAlertSingle(LoginActivity.this,"Please enter valid email.","OK",false);
                    }
                    else if(mPasswordView.getText().toString().trim().length()==0)
                    {
                        GlobalAlertDialogs.createAlertSingle(LoginActivity.this,"Please enter password.","OK",false);
                    }
                    else
                    {
                        if(Constants.isNetworkAvailable(context))
                        {
                            String data=createLoginRequestData(mEmailView.getText().toString().trim(),mPasswordView.getText().toString().trim());
                            new LoginTask().execute(data);
                        }
                        else
                            Constants.ShowNetworkError(context);
                    }

                    break;
                case R.id.register:
                    Intent i=new Intent(context,RegisterActivity.class);
                    startActivity(i);
                    finish();
                    break;
                case R.id.forgot_password:
                    createAlertForgotPassword();
                    break;
            }

        }
    };

    private void onFblogin() {
           // Set permissions
        LoginManager.getInstance().logInWithReadPermissions(this,Arrays.asList("email","public_profile"));

        LoginManager.getInstance().registerCallback(callbackmanager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {

                        System.out.println("Success");
                        GraphRequest request =GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject json, GraphResponse response) {
                                        if (response.getError() != null)
                                        {
                                            System.out.println("ERROR");
                                        }
                                        else
                                        {
                                            System.out.println("Success"+json);
                                            String id = json.optString("id", "");
                                            String name = json.optString("name", "");
                                            String email = json.optString("email", "");
                                            if(Constants.isNetworkAvailable(context))
                                            {
                                                String data = createFacebookRequestData(name,id,"facebook",email);
                                                new RegisterTask().execute(data);
                                            }
                                            else
                                                Constants.ShowNetworkError(context);
                                        }
                                    }

                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields","id,name,email"); // Parámetros que pedimos a facebook
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        Log.d("TAG_CANCEL", "On cancel");
                    }

                    @Override
                    public void onError(FacebookException error) {
                        Log.d("TAG_ERROR", error.toString());
                    }
                });
    }

    private void signIn()
    {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN)
        {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
        else if (callbackmanager != null) {
            callbackmanager.onActivityResult(requestCode, resultCode, data);

        }
    }

    private void handleSignInResult(GoogleSignInResult result) {

        if (result.isSuccess())
        {
            GoogleSignInAccount acct = result.getSignInAccount();
            Log.i("user_name", ":" + acct.getDisplayName());
            if(Constants.isNetworkAvailable(context))
            {
                String data = createGmailRequestData(acct.getEmail(),acct.getDisplayName(), acct.getId(),"google");
                new RegisterTask().execute(data);
            }
            else
                Constants.ShowNetworkError(context);

        }
        else
        {
            GlobalAlertDialogs.createAlertSingle((LoginActivity) context,"There is some problem in Google SignIn", "OK", false);
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    private String createLoginRequestData(String email,String password) {
        String data = "";
        try {
            data += URLEncoder.encode("email", "UTF-8")
                    + "=" + URLEncoder.encode(email, "UTF-8");

            data += "&" + URLEncoder.encode("password", "UTF-8") + "="
                    + URLEncoder.encode(password, "UTF-8");

            data += "&" + URLEncoder.encode("logintype", "UTF-8") + "="
                    + URLEncoder.encode("email", "UTF-8");

            data += "&" + URLEncoder.encode("passphase", "UTF-8") + "="
                    + URLEncoder.encode(Constants.PASSPHRASE, "UTF-8");

            data += "&" + URLEncoder.encode("auth", "UTF-8") + "="
                    + URLEncoder.encode(Constants.AUTHKEY, "UTF-8");

            data += "&" + URLEncoder.encode("action", "UTF-8") + "="
                    + URLEncoder.encode("login", "UTF-8");
            Log.e("Himanshu", "Dixit Facebook Data" + data);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return data;
    }

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }


    private class LoginTask extends AsyncTask<String, Void, String> {

        protected void onPreExecute()
        {
            Constants.showProgressDialog(context);
        }

        @Override
        protected String doInBackground(String... params)
        {
            String url = Constants.GLOBAL_URL;
            HttpCall httpCall = new HttpCall();
            String resp = httpCall.executeHttpPostRequest(url,params[0]);
            return resp;
        }

        protected void onPostExecute(String resp) {
            if (resp != null) {
                try {
                    JSONObject jo = new JSONObject(resp);
                    boolean status = jo.getBoolean("result");
                    if (status)
                    {
                        Constants.hideProgressDialog();
                        JSONObject joData=jo.optJSONObject("data");
                        if(joData!=null)
                        {
                            AppUtil.setUserId(context, joData.optString("uno",""));
                            Intent intent = new Intent(LoginActivity.this,MainActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    }
                    else
                    {
                        Constants.hideProgressDialog();
                        GlobalAlertDialogs.createAlertSingle((LoginActivity) context, jo.optString("msg"), "OK", false);
                    }
                } catch (Exception e) {
                    Constants.hideProgressDialog();
                    GlobalAlertDialogs.createAlertSingle((LoginActivity) context, Constants.SERVER_EXCEPTION_MSG, "OK", false);
                }
            } else {
                Constants.hideProgressDialog();
                GlobalAlertDialogs.createAlertSingle((LoginActivity) context, "Timeout From Server.Please try again.", "OK", false);
            }
        }
    }



    private String createFacebookRequestData(String name,String id,String loginType,String email)
    {
        String data = "";
        try {
            data += URLEncoder.encode("facebook_id", "UTF-8")
                    + "=" + URLEncoder.encode(id, "UTF-8");

            data += "&" + URLEncoder.encode("logintype", "UTF-8") + "="
                    + URLEncoder.encode(loginType, "UTF-8");

            data += "&" + URLEncoder.encode("passphase", "UTF-8") + "="
                    + URLEncoder.encode(Constants.PASSPHRASE, "UTF-8");

            data += "&" + URLEncoder.encode("auth", "UTF-8") + "="
                    + URLEncoder.encode(Constants.AUTHKEY, "UTF-8");

            data += "&" + URLEncoder.encode("action", "UTF-8") + "="
                    + URLEncoder.encode("register", "UTF-8");

            data += "&" + URLEncoder.encode("uname", "UTF-8") + "="
                    + URLEncoder.encode(name, "UTF-8");

            data += "&" + URLEncoder.encode("email", "UTF-8") + "="
                    + URLEncoder.encode(email, "UTF-8");

            Log.e("Himanshu", "Dixit Facebook Data" + data);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return data;
    }

    private String createGmailRequestData(String email,String name,String id,String loginType)
    {
        String data = "";
        try {
            data += URLEncoder.encode("google_id", "UTF-8")
                    + "=" + URLEncoder.encode(id, "UTF-8");

            data += "&" + URLEncoder.encode("logintype", "UTF-8") + "="
                    + URLEncoder.encode(loginType, "UTF-8");

            data += "&" + URLEncoder.encode("passphase", "UTF-8") + "="
                    + URLEncoder.encode(Constants.PASSPHRASE, "UTF-8");

            data += "&" + URLEncoder.encode("auth", "UTF-8") + "="
                    + URLEncoder.encode(Constants.AUTHKEY, "UTF-8");

            data += "&" + URLEncoder.encode("action", "UTF-8") + "="
                    + URLEncoder.encode("register", "UTF-8");

            data += "&" + URLEncoder.encode("uname", "UTF-8") + "="
                    + URLEncoder.encode(name, "UTF-8");

            data += "&" + URLEncoder.encode("email", "UTF-8") + "="
                    + URLEncoder.encode(email, "UTF-8");

            Log.e("Himanshu", "Dixit Google Data" + data);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return data;
    }


    private class RegisterTask extends AsyncTask<String, Void, String> {

        protected void onPreExecute()
        {
            Constants.showProgressDialog(context);
        }

        @Override
        protected String doInBackground(String... params)
        {
            String url = Constants.GLOBAL_URL;
            HttpCall httpCall = new HttpCall();
            String resp = httpCall.executeHttpPostRequest(url,params[0]);
            return resp;
        }

        protected void onPostExecute(String resp) {
            if (resp != null) {
                try {
                    Log.e("Himanshu","Dixit Response"+resp);
                    JSONObject jo = new JSONObject(resp);
                    boolean status = jo.getBoolean("result");
                    if (status)
                    {
                        Constants.hideProgressDialog();
                        AppUtil.setUserId(context,jo.optString("data",""));
                        Intent intent = new Intent(LoginActivity.this,MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                    else
                    {
                        Constants.hideProgressDialog();
                        GlobalAlertDialogs.createAlertSingle((LoginActivity) context, jo.optString("msg"), "OK", false);
                    }
                } catch (Exception e) {
                    Constants.hideProgressDialog();
                    GlobalAlertDialogs.createAlertSingle((LoginActivity) context, Constants.SERVER_EXCEPTION_MSG, "OK", false);
                }
            } else {
                Constants.hideProgressDialog();
                GlobalAlertDialogs.createAlertSingle((LoginActivity) context, "Timeout From Server.Please try again.", "OK", false);
            }
        }
    }



    public void createAlertForgotPassword()
    {
        final Dialog dialog = new Dialog(context);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_forgotpassword);
        final EditText email = (EditText) dialog.findViewById(R.id.edit_email);
        TextView btnConfirm = (TextView) dialog.findViewById(R.id.confirm);
        btnConfirm.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v) {
                if(email.getText().toString().trim().length()==0)
                {
                    email.requestFocus();
                    email.setError("Please enter your registered email address.");
                }
				else if(!Constants.validateEmail(email.getText().toString().trim()))
                {
                    email.requestFocus();
                    email.setError("Please enter valid email address.");
                }
                else
                {
                    dialog.dismiss();
                    if(Constants.isNetworkAvailable(context))
                    {
                        String data=createForgotPasswordRequestData(email.getText().toString().trim());
                        new ForgetPasswordTask().execute(data);
                    }
                    else
                    {
                        Constants.ShowNetworkError(context);
                    }

                }
            }
        });

        TextView btnCancel = (TextView) dialog.findViewById(R.id.cancel);
        btnCancel.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private String createForgotPasswordRequestData(String email)
    {
        String data = "";
        try {
            data += URLEncoder.encode("email", "UTF-8")
                    + "=" + URLEncoder.encode(email, "UTF-8");

            data += "&" + URLEncoder.encode("passphase", "UTF-8") + "="
                    + URLEncoder.encode(Constants.PASSPHRASE, "UTF-8");

            data += "&" + URLEncoder.encode("auth", "UTF-8") + "="
                    + URLEncoder.encode(Constants.AUTHKEY, "UTF-8");

            data += "&" + URLEncoder.encode("action", "UTF-8") + "="
                    + URLEncoder.encode("forget_password", "UTF-8");

            Log.e("Himanshu", "Dixit Facebook Data" + data);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return data;
    }


    private class ForgetPasswordTask extends AsyncTask<String, Void, String> {

        protected void onPreExecute()
        {
            Constants.showProgressDialog(context);
        }

        @Override
        protected String doInBackground(String... params)
        {
            String url = Constants.GLOBAL_URL;
            HttpCall httpCall = new HttpCall();
            String resp = httpCall.executeHttpPostRequest(url,params[0]);
            return resp;
        }

        protected void onPostExecute(String resp) {
            if (resp != null) {
                try {
                    Log.e("Himanshu","Dixit Response"+resp);
                    JSONObject jo = new JSONObject(resp);
                    boolean status = jo.getBoolean("result");
                    if (status)
                    {
                        Constants.hideProgressDialog();
                        GlobalAlertDialogs.createAlertSingle((LoginActivity) context, jo.optString("msg"), "OK", false);
                    }
                    else
                    {
                        Constants.hideProgressDialog();
                        GlobalAlertDialogs.createAlertSingle((LoginActivity) context, jo.optString("msg"), "OK", false);
                    }
                } catch (Exception e) {
                    Constants.hideProgressDialog();
                    GlobalAlertDialogs.createAlertSingle((LoginActivity) context, Constants.SERVER_EXCEPTION_MSG, "OK", false);
                }
            } else {
                Constants.hideProgressDialog();
                GlobalAlertDialogs.createAlertSingle((LoginActivity) context, "Timeout From Server.Please try again.", "OK", false);
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        //add this to connect Google Client
        mGoogleApiClient.connect();
    }
    @Override
    protected void onStop() {
        super.onStop();
        //Stop the Google Client when activity is stopped
        if(mGoogleApiClient.isConnected())
        {
            mGoogleApiClient.disconnect();
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        if(mGoogleApiClient.isConnected())
        {
            mGoogleApiClient.connect();
        }
    }

}

