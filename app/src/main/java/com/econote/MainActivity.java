package com.econote;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.econote.fragment.ChangePasswordFragment;
import com.econote.fragment.HomeFragment;
import com.econote.fragment.JoinUsFragment;
import com.econote.fragment.MyProfileFragment;
import com.econote.fragment.PurchaseFragment;
import com.econote.fragment.ReportIssueFragment;
import com.econote.fragment.VoiceFragment;
import com.econote.json.JsonParser;
import com.econote.model.JoinProgramModel;
import com.econote.model.ReportIssueModel;
import com.econote.model.UserModel;
import com.econote.services.NotificationService;
import com.econote.utils.AppUtil;
import com.econote.utils.Constants;
import com.econote.utils.GlobalAlertDialogs;
import com.econote.utils.GlobalTracker;
import com.econote.utils.HttpCall;

import org.json.JSONObject;

import java.net.URLEncoder;

public class MainActivity extends AppCompatActivity {

    DrawerLayout drawer;
    ActionBarDrawerToggle mDrawerToggle;
    private Context context;
    private Toolbar toolbar;
    private RelativeLayout row_home, row_report, row_join, row_purchase, row_change_password,row_voice, row_logout;
    private TextView username;
    private LinearLayout my_profile;
    private TextView messageBubble, hazardBubble, reminderBubble, activityBubble,purchase_count;
    private ReportIssueModel reportModel;
    private JoinProgramModel joinModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this;
        GlobalTracker.setScreen("MainScreen");
        reportModel=getIntent().getParcelableExtra("report_model");
        joinModel=getIntent().getParcelableExtra("join_model");
        username = (TextView) findViewById(R.id.username);
        row_home = (RelativeLayout) findViewById(R.id.row_home);
        row_home.setOnClickListener(mClickListener);
        row_report = (RelativeLayout) findViewById(R.id.row_report);
        row_report.setOnClickListener(mClickListener);
        row_join = (RelativeLayout) findViewById(R.id.row_join);
        row_join.setOnClickListener(mClickListener);
        row_purchase = (RelativeLayout) findViewById(R.id.row_purchase);
        row_purchase.setOnClickListener(mClickListener);
        row_change_password = (RelativeLayout) findViewById(R.id.row_change_password);
        row_change_password.setOnClickListener(mClickListener);
        row_voice= (RelativeLayout) findViewById(R.id.row_voice);
        row_voice.setOnClickListener(mClickListener);
        row_logout = (RelativeLayout) findViewById(R.id.row_logout);
        row_logout.setOnClickListener(mClickListener);
        my_profile = (LinearLayout) findViewById(R.id.profile_layout);
        my_profile.setOnClickListener(mClickListener);
        purchase_count=(TextView)findViewById(R.id.purchase_count);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        prepareActionBar(toolbar);
        drawer = (DrawerLayout) findViewById(R.id.DrawerLayout);
        mDrawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.openDrawer, R.string.closeDrawer) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);

            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);

            }
        };
        drawer.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationReceiver, new IntentFilter(Constants.INTENT_FILTER_NOTIFICATION));
        if(reportModel!=null)
        {
            if (toolbar != null)
                toolbar.setTitle("");
            ReportIssueFragment fragmentReport = ReportIssueFragment.newInstance();
            Bundle bundle=new Bundle();
            bundle.putParcelable("model",reportModel);
            fragmentReport.setArguments(bundle);
            addFragment(fragmentReport);
        }
        else
        {
            if (Constants.isNetworkAvailable(context)) {
                String data = createUserRequestData(AppUtil.getUserId(context));
                new GetUserTask().execute(data);
            } else
                Constants.ShowNetworkError(context);
        }
    }

    View.OnClickListener mClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (drawer.isDrawerOpen(Gravity.LEFT)) {
                drawer.closeDrawers();
            }
            switch (v.getId()) {
                case R.id.row_home:
                    if (toolbar != null)
                        toolbar.setTitle("");
                    GlobalTracker.setScreen("HomeScreen");
                    HomeFragment fragmentHome = HomeFragment.newInstance();
                    addFragment(fragmentHome);
                    break;
                case R.id.row_report:
                    if (toolbar != null)
                        toolbar.setTitle("");
                    GlobalTracker.setScreen("ReportIssueScreen");
                    ReportIssueFragment fragmentReport = ReportIssueFragment.newInstance();
                    addFragment(fragmentReport);
                    break;
                case R.id.row_join:
                    if (toolbar != null)
                        toolbar.setTitle("");
                    GlobalTracker.setScreen("JoinUsScreen");
                    JoinUsFragment fragmentjoin = JoinUsFragment.newInstance();
                    addFragment(fragmentjoin);
                    break;
                case R.id.row_purchase:
                    if (toolbar != null)
                        toolbar.setTitle("");
                    GlobalTracker.setScreen("PurchaseScreen");
                    PurchaseFragment fragmentPurchase = PurchaseFragment.newInstance();
                    addFragment(fragmentPurchase);
                    break;
                case R.id.row_change_password:
                    if (toolbar != null)
                        toolbar.setTitle("");
                    GlobalTracker.setScreen("ChangePasswordScreen");
                    ChangePasswordFragment fragmentChangePassword = ChangePasswordFragment.newInstance();
                    addFragment(fragmentChangePassword);
                    break;
                case R.id.profile_layout:
                    if (toolbar != null)
                        toolbar.setTitle("");
                    GlobalTracker.setScreen("ProfileScreen");
                    MyProfileFragment fragmentMyProfile = MyProfileFragment.newInstance();
                    addFragment(fragmentMyProfile);
                    break;
                case R.id.row_voice:
                    if (toolbar != null)
                        toolbar.setTitle("");
                    GlobalTracker.setScreen("VoiceListScreen");
                    VoiceFragment fragmentVoice = VoiceFragment.newInstance();
                    addFragment(fragmentVoice);
                    break;
                case R.id.row_logout:
                    GlobalAlertDialogs.createAlertLogout(MainActivity.this, "Are you sure want to logout?", "OK", true);
                    break;
            }

        }
    };

    private void prepareActionBar(Toolbar toolbar) {
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setHomeButtonEnabled(false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        MenuItem item = menu.findItem(R.id.action_chat);
        RelativeLayout messageView = (RelativeLayout) item.getActionView();
        messageBubble = (TextView) messageView.findViewById(R.id.notification_count);
        messageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(context,NotificationActivity.class);
                startActivity(i);
            }
        });

        item = menu.findItem(R.id.action_haz);
        RelativeLayout hazardView = (RelativeLayout) item.getActionView();
        hazardBubble = (TextView) hazardView.findViewById(R.id.notification_count);
        hazardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(context,NotificationActivity.class);
                startActivity(i);
            }
        });

        item = menu.findItem(R.id.action_alarm);
        RelativeLayout reminderView = (RelativeLayout) item.getActionView();
        reminderBubble = (TextView) reminderView.findViewById(R.id.notification_count);
        reminderView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(context,NotificationActivity.class);
                startActivity(i);
            }
        });

        item = menu.findItem(R.id.action_notif);
        RelativeLayout activityView = (RelativeLayout) item.getActionView();
        activityBubble = (TextView) activityView.findViewById(R.id.notification_count);
        activityView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(context,NotificationActivity.class);
                startActivity(i);
            }
        });

        showNotificationCount();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_map:
            {
                if (toolbar != null)
                    toolbar.setTitle("");
                HomeFragment fragmentHome = HomeFragment.newInstance();
                addFragment(fragmentHome);
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void addFragment(Fragment frag) {
        getSupportFragmentManager().beginTransaction().replace(R.id.main_container, frag).commit();
        mDrawerToggle.syncState();
    }


    private String createUserRequestData(String id) {
        String data = "";
        try {
            data += URLEncoder.encode("userid", "UTF-8")
                    + "=" + URLEncoder.encode(id, "UTF-8");

            data += "&" + URLEncoder.encode("passphase", "UTF-8") + "="
                    + URLEncoder.encode(Constants.PASSPHRASE, "UTF-8");

            data += "&" + URLEncoder.encode("auth", "UTF-8") + "="
                    + URLEncoder.encode(Constants.AUTHKEY, "UTF-8");

            data += "&" + URLEncoder.encode("action", "UTF-8") + "="
                    + URLEncoder.encode("getUserDetail", "UTF-8");

            Log.e("Himanshu", "Dixit User Data" + data);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return data;
    }

    private class GetUserTask extends AsyncTask<String, Void, String> {

        protected void onPreExecute() {
            Constants.showProgressDialog(context);
        }

        @Override
        protected String doInBackground(String... params) {
            String url = Constants.GLOBAL_URL;
            HttpCall httpCall = new HttpCall();
            String resp = httpCall.executeHttpPostRequest(url, params[0]);
            return resp;
        }

        protected void onPostExecute(String resp) {
            if (resp != null) {
                try {
                    JSONObject jo = new JSONObject(resp);
                    boolean status = jo.getBoolean("result");
                    if (status) {
                        Constants.hideProgressDialog();
                        UserModel model = new JsonParser().parseUserModel(resp);
                        if (model != null)
                        {
                            username.setText(model.getUname());
                            AppUtil.setUserEmail(context, model.getEmail());
                            Intent i = new Intent(MainActivity.this, NotificationService.class);
                            i.putExtra("email",model.getEmail());
                            context.startService(i);
                            HomeFragment fragment = HomeFragment.newInstance();
                            addFragment(fragment);
                        }
                        else {
                            GlobalAlertDialogs.createAlertSingle((MainActivity) context, "Some problem occured in getting user info", "OK", false);
                        }
                    } else {
                        Constants.hideProgressDialog();
                        GlobalAlertDialogs.createAlertSingle((MainActivity) context, jo.optString("msg"), "OK", false);
                    }
                } catch (Exception e) {
                    Constants.hideProgressDialog();
                    GlobalAlertDialogs.createAlertSingle((MainActivity) context, Constants.SERVER_EXCEPTION_MSG, "OK", false);
                }
            } else {
                Constants.hideProgressDialog();
                GlobalAlertDialogs.createAlertSingle((MainActivity) context, "Timeout From Server.Please try again.", "OK", false);
            }
        }
    }


    @Override
    public void onBackPressed() {
        GlobalAlertDialogs.createAlertDouble(context, "Are you sure want to quit", "OK", true);
    }


    private BroadcastReceiver mNotificationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            showNotificationCount();
        }
    };

    private void showNotificationCount() {

        if (messageBubble != null)
        {
            if(Constants.notificationMap!=null && Constants.notificationMap.get(Constants.KEY_MESSAGE)!=null && Constants.notificationMap.get(Constants.KEY_MESSAGE).size()>0)
            {
                messageBubble.setVisibility(View.VISIBLE);
                messageBubble.setText(""+Constants.notificationMap.get(Constants.KEY_MESSAGE).size());
            }
            else
                messageBubble.setVisibility(View.GONE);
        }
        if (hazardBubble != null)
        {
            if(Constants.notificationMap!=null && Constants.notificationMap.get(Constants.KEY_HAZARD)!=null && Constants.notificationMap.get(Constants.KEY_HAZARD).size()>0)
            {
                hazardBubble.setVisibility(View.VISIBLE);
                hazardBubble.setText(""+Constants.notificationMap.get(Constants.KEY_HAZARD).size());
            }
            else
                hazardBubble.setVisibility(View.GONE);
        }
        if (reminderBubble != null)
        {
            if(Constants.notificationMap!=null && Constants.notificationMap.get(Constants.KEY_REMINDER)!=null && Constants.notificationMap.get(Constants.KEY_REMINDER).size()>0)
            {
                reminderBubble.setVisibility(View.VISIBLE);
                reminderBubble.setText(""+Constants.notificationMap.get(Constants.KEY_REMINDER).size());
            }
            else
                reminderBubble.setVisibility(View.GONE);
        }
        if (activityBubble != null)
        {
            if(Constants.notificationMap!=null && Constants.notificationMap.get(Constants.KEY_ACTIVITY)!=null && Constants.notificationMap.get(Constants.KEY_ACTIVITY).size()>0)
            {
                activityBubble.setVisibility(View.VISIBLE);
                activityBubble.setText(""+Constants.notificationMap.get(Constants.KEY_ACTIVITY).size());
            }
            else
                activityBubble.setVisibility(View.GONE);
        }
    }

    public void callReportIssue()
    {
        if (toolbar != null)
            toolbar.setTitle("");
        ReportIssueFragment fragmentReport = ReportIssueFragment.newInstance();
        addFragment(fragmentReport);
    }

    public void callJoinProgram()
    {
        if (toolbar != null)
            toolbar.setTitle("");
        JoinUsFragment fragmentjoin = JoinUsFragment.newInstance();
        addFragment(fragmentjoin);
    }

    public void callJoinProgramEdit(ReportIssueModel model)
    {
        if (toolbar != null)
            toolbar.setTitle("");
        JoinUsFragment fragmentjoin = JoinUsFragment.newInstance();
        Bundle bundle=new Bundle();
        bundle.putParcelable("program_model",model);
        fragmentjoin.setArguments(bundle);
        addFragment(fragmentjoin);
    }


    public void callHome()
    {
        if (toolbar != null)
            toolbar.setTitle("");
        HomeFragment fragmentHome = HomeFragment.newInstance();
        addFragment(fragmentHome);
    }


    public void setMyPurchaseCount(int count)
    {
        if (purchase_count != null)
        {
            if(count>0)
            {
                purchase_count.setVisibility(View.VISIBLE);
                purchase_count.setText(""+count);
            }
            else
                purchase_count.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onDestroy()
    {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationReceiver);
        super.onDestroy();
    }
}
