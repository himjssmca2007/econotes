package com.econote;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import com.econote.adapter.NotificationListAdapter;
import com.econote.customviews.DividerItemDecoration;
import com.econote.model.NotificationModel;
import com.econote.services.NotificationStatusUpdateService;
import com.econote.utils.AppUtil;
import com.econote.utils.Constants;
import com.econote.utils.GlobalAlertDialogs;
import com.econote.utils.GlobalTracker;

import java.util.ArrayList;

public class NotificationActivity extends AppCompatActivity
{
	private Context context;
	private Toolbar toolbar;
	private RecyclerView recyclerView;
	private LinearLayoutManager linearLayoutManager;
	private String url;
	private NotificationListAdapter adapter;
	private ArrayList<NotificationModel> notificationList;
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_notification_list);
		context=this;
		toolbar = (Toolbar) findViewById(R.id.toolbar_viewpager);
		toolbar.setTitle("Notifications");
		prepareActionBar(toolbar);
		GlobalTracker.setScreen("NotificationListScreen");
		recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
		linearLayoutManager = new LinearLayoutManager(this);
		recyclerView.setLayoutManager(linearLayoutManager);
		recyclerView.addItemDecoration(new DividerItemDecoration(context, DividerItemDecoration.VERTICAL_LIST));
		if(Constants.notificationList!=null && Constants.notificationList.size()>0)
		{
			NotificationListAdapter adapter=new NotificationListAdapter(Constants.notificationList);
			recyclerView.setAdapter(adapter);
		}
		else
			GlobalAlertDialogs.createAlertSingle(NotificationActivity.this,"Couldn't found any notification, Check back later","OK",true);
	}


	private void prepareActionBar(Toolbar toolbar)
	{
		setSupportActionBar(toolbar);
		ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setHomeButtonEnabled(true);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		switch (id)
		{
			case android.R.id.home:
				onBackPressed();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	public void onBackPressed()
	{
		Intent i=new Intent(context,NotificationStatusUpdateService.class);
		i.putExtra("email", AppUtil.getUserEmail(context));
		i.putExtra("type","ACTIVITY");
		context.startService(i);
		finish();
	}

}
