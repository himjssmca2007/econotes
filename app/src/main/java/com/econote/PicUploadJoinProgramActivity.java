package com.econote;


import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.econote.adapter.IssueListingAdapter;
import com.econote.model.JoinProgramModel;
import com.econote.model.ReportIssueModel;
import com.econote.services.NotificationService;
import com.econote.utils.AppUtil;
import com.econote.utils.Constants;
import com.econote.utils.GlobalAlertDialogs;
import com.econote.utils.GlobalTracker;
import com.econote.utils.HttpCall;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONObject;

import java.util.ArrayList;

import nl.changer.polypicker.ImagePickerActivity;


public class PicUploadJoinProgramActivity extends AppCompatActivity {

    private Context context;
    private JoinProgramModel model;
    private TextView upload_pic,submit;
    private Toolbar toolbar;
    private static final int INTENT_REQUEST_GET_N_IMAGES = 14;
    ArrayList<String> mMedia = new ArrayList<String>();
    private LinearLayout mSelectedImagesContainer;
    private TextView mSelectedImageEmptyMessage;
    private TextView messageBubble,hazardBubble,reminderBubble,activityBubble;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pic_join_program);
        context = this;
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        prepareActionBar(toolbar);
        GlobalTracker.setScreen("JoinProgramPicUploadScreen");
        model=getIntent().getParcelableExtra("model");
        upload_pic=(TextView)findViewById(R.id.upload_pic);
        upload_pic.setOnClickListener(mClickListener);
        submit=(TextView)findViewById(R.id.submit);
        submit.setOnClickListener(mClickListener);
        mSelectedImagesContainer = (LinearLayout) findViewById(nl.changer.polypicker.R.id.selected_photos_container);
        mSelectedImageEmptyMessage = (TextView) findViewById(nl.changer.polypicker.R.id.selected_photos_empty);
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationReceiver, new IntentFilter(Constants.INTENT_FILTER_NOTIFICATION));
    }

    private void prepareActionBar(Toolbar toolbar)
    {
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
    }



    OnClickListener mClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {

            switch (v.getId())
            {
                case R.id.upload_pic:
                    getNImages();
                    break;
                case R.id.submit:
                    if(mMedia!=null && mMedia.size()==0)
                    {
                        GlobalAlertDialogs.createAlertSingle(PicUploadJoinProgramActivity.this, "Please select images of issue.", "OK", false);
                    }
                    else
                    {
                        model.setImageList(mMedia);
                        Intent i=new Intent();
                        i.putExtra("model",model);
                        setResult(RESULT_OK, i);
                        finish();
                    }
                    break;
            }

        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        MenuItem item = menu.findItem(R.id.action_chat);
        RelativeLayout messageView = (RelativeLayout) item.getActionView();
        messageBubble = (TextView) messageView.findViewById(R.id.notification_count);
        messageView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(context,NotificationActivity.class);
                startActivity(i);
            }
        });

        item = menu.findItem(R.id.action_haz);
        RelativeLayout hazardView = (RelativeLayout) item.getActionView();
        hazardBubble = (TextView) hazardView.findViewById(R.id.notification_count);
        hazardView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(context,NotificationActivity.class);
                startActivity(i);
            }
        });

        item = menu.findItem(R.id.action_alarm);
        RelativeLayout reminderView = (RelativeLayout) item.getActionView();
        reminderBubble = (TextView) reminderView.findViewById(R.id.notification_count);
        reminderView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(context,NotificationActivity.class);
                startActivity(i);
            }
        });

        item = menu.findItem(R.id.action_notif);
        RelativeLayout activityView = (RelativeLayout) item.getActionView();
        activityBubble = (TextView) activityView.findViewById(R.id.notification_count);
        activityView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(context,NotificationActivity.class);
                startActivity(i);
            }
        });

        showNotificationCount();
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            case R.id.action_map:
            {
                Intent i=new Intent(PicUploadJoinProgramActivity.this,MainActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                finish();
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void getNImages() {
        Intent intent = new Intent(context, ImagePickerActivity.class);
        // limit image pick count to only 3 images.
        intent.putExtra(ImagePickerActivity.EXTRA_SELECTION_LIMIT,5);
        startActivityForResult(intent, INTENT_REQUEST_GET_N_IMAGES);
    }

    @Override
    protected void onActivityResult(int requestCode, int resuleCode, Intent intent) {
        super.onActivityResult(requestCode, resuleCode, intent);

        if (resuleCode == Activity.RESULT_OK)
        {
            if (requestCode == INTENT_REQUEST_GET_N_IMAGES)
            {
                mMedia.clear();
                mSelectedImagesContainer.removeAllViews();
                Parcelable[] parcelableUris = intent.getParcelableArrayExtra(ImagePickerActivity.EXTRA_IMAGE_URIS);

                if(parcelableUris ==null) {
                    return;
                }

                // Java doesn't allow array casting, this is a little hack
                Uri[] uris = new Uri[parcelableUris.length];
                System.arraycopy(parcelableUris, 0, uris, 0, parcelableUris.length);

                if(uris != null) {
                    for (Uri uri : uris)
                    {
                        String compressedPath=Constants.compressImage(uri.getPath(),context);
                        addImage(compressedPath);
                    }
                }
            }
        }
    }


    public boolean addImage(String path)
    {
        if (mMedia.add(path))
        {
            View rootView = LayoutInflater.from(PicUploadJoinProgramActivity.this).inflate(nl.changer.polypicker.R.layout.list_item_selected_thumbnail, null);
            ImageView thumbnail = (ImageView) rootView.findViewById(nl.changer.polypicker.R.id.selected_photo);
            mSelectedImagesContainer.addView(rootView, 0);
            ImageLoader.getInstance().displayImage("file:///"+path,thumbnail);
            int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,60, getResources().getDisplayMetrics());
            thumbnail.setLayoutParams(new FrameLayout.LayoutParams(px, px));
            if (mMedia.size() >= 1) {
                mSelectedImagesContainer.setVisibility(View.VISIBLE);
                mSelectedImageEmptyMessage.setVisibility(View.GONE);
            }
            return true;
        }

        return false;
    }


    private BroadcastReceiver mNotificationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            showNotificationCount();
        }
    };

    private void showNotificationCount() {

        if (messageBubble != null)
        {
            if(Constants.notificationMap!=null && Constants.notificationMap.get(Constants.KEY_MESSAGE)!=null && Constants.notificationMap.get(Constants.KEY_MESSAGE).size()>0)
            {
                messageBubble.setVisibility(View.VISIBLE);
                messageBubble.setText(""+Constants.notificationMap.get(Constants.KEY_MESSAGE).size());
            }
            else
                messageBubble.setVisibility(View.GONE);
        }
        if (hazardBubble != null)
        {
            if(Constants.notificationMap!=null && Constants.notificationMap.get(Constants.KEY_HAZARD)!=null && Constants.notificationMap.get(Constants.KEY_HAZARD).size()>0)
            {
                hazardBubble.setVisibility(View.VISIBLE);
                hazardBubble.setText(""+Constants.notificationMap.get(Constants.KEY_HAZARD).size());
            }
            else
                hazardBubble.setVisibility(View.GONE);
        }
        if (reminderBubble != null)
        {
            if(Constants.notificationMap!=null && Constants.notificationMap.get(Constants.KEY_REMINDER)!=null && Constants.notificationMap.get(Constants.KEY_REMINDER).size()>0)
            {
                reminderBubble.setVisibility(View.VISIBLE);
                reminderBubble.setText(""+Constants.notificationMap.get(Constants.KEY_REMINDER).size());
            }
            else
                reminderBubble.setVisibility(View.GONE);
        }
        if (activityBubble != null)
        {
            if(Constants.notificationMap!=null && Constants.notificationMap.get(Constants.KEY_ACTIVITY)!=null && Constants.notificationMap.get(Constants.KEY_ACTIVITY).size()>0)
            {
                activityBubble.setVisibility(View.VISIBLE);
                activityBubble.setText(""+Constants.notificationMap.get(Constants.KEY_ACTIVITY).size());
            }
            else
                activityBubble.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onDestroy()
    {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationReceiver);
        super.onDestroy();
    }
}

