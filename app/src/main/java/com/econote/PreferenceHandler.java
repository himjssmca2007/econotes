package com.econote;

import java.io.File;
import android.content.Context;
import android.preference.PreferenceManager;

/**
 * Created by admin on 9/2/2015.
 */
public class PreferenceHandler
{

    private static final String PREF_SET_LOGIN_STATUS = "login_status";
    private static final String PREF_SET_USERNAME = "user_name";
    private static final String PREF_SET_USER_EMAIL = "user_email";


    public static void clearAllFromPrefrence(String packageName) {
        File sharedPreferenceFile = new File("/data/data/" + packageName + "/shared_prefs/");
        File[] listFiles = sharedPreferenceFile.listFiles();
        for (File file : listFiles) {
            file.delete();
        }
    }


    public static boolean setLoginStatus(Context paramContext, boolean flag) {
        return PreferenceManager.getDefaultSharedPreferences(paramContext)
                .edit().putBoolean(PREF_SET_LOGIN_STATUS, flag)
                .commit();
    }

    public static boolean getLoginStatus(Context paramContext) {
        return PreferenceManager.getDefaultSharedPreferences(paramContext).getBoolean(PREF_SET_LOGIN_STATUS, false);
    }


    public static void setUserName(Context paramContext, String flag) {
        PreferenceManager.getDefaultSharedPreferences(paramContext)
                .edit().putString(PREF_SET_USERNAME, flag)
                .commit();
    }

    public static String getUserName(Context paramContext) {
        return PreferenceManager.getDefaultSharedPreferences(paramContext).getString(PREF_SET_USERNAME, null);
    }

    public static void setUserEmail(Context paramContext, String flag) {
        PreferenceManager.getDefaultSharedPreferences(paramContext)
                .edit().putString(PREF_SET_USER_EMAIL, flag)
                .commit();
    }

    public static String getUserEmail(Context paramContext) {
        return PreferenceManager.getDefaultSharedPreferences(paramContext).getString(PREF_SET_USER_EMAIL, null);
    }

}
