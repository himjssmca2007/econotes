package com.econote;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.econote.adapter.ProgramListAdapter;
import com.econote.json.JsonParser;
import com.econote.model.JoinProgramModel;
import com.econote.model.ProgramModel;
import com.econote.utils.AppUtil;
import com.econote.utils.Constants;
import com.econote.utils.GlobalAlertDialogs;
import com.econote.utils.GlobalTracker;
import com.econote.utils.HttpCall;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.ArrayList;


public class ProgramDetailActivity extends AppCompatActivity {

    private Context context;
    private TextView submit;
    private Toolbar toolbar;
    private TextView messageBubble,hazardBubble,reminderBubble,activityBubble;
    private ProgramModel selectedProgram;
    private ImageView image;
    private TextView title,cost,description;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_program_detail);
        context = this;
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        prepareActionBar(toolbar);
        GlobalTracker.setScreen("ProgramDetailScreen");
        selectedProgram=getIntent().getParcelableExtra("model");
        image=(ImageView)findViewById(R.id.image);
        title=(TextView)findViewById(R.id.title);
        cost=(TextView)findViewById(R.id.cost);
        description=(TextView)findViewById(R.id.description);
        submit=(TextView)findViewById(R.id.submit);
        submit.setOnClickListener(mClickListener);

        title.setText(selectedProgram.getTitle());
        if(selectedProgram.getPhotos()!=null && selectedProgram.getPhotos().size()>0)
            ImageLoader.getInstance().displayImage(selectedProgram.getPhotos().get(0),image, Constants.options);
        cost.setText("Rs. " + selectedProgram.getCost());
        description.setText(selectedProgram.getDetail());
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationReceiver, new IntentFilter(Constants.INTENT_FILTER_NOTIFICATION));

    }

    private void prepareActionBar(Toolbar toolbar)
    {
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
    }



    OnClickListener mClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {

            switch (v.getId())
            {
                case R.id.submit:
                    Intent i=new Intent();
                    i.putExtra("model",selectedProgram);
                    setResult(RESULT_OK, i);
                    finish();
                    break;
            }

        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        MenuItem item = menu.findItem(R.id.action_chat);
        RelativeLayout messageView = (RelativeLayout) item.getActionView();
        messageBubble = (TextView) messageView.findViewById(R.id.notification_count);
        messageView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(context,NotificationActivity.class);
                startActivity(i);
            }
        });

        item = menu.findItem(R.id.action_haz);
        RelativeLayout hazardView = (RelativeLayout) item.getActionView();
        hazardBubble = (TextView) hazardView.findViewById(R.id.notification_count);
        hazardView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(context,NotificationActivity.class);
                startActivity(i);
            }
        });

        item = menu.findItem(R.id.action_alarm);
        RelativeLayout reminderView = (RelativeLayout) item.getActionView();
        reminderBubble = (TextView) reminderView.findViewById(R.id.notification_count);
        reminderView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(context,NotificationActivity.class);
                startActivity(i);
            }
        });

        item = menu.findItem(R.id.action_notif);
        RelativeLayout activityView = (RelativeLayout) item.getActionView();
        activityBubble = (TextView) activityView.findViewById(R.id.notification_count);
        activityView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(context,NotificationActivity.class);
                startActivity(i);
            }
        });

        showNotificationCount();
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            case R.id.action_map:
            {
                Intent i=new Intent(ProgramDetailActivity.this,MainActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                finish();
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }


    private BroadcastReceiver mNotificationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            showNotificationCount();
        }
    };

    private void showNotificationCount() {

        if (messageBubble != null)
        {
            if(Constants.notificationMap!=null && Constants.notificationMap.get(Constants.KEY_MESSAGE)!=null && Constants.notificationMap.get(Constants.KEY_MESSAGE).size()>0)
            {
                messageBubble.setVisibility(View.VISIBLE);
                messageBubble.setText(""+Constants.notificationMap.get(Constants.KEY_MESSAGE).size());
            }
            else
                messageBubble.setVisibility(View.GONE);
        }
        if (hazardBubble != null)
        {
            if(Constants.notificationMap!=null && Constants.notificationMap.get(Constants.KEY_HAZARD)!=null && Constants.notificationMap.get(Constants.KEY_HAZARD).size()>0)
            {
                hazardBubble.setVisibility(View.VISIBLE);
                hazardBubble.setText(""+Constants.notificationMap.get(Constants.KEY_HAZARD).size());
            }
            else
                hazardBubble.setVisibility(View.GONE);
        }
        if (reminderBubble != null)
        {
            if(Constants.notificationMap!=null && Constants.notificationMap.get(Constants.KEY_REMINDER)!=null && Constants.notificationMap.get(Constants.KEY_REMINDER).size()>0)
            {
                reminderBubble.setVisibility(View.VISIBLE);
                reminderBubble.setText(""+Constants.notificationMap.get(Constants.KEY_REMINDER).size());
            }
            else
                reminderBubble.setVisibility(View.GONE);
        }
        if (activityBubble != null)
        {
            if(Constants.notificationMap!=null && Constants.notificationMap.get(Constants.KEY_ACTIVITY)!=null && Constants.notificationMap.get(Constants.KEY_ACTIVITY).size()>0)
            {
                activityBubble.setVisibility(View.VISIBLE);
                activityBubble.setText(""+Constants.notificationMap.get(Constants.KEY_ACTIVITY).size());
            }
            else
                activityBubble.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onDestroy()
    {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationReceiver);
        super.onDestroy();
    }
}

