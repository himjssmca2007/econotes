package com.econote;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.econote.utils.AppUtil;
import com.econote.utils.Constants;
import com.econote.utils.GlobalAlertDialogs;
import com.econote.utils.GlobalTracker;
import com.econote.utils.HttpCall;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.Arrays;


public class RegisterActivity extends FragmentActivity implements GoogleApiClient.OnConnectionFailedListener,GoogleApiClient.ConnectionCallbacks {

    private Context context;
    private EditText email_et,password_et,confirm_password,mobile,fullname,city;
    private ImageView fbbutton;
    private ImageView googlebutton;
    public static CallbackManager callbackmanager;
    private Button register;
    GoogleApiClient mGoogleApiClient;
    private static final int RC_SIGN_IN = 9001;
    private TextView signIn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        context = this;
        GlobalTracker.setScreen("RegisterScreen");
        email_et = (EditText) findViewById(R.id.email);
        password_et = (EditText) findViewById(R.id.password);
        confirm_password=(EditText)findViewById(R.id.confirm_password);
        fullname=(EditText)findViewById(R.id.fullname);
        city=(EditText)findViewById(R.id.city);
        mobile=(EditText)findViewById(R.id.mobile);
        register = (Button) findViewById(R.id.register);
        signIn=(TextView)findViewById(R.id.signIn);
        fbbutton = (ImageView) findViewById(R.id.fb_login_button);
        googlebutton = (ImageView) findViewById(R.id.google_login_button);
        fbbutton.setOnClickListener(mClickListener);
        googlebutton.setOnClickListener(mClickListener);
        register.setOnClickListener(mClickListener);
        signIn.setOnClickListener(mClickListener);
        callbackmanager = CallbackManager.Factory.create();
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestProfile()
                .requestScopes(new Scope(Scopes.PLUS_ME))
                .requestScopes(new Scope(Scopes.PLUS_LOGIN))
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, RegisterActivity.this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();


    }

    OnClickListener mClickListener=new OnClickListener()
    {
        @Override
        public void onClick(View v) {

            switch (v.getId())
            {
                case R.id.fb_login_button:
                    onFblogin();
                    break;
                case R.id.google_login_button:
                    signIn();
                    break;
                case R.id.register:
                    if(fullname.getText().toString().trim().length()==0)
                    {
                        GlobalAlertDialogs.createAlertSingle(RegisterActivity.this, "Please enter full name.", "OK", false);
                    }
                    else if(email_et.getText().toString().trim().length()==0)
                    {
                        GlobalAlertDialogs.createAlertSingle(RegisterActivity.this, "Please enter email.", "OK", false);
                    }
                    else if(!Constants.validateEmail(email_et.getText().toString().trim()))
                    {
                        GlobalAlertDialogs.createAlertSingle(RegisterActivity.this,"Please enter valid email.","OK",false);
                    }
                    else if(password_et.getText().toString().trim().length()==0)
                    {
                        GlobalAlertDialogs.createAlertSingle(RegisterActivity.this,"Please enter password.","OK",false);
                    }
                    else if(confirm_password.getText().toString().trim().length()==0)
                    {
                        GlobalAlertDialogs.createAlertSingle(RegisterActivity.this,"Please enter confirm password.","OK",false);
                    }
                    else if(!password_et.getText().toString().trim().equals(confirm_password.getText().toString().trim()))
                    {
                        GlobalAlertDialogs.createAlertSingle(RegisterActivity.this,"Password and confirm password are not same.","OK",false);
                    }
                    else if(city.getText().toString().trim().length()==0)
                    {
                        GlobalAlertDialogs.createAlertSingle(RegisterActivity.this,"Please enter city.","OK",false);
                    }
                    else if(mobile.getText().toString().trim().length()==0)
                    {
                        GlobalAlertDialogs.createAlertSingle(RegisterActivity.this,"Please enter mobile no.","OK",false);
                    }
                    else
                    {
                        if(Constants.isNetworkAvailable(context))
                        {
                            String data=createRegisterRequestData(fullname.getText().toString().trim(),email_et.getText().toString().trim(),password_et.getText().toString().trim(),mobile.getText().toString().trim(),city.getText().toString().trim());
                            new RegisterTask().execute(data);
                        }
                        else
                            Constants.ShowNetworkError(context);
                    }
                    break;
                case R.id.signIn:
                    Intent i=new Intent(context,LoginActivity.class);
                    startActivity(i);
                    finish();
                    break;
            }

        }
    };

    // Private method to handle Facebook login and callback
    private void onFblogin()
    {
        LoginManager.getInstance().logInWithReadPermissions(this,Arrays.asList("email","public_profile"));

        LoginManager.getInstance().registerCallback(callbackmanager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult)
                    {

                        System.out.println("Success");
                        GraphRequest request =GraphRequest.newMeRequest(
                                loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject json, GraphResponse response) {
                                        if (response.getError() != null) {
                                            // handle error
                                            System.out.println("ERROR");
                                        } else {
                                            System.out.println("Success");
                                            String id = json.optString("id", "");
                                            String name = json.optString("name", "");
                                            String email = json.optString("email", "");
                                            if(Constants.isNetworkAvailable(context))
                                            {
                                                String data = createFacebookRequestData(name,id,"facebook",email);
                                                new SocialLoginTask().execute(data);
                                            }
                                            else
                                                Constants.ShowNetworkError(context);
                                        }
                                    }

                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields","id,name,email"); // Parámetros que pedimos a facebook
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel()
                    {
                        Log.d("TAG_CANCEL", "On cancel");
                    }

                    @Override
                    public void onError(FacebookException error) {
                        Log.d("TAG_ERROR", error.toString());
                    }
                });
    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN)
        {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
        else if (callbackmanager != null)
        {
            callbackmanager.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void handleSignInResult(GoogleSignInResult result)
    {
        if (result.isSuccess())
        {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            if(Constants.isNetworkAvailable(context))
            {
                String data = createGmailRequestData(acct.getEmail(),acct.getDisplayName(), acct.getId(),"google");
                new SocialLoginTask().execute(data);
            }
            else
                Constants.ShowNetworkError(context);



        }
        else
        {
            GlobalAlertDialogs.createAlertSingle((RegisterActivity) context,"There is some problem in Google SignIn", "OK", false);
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }


    private String createRegisterRequestData(String name,String email,String password,String mobile,String city)
    {
        String data = "";
        try {
            data += URLEncoder.encode("email", "UTF-8")
                    + "=" + URLEncoder.encode(email, "UTF-8");

            data += "&" + URLEncoder.encode("password", "UTF-8") + "="
                    + URLEncoder.encode(password, "UTF-8");

            data += "&" + URLEncoder.encode("logintype", "UTF-8") + "="
                    + URLEncoder.encode("email", "UTF-8");

            data += "&" + URLEncoder.encode("passphase", "UTF-8") + "="
                    + URLEncoder.encode(Constants.PASSPHRASE, "UTF-8");

            data += "&" + URLEncoder.encode("auth", "UTF-8") + "="
                    + URLEncoder.encode(Constants.AUTHKEY, "UTF-8");

            data += "&" + URLEncoder.encode("action", "UTF-8") + "="
                    + URLEncoder.encode("register", "UTF-8");

            data += "&" + URLEncoder.encode("uname", "UTF-8") + "="
                    + URLEncoder.encode(name, "UTF-8");

            data += "&" + URLEncoder.encode("mobileno", "UTF-8") + "="
                    + URLEncoder.encode(mobile, "UTF-8");

            Log.e("Himanshu", "Dixit Facebook Data" + data);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return data;
    }

    private String createGmailRequestData(String email,String name,String id,String loginType)
    {
        String data = "";
        try {
            data += URLEncoder.encode("google_id", "UTF-8")
                    + "=" + URLEncoder.encode(id, "UTF-8");

            data += "&" + URLEncoder.encode("logintype", "UTF-8") + "="
                    + URLEncoder.encode(loginType, "UTF-8");

            data += "&" + URLEncoder.encode("passphase", "UTF-8") + "="
                    + URLEncoder.encode(Constants.PASSPHRASE, "UTF-8");

            data += "&" + URLEncoder.encode("auth", "UTF-8") + "="
                    + URLEncoder.encode(Constants.AUTHKEY, "UTF-8");

            data += "&" + URLEncoder.encode("action", "UTF-8") + "="
                    + URLEncoder.encode("register", "UTF-8");

            data += "&" + URLEncoder.encode("uname", "UTF-8") + "="
                    + URLEncoder.encode(name, "UTF-8");

            data += "&" + URLEncoder.encode("email", "UTF-8") + "="
                    + URLEncoder.encode(email, "UTF-8");

            Log.e("Himanshu", "Dixit Facebook Data" + data);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return data;
    }


    private String createFacebookRequestData(String name,String id,String loginType,String email)
    {
        String data = "";
        try {
            data += URLEncoder.encode("facebook_id", "UTF-8")
                    + "=" + URLEncoder.encode(id, "UTF-8");

            data += "&" + URLEncoder.encode("logintype", "UTF-8") + "="
                    + URLEncoder.encode(loginType, "UTF-8");

            data += "&" + URLEncoder.encode("passphase", "UTF-8") + "="
                    + URLEncoder.encode(Constants.PASSPHRASE, "UTF-8");

            data += "&" + URLEncoder.encode("auth", "UTF-8") + "="
                    + URLEncoder.encode(Constants.AUTHKEY, "UTF-8");

            data += "&" + URLEncoder.encode("action", "UTF-8") + "="
                    + URLEncoder.encode("register", "UTF-8");

            data += "&" + URLEncoder.encode("uname", "UTF-8") + "="
                    + URLEncoder.encode(name, "UTF-8");

            data += "&" + URLEncoder.encode("email", "UTF-8") + "="
                    + URLEncoder.encode(email, "UTF-8");
            Log.e("Himanshu", "Dixit Facebook Data" + data);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return data;
    }


    private class RegisterTask extends AsyncTask<String, Void, String> {

        protected void onPreExecute()
        {
            Constants.showProgressDialog(context);
        }

        @Override
        protected String doInBackground(String... params)
        {
            String url = Constants.GLOBAL_URL;
            HttpCall httpCall = new HttpCall();
            String resp = httpCall.executeHttpPostRequest(url,params[0]);
            return resp;
        }

        protected void onPostExecute(String resp) {
            if (resp != null) {
                try {
                    Log.e("Himanshu","Dixit Response"+resp);
                    JSONObject jo = new JSONObject(resp);
                    boolean status = jo.getBoolean("result");
                    if (status)
                    {
                        Constants.hideProgressDialog();
                        AppUtil.setUserId(context, jo.optString("data", ""));
                        createAlertLogin();
                    }
                    else
                    {
                        Constants.hideProgressDialog();
                        GlobalAlertDialogs.createAlertSingle((RegisterActivity) context, jo.optString("msg"), "OK", false);
                    }
                } catch (Exception e) {
                    Constants.hideProgressDialog();
                    GlobalAlertDialogs.createAlertSingle((RegisterActivity) context, Constants.SERVER_EXCEPTION_MSG, "OK", false);
                }
            } else {
                Constants.hideProgressDialog();
                GlobalAlertDialogs.createAlertSingle((RegisterActivity) context, "Timeout From Server.Please try again.", "OK", false);
            }
        }
    }


    private class SocialLoginTask extends AsyncTask<String, Void, String> {

        protected void onPreExecute()
        {
            Constants.showProgressDialog(context);
        }

        @Override
        protected String doInBackground(String... params)
        {
            String url = Constants.GLOBAL_URL;
            HttpCall httpCall = new HttpCall();
            String resp = httpCall.executeHttpPostRequest(url,params[0]);
            return resp;
        }

        protected void onPostExecute(String resp) {
            if (resp != null) {
                try {
                    Log.e("Himanshu","Dixit Response"+resp);
                    JSONObject jo = new JSONObject(resp);
                    boolean status = jo.getBoolean("result");
                    if (status)
                    {
                        Constants.hideProgressDialog();
                        AppUtil.setUserId(context, jo.optString("data", ""));
                        Intent intent = new Intent(RegisterActivity.this,MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                    else
                    {
                        Constants.hideProgressDialog();
                        GlobalAlertDialogs.createAlertSingle((RegisterActivity) context, jo.optString("msg"), "OK", false);
                    }
                } catch (Exception e) {
                    Constants.hideProgressDialog();
                    GlobalAlertDialogs.createAlertSingle((RegisterActivity) context, Constants.SERVER_EXCEPTION_MSG, "OK", false);
                }
            } else {
                Constants.hideProgressDialog();
                GlobalAlertDialogs.createAlertSingle((RegisterActivity) context, "Timeout From Server.Please try again.", "OK", false);
            }
        }
    }


    public void createAlertLogin() {
        final Dialog dialog = new Dialog(context);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.alert_dialog);
        TextView msg = (TextView) dialog.findViewById(R.id.msg);
        msg.setText("You have successfully registered on Econote.Please login to continue.");
        Button btnCancel = (Button) dialog.findViewById(R.id.cancel);
        btnCancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        Button btnConfirm = (Button) dialog.findViewById(R.id.confirm);
        btnConfirm.setText("OK");
        btnConfirm.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent = new Intent(RegisterActivity.this,LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });
        dialog.show();
    }
}

