package com.econote;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.econote.model.JoinProgramModel;
import com.econote.utils.Constants;
import com.econote.utils.GlobalAlertDialogs;
import com.econote.utils.GlobalTracker;
import com.econote.utils.HttpCall;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONObject;

import java.util.ArrayList;


public class SoundCheckActivity extends AppCompatActivity {

    private Context context;
    private TextView submit;
    private Toolbar toolbar;
    private TextView messageBubble,hazardBubble,reminderBubble,activityBubble;
    private JoinProgramModel model;
    private String selectedValue;
    private RadioGroup radio_group;
    private ImageView site_pic;
    private EditText testing_time,noise_level,other_text;
    private ArrayList<String> checkList;
    private String noiseLevel,testTime;
    private CheckBox other,wheelers,passenger_vehicle,small_vehicle,large_vehicle,generators,construction_machinery,construction_activity,industrial_activity,loudspeakers,passenger_aircrafts,railway_activities,crackers,religious_activities,honking;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sound_check);
        context = this;
        model=getIntent().getParcelableExtra("model");
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        prepareActionBar(toolbar);
        GlobalTracker.setScreen("SoundCheckScreen");
        checkList=new ArrayList<>();
        site_pic=(ImageView)findViewById(R.id.site_pic);
        if(model.getImageList()!=null && model.getImageList().size()>0)
            ImageLoader.getInstance().displayImage("file:///"+model.getImageList().get(0),site_pic,Constants.options);
        submit=(TextView)findViewById(R.id.submit);
        submit.setOnClickListener(mClickListener);

        wheelers=(CheckBox)findViewById(R.id.wheelers);
        wheelers.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b)
                    checkList.add(wheelers.getText().toString());
                else
                    checkList.remove(wheelers.getText().toString());
            }
        });
        passenger_vehicle=(CheckBox)findViewById(R.id.passenger_vehicle);
        passenger_vehicle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b)
                    checkList.add(passenger_vehicle.getText().toString());
                else
                    checkList.remove(passenger_vehicle.getText().toString());
            }
        });
        small_vehicle=(CheckBox)findViewById(R.id.small_vehicle);
        small_vehicle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b)
                    checkList.add(small_vehicle.getText().toString());
                else
                    checkList.remove(small_vehicle.getText().toString());
            }
        });
        large_vehicle=(CheckBox)findViewById(R.id.large_vehicle);
        large_vehicle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b)
                    checkList.add(large_vehicle.getText().toString());
                else
                    checkList.remove(large_vehicle.getText().toString());
            }
        });
        generators=(CheckBox)findViewById(R.id.generators);
        generators.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b)
                    checkList.add(generators.getText().toString());
                else
                    checkList.remove(generators.getText().toString());
            }
        });
        construction_machinery=(CheckBox)findViewById(R.id.construction_machinery);
        construction_machinery.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b)
                    checkList.add(construction_machinery.getText().toString());
                else
                    checkList.remove(construction_machinery.getText().toString());
            }
        });
        construction_activity=(CheckBox)findViewById(R.id.construction_activity);
        construction_activity.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b)
                    checkList.add(construction_activity.getText().toString());
                else
                    checkList.remove(construction_activity.getText().toString());
            }
        });
        industrial_activity=(CheckBox)findViewById(R.id.industrial_activity);
        industrial_activity.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b)
                    checkList.add(industrial_activity.getText().toString());
                else
                    checkList.remove(industrial_activity.getText().toString());
            }
        });
        loudspeakers=(CheckBox)findViewById(R.id.loudspeakers);
        loudspeakers.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b)
                    checkList.add(loudspeakers.getText().toString());
                else
                    checkList.remove(loudspeakers.getText().toString());
            }
        });
        passenger_aircrafts=(CheckBox)findViewById(R.id.passenger_aircrafts);
        passenger_aircrafts.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b)
                    checkList.add(passenger_aircrafts.getText().toString());
                else
                    checkList.remove(passenger_aircrafts.getText().toString());
            }
        });
        railway_activities=(CheckBox)findViewById(R.id.railway_activities);
        railway_activities.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b)
                    checkList.add(railway_activities.getText().toString());
                else
                    checkList.remove(railway_activities.getText().toString());
            }
        });
        crackers=(CheckBox)findViewById(R.id.crackers);
        crackers.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b)
                    checkList.add(crackers.getText().toString());
                else
                    checkList.remove(crackers.getText().toString());
            }
        });
        religious_activities=(CheckBox)findViewById(R.id.religious_activities);
        religious_activities.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b)
                    checkList.add(religious_activities.getText().toString());
                else
                    checkList.remove(religious_activities.getText().toString());
            }
        });
        honking=(CheckBox)findViewById(R.id.honking);
        honking.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b)
                    checkList.add(honking.getText().toString());
                else
                    checkList.remove(honking.getText().toString());
            }
        });
        other_text=(EditText)findViewById(R.id.other_text);
        other=(CheckBox)findViewById(R.id.other);
        other.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b)
                {
                    checkList.add(other.getText().toString());
                    other_text.setVisibility(View.VISIBLE);
                }
                else
                {
                    checkList.remove(other.getText().toString());
                    other_text.setVisibility(View.GONE);
                    other_text.setText("");
                }
            }
        });
        testing_time=(EditText)findViewById(R.id.testing_time);
        noise_level=(EditText)findViewById(R.id.noise_level);
        radio_group=(RadioGroup)findViewById(R.id.radio_group);
        radio_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (i == R.id.industrial) {
                    RadioButton radio = (RadioButton) findViewById(R.id.industrial);
                    selectedValue = radio.getText().toString().trim();
                } else if (i == R.id.commercial) {
                    RadioButton radio = (RadioButton) findViewById(R.id.commercial);
                    selectedValue = radio.getText().toString().trim();
                }else if (i == R.id.residential) {
                    RadioButton radio = (RadioButton) findViewById(R.id.residential);
                    selectedValue = radio.getText().toString().trim();
                }else if (i == R.id.silence) {
                    RadioButton radio = (RadioButton) findViewById(R.id.silence);
                    selectedValue = radio.getText().toString().trim();
                }
            }
        });
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationReceiver, new IntentFilter(Constants.INTENT_FILTER_NOTIFICATION));
    }

    private void prepareActionBar(Toolbar toolbar)
    {
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
    }



    OnClickListener mClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {

            switch (v.getId())
            {
                case R.id.submit:
                    Log.e("Himanshu","Dixit Sound Source String"+createSoundSourceString());
                    if(checkList.size()==0)
                    {
                        GlobalAlertDialogs.createAlertSingle(SoundCheckActivity.this, "Please select any source of pollution?", "OK", false);
                    }
                    else if(other.isChecked() && other_text.getText().toString().trim().length()==0)
                    {
                        GlobalAlertDialogs.createAlertSingle(SoundCheckActivity.this, "Please enter the name of other source of pollution?", "OK", false);
                    }
                    else if(noise_level.getText().toString().trim().length()==0)
                    {
                        GlobalAlertDialogs.createAlertSingle(SoundCheckActivity.this, "Please enter Max recorded noise level.", "OK", false);
                    }
                    else if(testing_time.getText().toString().trim().length()==0)
                    {
                        GlobalAlertDialogs.createAlertSingle(SoundCheckActivity.this, "Please enter time of testing.", "OK", false);
                    }
                    else if(TextUtils.isEmpty(selectedValue))
                    {
                        GlobalAlertDialogs.createAlertSingle(SoundCheckActivity.this, "Please select where was the test done?", "OK", false);
                    }
                    else
                    {
                        if(Constants.isNetworkAvailable(context))
                        {
                            noiseLevel=noise_level.getText().toString().trim();
                            testTime=testing_time.getText().toString().trim();
                            new SubmitProgramTask().execute(model);
                        }
                        else
                            Constants.ShowNetworkError(context);
                    }
                    break;
            }

        }
    };


    private class SubmitProgramTask extends AsyncTask<JoinProgramModel, Void, String> {

        JoinProgramModel joinProgram;
        protected void onPreExecute()
        {
            Constants.showProgressDialog(context);
        }

        @Override
        protected String doInBackground(JoinProgramModel... params)
        {
            joinProgram=params[0];
            String source=createSoundSourceString();
            HttpCall httpCall = new HttpCall();
            String resp = httpCall.doJoinProgramSubmitSound(Constants.GLOBAL_URL,joinProgram,source,noiseLevel,testTime,selectedValue);
            return resp;
        }

        protected void onPostExecute(String resp)
        {
            noiseLevel="";
            testTime="";
            if (resp != null)
            {
                try {
                    JSONObject jo = new JSONObject(resp);
                    boolean status = jo.getBoolean("result");
                    if (status)
                    {
                        Constants.hideProgressDialog();
                        Intent i=new Intent();
                        i.putExtra("message",jo.optString("msg"));
                        setResult(RESULT_OK, i);
                        finish();
                    }
                    else
                    {
                        Constants.hideProgressDialog();
                        GlobalAlertDialogs.createAlertSingle(SoundCheckActivity.this, jo.optString("msg"), "OK", false);
                    }
                } catch (Exception e) {
                    Constants.hideProgressDialog();
                    GlobalAlertDialogs.createAlertSingle(SoundCheckActivity.this, Constants.SERVER_EXCEPTION_MSG, "OK", false);
                }
            } else {
                Constants.hideProgressDialog();
                GlobalAlertDialogs.createAlertSingle(SoundCheckActivity.this, "Timeout From Server.Please try again.", "OK", false);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        MenuItem item = menu.findItem(R.id.action_chat);
        RelativeLayout messageView = (RelativeLayout) item.getActionView();
        messageBubble = (TextView) messageView.findViewById(R.id.notification_count);
        messageView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(context,NotificationActivity.class);
                startActivity(i);
            }
        });

        item = menu.findItem(R.id.action_haz);
        RelativeLayout hazardView = (RelativeLayout) item.getActionView();
        hazardBubble = (TextView) hazardView.findViewById(R.id.notification_count);
        hazardView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(context,NotificationActivity.class);
                startActivity(i);
            }
        });

        item = menu.findItem(R.id.action_alarm);
        RelativeLayout reminderView = (RelativeLayout) item.getActionView();
        reminderBubble = (TextView) reminderView.findViewById(R.id.notification_count);
        reminderView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(context,NotificationActivity.class);
                startActivity(i);
            }
        });

        item = menu.findItem(R.id.action_notif);
        RelativeLayout activityView = (RelativeLayout) item.getActionView();
        activityBubble = (TextView) activityView.findViewById(R.id.notification_count);
        activityView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(context,NotificationActivity.class);
                startActivity(i);
            }
        });

        showNotificationCount();
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_map:
            {
                Intent i=new Intent(SoundCheckActivity.this,MainActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                finish();
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private BroadcastReceiver mNotificationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            showNotificationCount();
        }
    };

    private void showNotificationCount() {

        if (messageBubble != null)
        {
            if(Constants.notificationMap!=null && Constants.notificationMap.get(Constants.KEY_MESSAGE)!=null && Constants.notificationMap.get(Constants.KEY_MESSAGE).size()>0)
            {
                messageBubble.setVisibility(View.VISIBLE);
                messageBubble.setText(""+Constants.notificationMap.get(Constants.KEY_MESSAGE).size());
            }
            else
                messageBubble.setVisibility(View.GONE);
        }
        if (hazardBubble != null)
        {
            if(Constants.notificationMap!=null && Constants.notificationMap.get(Constants.KEY_HAZARD)!=null && Constants.notificationMap.get(Constants.KEY_HAZARD).size()>0)
            {
                hazardBubble.setVisibility(View.VISIBLE);
                hazardBubble.setText(""+Constants.notificationMap.get(Constants.KEY_HAZARD).size());
            }
            else
                hazardBubble.setVisibility(View.GONE);
        }
        if (reminderBubble != null)
        {
            if(Constants.notificationMap!=null && Constants.notificationMap.get(Constants.KEY_REMINDER)!=null && Constants.notificationMap.get(Constants.KEY_REMINDER).size()>0)
            {
                reminderBubble.setVisibility(View.VISIBLE);
                reminderBubble.setText(""+Constants.notificationMap.get(Constants.KEY_REMINDER).size());
            }
            else
                reminderBubble.setVisibility(View.GONE);
        }
        if (activityBubble != null)
        {
            if(Constants.notificationMap!=null && Constants.notificationMap.get(Constants.KEY_ACTIVITY)!=null && Constants.notificationMap.get(Constants.KEY_ACTIVITY).size()>0)
            {
                activityBubble.setVisibility(View.VISIBLE);
                activityBubble.setText(""+Constants.notificationMap.get(Constants.KEY_ACTIVITY).size());
            }
            else
                activityBubble.setVisibility(View.GONE);
        }
    }


    private String createSoundSourceString()
    {
        String data="";
        if(checkList!=null && checkList.size()>0)
        {
            for(int i=0;i<checkList.size();i++)
            {
                data=data+checkList.get(i)+"##";
            }
            if(other.isChecked())
            {
                data=data+other_text.getText().toString().trim();
            }
            else if(!TextUtils.isEmpty(data))
            {
                data=data.substring(0,data.length()-2);
            }
        }
        return data;
    }

    @Override
    public void onBackPressed()
    {
        Intent i=new Intent();
        setResult(RESULT_CANCELED,i);
        finish();
    }

    @Override
    protected void onDestroy()
    {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationReceiver);
        super.onDestroy();
    }

}

