package com.econote;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.econote.services.GetIssueCategory;
import com.econote.utils.AppUtil;
import com.econote.utils.Constants;
import com.econote.utils.GlobalAlertDialogs;
import com.econote.utils.GlobalTracker;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import java.net.URLEncoder;


public class SplashActivity extends AppCompatActivity {

    private Context context;
    private boolean timerEnd,categoryEnd;
    private Button close_popup,req_again;
    private SlidingUpPanelLayout mLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        context = this;
        GlobalTracker.setScreen("SplashScreen");
        mLayout = (SlidingUpPanelLayout) findViewById(R.id.sliding_layout);
        close_popup=(Button)findViewById(R.id.close_popup);
        req_again=(Button)findViewById(R.id.req_again);
        close_popup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                finish();
            }
        });
        req_again.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                {
                    if(Constants.checkMarshmallowPermission(context, SplashActivity.this))
                        appEntryPoint();
                }
            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            if(Constants.checkMarshmallowPermission(context, SplashActivity.this))
                appEntryPoint();
        }
        else
            appEntryPoint();

    }


    private void appEntryPoint()
    {
        if (Constants.isNetworkAvailable(context))
        {
            Intent intent = new Intent(context, GetIssueCategory.class);
            intent.putExtra("url", Constants.GLOBAL_URL);
            intent.putExtra("data", createCategoryRequestData());
            intent.putExtra("receiver", new MyReceiver(new Handler()));
            startService(intent);
            startApplication();
        } else
            GlobalAlertDialogs.createAlertSingle(SplashActivity.this, "Please check your Internet Connection!", "OK", true);
    }

    private void startApplication()
    {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run()
            {
                timerEnd=true;
                navigateLogin();
            }
        },1000);
    }


    class MyReceiver extends ResultReceiver
    {
        public MyReceiver(Handler handler)
        {
            super(handler);
        }


        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            super.onReceiveResult(resultCode, resultData);
            if (resultCode == Constants.REQUEST_ISSUE_CATEGORY)
            {
                boolean success = resultData.getBoolean("success", false);
                String responseMessage = resultData.getString("response_message", "");
                if(success)
                {
                    categoryEnd=true;
                    navigateLogin();
                }
                else
                {
                    GlobalAlertDialogs.createAlertSingle(SplashActivity.this,responseMessage,"OK",true);
                }
            }
        }
    }


    private String createCategoryRequestData() {
        String data = "";
        try {
            data += URLEncoder.encode("action", "UTF-8")
                    + "=" + URLEncoder.encode("getCategory", "UTF-8");

            data += "&" + URLEncoder.encode("passphase", "UTF-8") + "="
                    + URLEncoder.encode(Constants.PASSPHRASE, "UTF-8");

            data += "&" + URLEncoder.encode("auth", "UTF-8") + "="
                    + URLEncoder.encode(Constants.AUTHKEY, "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return data;
    }

    private void navigateLogin()
    {
        if(timerEnd && categoryEnd)
        {
            if(AppUtil.isLoggedIn(context))
            {
                Intent i = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(i);
                finish();
            }
            else {
                Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                startActivity(i);
                finish();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(!Constants.checkMarshmallowPermissionGranted(requestCode, permissions, grantResults))
        {
            mLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
        }
        else
            appEntryPoint();
    }
}

