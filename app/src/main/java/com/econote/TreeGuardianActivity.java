package com.econote;


import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.econote.model.JoinProgramModel;
import com.econote.model.ReportIssueModel;
import com.econote.utils.Constants;
import com.econote.utils.GlobalAlertDialogs;
import com.econote.utils.GlobalTracker;
import com.econote.utils.HttpCall;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONObject;


public class TreeGuardianActivity extends AppCompatActivity {

    private Context context;
    private TextView submit;
    private Toolbar toolbar;
    private TextView messageBubble,hazardBubble,reminderBubble,activityBubble;
    private JoinProgramModel model;
    private String selectedValue;
    private RadioGroup radio_group;
    private ImageView site_pic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tree_guardian);
        context = this;
        model=getIntent().getParcelableExtra("model");
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        prepareActionBar(toolbar);
        GlobalTracker.setScreen("TreeGuardianScreen");
        site_pic=(ImageView)findViewById(R.id.site_pic);
        if(model.getImageList()!=null && model.getImageList().size()>0)
            ImageLoader.getInstance().displayImage("file:///"+model.getImageList().get(0),site_pic,Constants.options);
        radio_group=(RadioGroup)findViewById(R.id.radio_group);
        radio_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (i == R.id.healthy) {
                    RadioButton radio = (RadioButton) findViewById(R.id.healthy);
                    selectedValue = radio.getText().toString().trim();
                } else if (i == R.id.sick) {
                    RadioButton radio = (RadioButton) findViewById(R.id.sick);
                    selectedValue = radio.getText().toString().trim();
                } else if (i == R.id.trimmed) {
                    RadioButton radio = (RadioButton) findViewById(R.id.trimmed);
                    selectedValue = radio.getText().toString().trim();
                } else if (i == R.id.chopped) {
                    RadioButton radio = (RadioButton) findViewById(R.id.chopped);
                    selectedValue = radio.getText().toString().trim();
                }
            }
        });
        submit=(TextView)findViewById(R.id.submit);
        submit.setOnClickListener(mClickListener);
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationReceiver, new IntentFilter(Constants.INTENT_FILTER_NOTIFICATION));
    }

    private void prepareActionBar(Toolbar toolbar)
    {
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
    }



    OnClickListener mClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {

            switch (v.getId())
            {
                case R.id.submit:
                    if(TextUtils.isEmpty(selectedValue))
                    {
                        GlobalAlertDialogs.createAlertSingle(TreeGuardianActivity.this, "Please select Current Status", "OK", false);
                    }
                    else
                    {
                        if(Constants.isNetworkAvailable(context))
                        {
                            new SubmitProgramTask().execute(model);
                        }
                        else
                            Constants.ShowNetworkError(context);
                    }
                    break;
            }

        }
    };


    private class SubmitProgramTask extends AsyncTask<JoinProgramModel, Void, String> {

        JoinProgramModel joinProgram;
        protected void onPreExecute()
        {
            Constants.showProgressDialog(context);
        }

        @Override
        protected String doInBackground(JoinProgramModel... params)
        {
            joinProgram=params[0];
            HttpCall httpCall = new HttpCall();
            String resp = httpCall.doJoinProgramSubmitTree(Constants.GLOBAL_URL, joinProgram, selectedValue);
            return resp;
        }

        protected void onPostExecute(String resp) {
            if (resp != null) {
                try {
                    JSONObject jo = new JSONObject(resp);
                    boolean status = jo.getBoolean("result");
                    if (status)
                    {
                        Constants.hideProgressDialog();
                        Intent i=new Intent();
                        i.putExtra("message",jo.optString("msg"));
                        setResult(RESULT_OK, i);
                        finish();
                    }
                    else
                    {
                        Constants.hideProgressDialog();
                        GlobalAlertDialogs.createAlertSingle(TreeGuardianActivity.this, jo.optString("msg"), "OK", false);
                    }
                } catch (Exception e) {
                    Constants.hideProgressDialog();
                    GlobalAlertDialogs.createAlertSingle(TreeGuardianActivity.this, Constants.SERVER_EXCEPTION_MSG, "OK", false);
                }
            } else {
                Constants.hideProgressDialog();
                GlobalAlertDialogs.createAlertSingle(TreeGuardianActivity.this, "Timeout From Server.Please try again.", "OK", false);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        MenuItem item = menu.findItem(R.id.action_chat);
        RelativeLayout messageView = (RelativeLayout) item.getActionView();
        messageBubble = (TextView) messageView.findViewById(R.id.notification_count);
        messageView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(context,NotificationActivity.class);
                startActivity(i);
            }
        });

        item = menu.findItem(R.id.action_haz);
        RelativeLayout hazardView = (RelativeLayout) item.getActionView();
        hazardBubble = (TextView) hazardView.findViewById(R.id.notification_count);
        hazardView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(context,NotificationActivity.class);
                startActivity(i);
            }
        });

        item = menu.findItem(R.id.action_alarm);
        RelativeLayout reminderView = (RelativeLayout) item.getActionView();
        reminderBubble = (TextView) reminderView.findViewById(R.id.notification_count);
        reminderView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(context,NotificationActivity.class);
                startActivity(i);
            }
        });

        item = menu.findItem(R.id.action_notif);
        RelativeLayout activityView = (RelativeLayout) item.getActionView();
        activityBubble = (TextView) activityView.findViewById(R.id.notification_count);
        activityView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(context,NotificationActivity.class);
                startActivity(i);
            }
        });

        showNotificationCount();
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_map:
            {
                Intent i=new Intent(TreeGuardianActivity.this,MainActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                finish();
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private BroadcastReceiver mNotificationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            showNotificationCount();
        }
    };

    private void showNotificationCount() {

        if (messageBubble != null) {
            if (Constants.notificationMap != null && Constants.notificationMap.get(Constants.KEY_MESSAGE) != null && Constants.notificationMap.get(Constants.KEY_MESSAGE).size() > 0) {
                messageBubble.setVisibility(View.VISIBLE);
                messageBubble.setText("" + Constants.notificationMap.get(Constants.KEY_MESSAGE).size());
            } else
                messageBubble.setVisibility(View.GONE);
        }
        if (hazardBubble != null) {
            if (Constants.notificationMap != null && Constants.notificationMap.get(Constants.KEY_HAZARD) != null && Constants.notificationMap.get(Constants.KEY_HAZARD).size() > 0) {
                hazardBubble.setVisibility(View.VISIBLE);
                hazardBubble.setText("" + Constants.notificationMap.get(Constants.KEY_HAZARD).size());
            } else
                hazardBubble.setVisibility(View.GONE);
        }
        if (reminderBubble != null) {
            if (Constants.notificationMap != null && Constants.notificationMap.get(Constants.KEY_REMINDER) != null && Constants.notificationMap.get(Constants.KEY_REMINDER).size() > 0) {
                reminderBubble.setVisibility(View.VISIBLE);
                reminderBubble.setText("" + Constants.notificationMap.get(Constants.KEY_REMINDER).size());
            } else
                reminderBubble.setVisibility(View.GONE);
        }
        if (activityBubble != null) {
            if (Constants.notificationMap != null && Constants.notificationMap.get(Constants.KEY_ACTIVITY) != null && Constants.notificationMap.get(Constants.KEY_ACTIVITY).size() > 0) {
                activityBubble.setVisibility(View.VISIBLE);
                activityBubble.setText("" + Constants.notificationMap.get(Constants.KEY_ACTIVITY).size());
            } else
                activityBubble.setVisibility(View.GONE);
        }
    }


    @Override
    public void onBackPressed()
    {
        Intent i=new Intent();
        setResult(RESULT_CANCELED, i);
        finish();
    }

    @Override
    protected void onDestroy()
    {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationReceiver);
        super.onDestroy();
    }
}

