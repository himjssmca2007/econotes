package com.econote.adapter;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.econote.R;
import com.econote.model.NotificationModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class NotificationListAdapter extends RecyclerView.Adapter<NotificationListAdapter.ViewHolder>{

    private ArrayList<NotificationModel> adapterList;
    private SimpleDateFormat dfParse=new SimpleDateFormat("yyyy-MM-dd");
    private SimpleDateFormat dfFormat=new SimpleDateFormat("EEE, MMM dd yyyy");
    public NotificationListAdapter(ArrayList<NotificationModel> news_list)
    {
        this.adapterList=news_list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_row, parent, false);
        ViewHolder yourViewHolder =  new ViewHolder(view);
        return yourViewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position)
    {
        final NotificationModel model=adapterList.get(position);
        if(!TextUtils.isEmpty(model.getStrong_heading()))
            holder.title.setText(model.getStrong_heading());
        else
            holder.title.setVisibility(View.GONE);
        holder.detail.setText(model.getDetail());
        holder.type.setText(model.getCategory());
    }

    @Override
    public int getItemCount() {

        if(adapterList!=null && adapterList.size()>0)
        {
            return adapterList.size();
        }
        else
            return 0;
    }



    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView title,type,detail;
        public ViewHolder(View itemView) {
            super(itemView);
            title=(TextView)itemView.findViewById(R.id.title);
            type=(TextView)itemView.findViewById(R.id.type);
            detail=(TextView)itemView.findViewById(R.id.detail);
        }
    }
}