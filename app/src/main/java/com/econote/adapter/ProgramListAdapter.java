package com.econote.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.econote.R;
import com.econote.model.IssueCategoryModel;
import com.econote.model.ProgramModel;
import com.econote.utils.Constants;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

public class ProgramListAdapter extends RecyclerView.Adapter<ProgramListAdapter.ViewHolder>{

    private  Context context;
    ImageLoader imageLoader = null;
    ArrayList<ProgramModel> adapterList;
    public ProgramListAdapter(Context context, ArrayList<ProgramModel> list)
    {
        this.context=context;
        this.adapterList=list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_program, parent, false);
        ViewHolder yourViewHolder =  new ViewHolder(view);
        return yourViewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position)
    {
        ProgramModel model=adapterList.get(position);
        holder.title.setText(model.getTitle());
        ImageLoader.getInstance().displayImage(model.getMobileapp_pic(), holder.image, Constants.options);
        holder.cost.setText("Rs. "+model.getCost());
        if(model.selected) {
            holder.root.setBackgroundColor(ContextCompat.getColor(context, R.color.grey_700));
            holder.title.setTextColor(ContextCompat.getColor(context, R.color.white));
        }
        else {
            holder.root.setBackgroundColor(Color.TRANSPARENT);
            holder.title.setTextColor(ContextCompat.getColor(context, R.color.grey_700));
        }
        holder.root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for(int i=0;i<adapterList.size();i++)
                {
                    adapterList.get(i).selected=false;
                }
                adapterList.get(position).selected=true;
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {

        if(adapterList!=null && adapterList.size()>0)
        {
            return adapterList.size();
        }
        else
            return 0;
    }



    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView image;
        private TextView title,cost;
        private LinearLayout root;
        public ViewHolder(View itemView) {
            super(itemView);
            imageLoader = ImageLoader.getInstance();
            image=(ImageView)itemView.findViewById(R.id.image);
            title=(TextView)itemView.findViewById(R.id.title);
            cost=(TextView)itemView.findViewById(R.id.cost);
            root=(LinearLayout)itemView.findViewById(R.id.root);
        }
    }
}