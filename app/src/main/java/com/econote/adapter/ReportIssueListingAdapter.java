package com.econote.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.econote.MainActivity;
import com.econote.R;
import com.econote.ResolvedIssueActivity;
import com.econote.model.IssueCategoryModel;
import com.econote.model.ReportIssueModel;
import com.econote.utils.Constants;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

public class ReportIssueListingAdapter extends RecyclerView.Adapter<ReportIssueListingAdapter.ViewHolder>{

    private  Context context;
    private DisplayImageOptions options;
    ImageLoader imageLoader = null;
    ArrayList<ReportIssueModel> adapterList;
    private MainActivity mActivity;
    public ReportIssueListingAdapter(Context context,MainActivity activity,ArrayList<ReportIssueModel> news_list)
    {
        this.context=context;
        this.adapterList=news_list;
        this.mActivity=activity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_report_issue, parent, false);
        ViewHolder yourViewHolder =  new ViewHolder(view);
        return yourViewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position)
    {
        ReportIssueModel model=adapterList.get(position);
        holder.title.setText(model.getTitle());
        if(!TextUtils.isEmpty(model.getTimestamp()))
        {
            holder.date.setVisibility(View.VISIBLE);
            holder.date.setText(model.getTimestamp());
        }
        else
            holder.date.setVisibility(View.GONE);
        if(model.getImageList()!=null && model.getImageList().size()>0)
            ImageLoader.getInstance().displayImage(model.getImageList().get(0), holder.image, Constants.options);
        if(adapterList.get(position).getTest_ID().equalsIgnoreCase("REPORT_A_SITE"))
            holder.action.setText("Report Status");
        else
            holder.action.setText("Go To Test");
        holder.action.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if(adapterList.get(position).getTest_ID().equalsIgnoreCase("REPORT_A_SITE"))
                {
                    Intent i = new Intent(context, ResolvedIssueActivity.class);
                    i.putExtra("model", adapterList.get(position));
                    context.startActivity(i);
                }
                else
                {
                    mActivity.callJoinProgramEdit(adapterList.get(position));
                }
            }
        });

    }

    @Override
    public int getItemCount() {

        if(adapterList!=null && adapterList.size()>0)
        {
            return adapterList.size();
        }
        else
            return 0;
    }



    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView image;
        private TextView title,date,action;
        private LinearLayout root;
        public ViewHolder(View itemView) {
            super(itemView);
            imageLoader = ImageLoader.getInstance();
            image=(ImageView)itemView.findViewById(R.id.image);
            title=(TextView)itemView.findViewById(R.id.title);
            root=(LinearLayout)itemView.findViewById(R.id.root);
            date=(TextView)itemView.findViewById(R.id.date);
            action=(TextView)itemView.findViewById(R.id.action);
        }
    }
}