package com.econote.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.econote.AudioPlaybackActivity;
import com.econote.AudioPlaybackListActivity;
import com.econote.R;
import com.econote.model.IssueCategoryModel;
import com.econote.model.VoiceModel;
import com.econote.utils.Constants;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

public class VoiceListingAdapter extends RecyclerView.Adapter<VoiceListingAdapter.ViewHolder>{

    private  Context context;
    ArrayList<VoiceModel> adapterList;
    public VoiceListingAdapter(Context context, ArrayList<VoiceModel> news_list)
    {
        this.context=context;
        this.adapterList=news_list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_voice, parent, false);
        ViewHolder yourViewHolder =  new ViewHolder(view);
        return yourViewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position)
    {
        VoiceModel model=adapterList.get(position);
        String existingFileName = model.getName().substring(model.getName().lastIndexOf('/') + 1,model.getName().length());
        holder.title.setText(existingFileName);
        holder.root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                Intent i = new Intent(context, AudioPlaybackListActivity.class);
                i.putExtra(AudioPlaybackActivity.FileNameArg,adapterList.get(position).getName());
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {

        if(adapterList!=null && adapterList.size()>0)
        {
            return adapterList.size();
        }
        else
            return 0;
    }



    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView title;
        private LinearLayout root;
        public ViewHolder(View itemView) {
            super(itemView);
            title=(TextView)itemView.findViewById(R.id.title);
            root=(LinearLayout)itemView.findViewById(R.id.root);
        }
    }
}