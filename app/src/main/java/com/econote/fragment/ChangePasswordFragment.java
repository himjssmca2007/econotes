package com.econote.fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.econote.MainActivity;
import com.econote.R;
import com.econote.adapter.ReportIssueListingAdapter;
import com.econote.customviews.WorkaroundMapFragment;
import com.econote.json.JsonParser;
import com.econote.model.ReportIssueModel;
import com.econote.services.NotificationService;
import com.econote.utils.AppUtil;
import com.econote.utils.Constants;
import com.econote.utils.GlobalAlertDialogs;
import com.econote.utils.HttpCall;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.ArrayList;

public class ChangePasswordFragment extends Fragment{
    private Context context;
    private MainActivity mActivty;
    private EditText old_password,password,confirm_password;
    private TextView submit;
    // TODO: Rename and change types and number of parameters
    public static ChangePasswordFragment newInstance() {
        ChangePasswordFragment fragment = new ChangePasswordFragment();
        return fragment;
    }

    public ChangePasswordFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getActivity();
        mActivty = (MainActivity) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this match_frag
        View view = inflater.inflate(R.layout.fragment_change_password, container, false);
        old_password=(EditText)view.findViewById(R.id.old_password);
        password=(EditText)view.findViewById(R.id.password);
        confirm_password=(EditText)view.findViewById(R.id.confirm_password);
        submit=(TextView)view.findViewById(R.id.submit);
        submit.setOnClickListener(mClickListener);
        return view;
    }


    View.OnClickListener mClickListener=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId())
            {
                case R.id.submit:
                if(old_password.getText().toString().trim().length()==0)
                {
                    GlobalAlertDialogs.createAlertSingle(mActivty,"Please enter old password.","OK",false);
                }
                else if(password.getText().toString().trim().length()==0)
                {
                    GlobalAlertDialogs.createAlertSingle(mActivty,"Please enter new password.","OK",false);
                }
                else if(confirm_password.getText().toString().trim().length()==0)
                {
                    GlobalAlertDialogs.createAlertSingle(mActivty,"Please enter confirm password.","OK",false);
                }
                else if(!password.getText().toString().trim().equals(confirm_password.getText().toString().trim()))
                {
                    GlobalAlertDialogs.createAlertSingle(mActivty,"New password and confirm password are not same.","OK",false);
                }
                else
                {
                    if (Constants.isNetworkAvailable(context)) {
                        String[] params = new String[2];
                        params[0] = Constants.GLOBAL_URL;
                        params[1] = createChangePasswordRequestData(old_password.getText().toString().trim(),password.getText().toString().trim());
                        new ChangePasswordTask().execute(params);
                    } else
                        Constants.ShowNetworkError(context);
                }
                break;
            }
        }
    };


    private class ChangePasswordTask extends AsyncTask<String, Void, String> {

        protected void onPreExecute()
        {
            Constants.showProgressDialog(context);
        }

        @Override
        protected String doInBackground(String... params)
        {
            String url = Constants.GLOBAL_URL;
            HttpCall httpCall = new HttpCall();
            String resp = httpCall.executeHttpPostRequest(url,params[1]);
            return resp;
        }

        protected void onPostExecute(String resp) {
            if (resp != null) {
                try {
                    JSONObject jo = new JSONObject(resp);
                    boolean status = jo.getBoolean("result");
                    if (status)
                    {
                        Constants.hideProgressDialog();
                        createAlertSingle(jo.optString("msg"));
                    }
                    else
                    {
                        Constants.hideProgressDialog();
                        GlobalAlertDialogs.createAlertSingle(mActivty, jo.optString("msg"), "OK", false);
                    }
                } catch (Exception e) {
                    Constants.hideProgressDialog();
                    GlobalAlertDialogs.createAlertSingle(mActivty, Constants.SERVER_EXCEPTION_MSG, "OK", false);
                }
            } else {
                Constants.hideProgressDialog();
                GlobalAlertDialogs.createAlertSingle(mActivty, "Timeout From Server.Please try again.", "OK", false);
            }
        }
    }

    private String createChangePasswordRequestData(String oldPassword,String newPassword)
    {
        String data = "";
        try {
            data += URLEncoder.encode("action", "UTF-8")
                    + "=" + URLEncoder.encode("updatePassword", "UTF-8");

            data += "&" + URLEncoder.encode("passphase", "UTF-8") + "="
                    + URLEncoder.encode(Constants.PASSPHRASE, "UTF-8");

            data += "&" + URLEncoder.encode("auth", "UTF-8") + "="
                    + URLEncoder.encode(Constants.AUTHKEY, "UTF-8");

            data += "&" + URLEncoder.encode("userId", "UTF-8") + "="
                    + URLEncoder.encode(AppUtil.getUserId(context), "UTF-8");

            data += "&" + URLEncoder.encode("userOldPassword", "UTF-8") + "="
                    + URLEncoder.encode(oldPassword, "UTF-8");

            data += "&" + URLEncoder.encode("userNewPassword", "UTF-8") + "="
                    + URLEncoder.encode(newPassword, "UTF-8");

            Log.e("Himanshu", "Dixit Change Password Data" + data);
        } catch (Exception e)
        {
            e.printStackTrace();
        }
        return data;
    }

    public void createAlertSingle(String message) {
        final Dialog dialog = new Dialog(context);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.alert_dialog_single);
        TextView msg = (TextView) dialog.findViewById(R.id.msg);
        msg.setText(message);
        Button btnConfirm = (Button) dialog.findViewById(R.id.confirm);
        btnConfirm.setText("OK");
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                mActivty.callHome();
            }
        });
        dialog.show();
    }

}
