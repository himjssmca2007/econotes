package com.econote.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;

import com.econote.AudioPlaybackActivity;
import com.econote.AudioRecordingActivity;
import com.econote.MainActivity;
import com.econote.R;
import com.econote.json.JsonParser;
import com.econote.model.HomePageMarkerModel;
import com.econote.model.JoinProgramModel;
import com.econote.model.ReportIssueModel;
import com.econote.utils.AppUtil;
import com.econote.utils.Constants;
import com.econote.utils.GlobalAlertDialogs;
import com.econote.utils.HttpCall;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import org.json.JSONObject;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.TimeZone;

public class HomeFragment extends Fragment implements OnMapReadyCallback {
    private Context context;
    private MainActivity mActivty;
    private ArrayList<ReportIssueModel> reportList;
    private GoogleMap mMap;
    private BitmapDescriptor blueIcon,tree_red,tree_green,tree_yellow,soil_marker;
    private BitmapDescriptor coral_green,coral_red,coral_yellow,water_green,water_red,water_yellow,bird_marker,tower_red,report_green,report_red,report_yellow,report_blue;
    private Marker marker;
    private Hashtable<String, String> markers;
    private ImageView rai,jap,issue,coral, water, sparrow, soil, tree;
    private boolean issueSelected,coralSelected, waterSelected, sparrowSelected, soilSelected, treeSelected;
    private ArrayList<String> selectedMap;
    private TextView fromDate,toDate;
    private String fromDateServer,toDateServer;
    SimpleDateFormat dateformat,dateServer;
    private String reportSite="";
    private ImageView record_voice;

    // TODO: Rename and change types and number of parameters
    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getActivity();
        mActivty = (MainActivity) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this match_frag
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        dateServer = new SimpleDateFormat("yyyy-MM-dd");
        dateformat = new SimpleDateFormat("dd MMM yy");
        dateformat.setTimeZone(TimeZone.getDefault());
        selectedMap = new ArrayList<>();
        markers = new Hashtable<String, String>();
        final SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        Constants.showProgressDialog(context);
        blueIcon = BitmapDescriptorFactory.fromResource(R.drawable.blue_map);
        coral_green= BitmapDescriptorFactory.fromResource(R.drawable.corel_green);
        coral_red= BitmapDescriptorFactory.fromResource(R.drawable.corel_red);
        coral_yellow=BitmapDescriptorFactory.fromResource(R.drawable.corel_yellow);

        water_green= BitmapDescriptorFactory.fromResource(R.drawable.green);
        water_red= BitmapDescriptorFactory.fromResource(R.drawable.red);
        water_yellow=BitmapDescriptorFactory.fromResource(R.drawable.yello);

        bird_marker= BitmapDescriptorFactory.fromResource(R.drawable.bird_marker);
        tower_red= BitmapDescriptorFactory.fromResource(R.drawable.tower_red);

        report_green=BitmapDescriptorFactory.fromResource(R.drawable.report_green);
        report_red=BitmapDescriptorFactory.fromResource(R.drawable.report_red);
        report_blue=BitmapDescriptorFactory.fromResource(R.drawable.report_blue);
        report_yellow=BitmapDescriptorFactory.fromResource(R.drawable.report_yellow);

        tree_red= BitmapDescriptorFactory.fromResource(R.drawable.tree_red);
        tree_green= BitmapDescriptorFactory.fromResource(R.drawable.tree_green);
        tree_yellow=BitmapDescriptorFactory.fromResource(R.drawable.tree_yellow);

        soil_marker=BitmapDescriptorFactory.fromResource(R.drawable.soil);

        rai = (ImageView) view.findViewById(R.id.rai);
        rai.setOnClickListener(mCLickListener);
        jap = (ImageView) view.findViewById(R.id.jap);
        jap.setOnClickListener(mCLickListener);
        issue= (ImageView) view.findViewById(R.id.issue);
        coral = (ImageView) view.findViewById(R.id.coral);
        water = (ImageView) view.findViewById(R.id.water);
        sparrow = (ImageView) view.findViewById(R.id.sparrow);
        soil = (ImageView) view.findViewById(R.id.soil);
        tree = (ImageView) view.findViewById(R.id.tree);
        resetSelected();

        fromDate=(TextView)view.findViewById(R.id.fromDate);
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, -30);
        Date datePrevious = calendar.getTime();
        fromDate.setText(dateformat.format(datePrevious));
        fromDate.setOnClickListener(mCLickListener);
        toDate=(TextView)view.findViewById(R.id.toDate);
        toDate.setText(dateformat.format(new Date()));
        toDate.setOnClickListener(mCLickListener);

        record_voice=(ImageView)view.findViewById(R.id.record_voice);
        record_voice.setOnClickListener(mCLickListener);
        return view;
    }

    private void resetSelected()
    {
        issueSelected=false;
        coralSelected = false;
        waterSelected = false;
        sparrowSelected = false;
        soilSelected = false;
        treeSelected = false;
        issue.setImageResource(R.drawable.issue);
        issue.setOnClickListener(mCLickListener);
        coral.setImageResource(R.drawable.coral);
        coral.setOnClickListener(mCLickListener);
        water.setImageResource(R.drawable.water);
        water.setOnClickListener(mCLickListener);
        sparrow.setImageResource(R.drawable.bird);
        sparrow.setOnClickListener(mCLickListener);
        soil.setImageResource(R.drawable.soil);
        soil.setOnClickListener(mCLickListener);
        tree.setImageResource(R.drawable.tree);
        tree.setOnClickListener(mCLickListener);
    }


    private void setSelected()
    {
        reportSite="REPORT_A_SITE";
        issueSelected = true;
        issue.setImageResource(R.drawable.issue_selected);

        selectedMap.add("24");
        coralSelected = true;
        coral.setImageResource(R.drawable.coral_selected);

        selectedMap.add("21");
        waterSelected = true;
        water.setImageResource(R.drawable.water_selected);

        selectedMap.add("23");
        sparrowSelected = true;
        sparrow.setImageResource(R.drawable.bird_selected);

        selectedMap.add("22");
        soilSelected = true;
        soil.setImageResource(R.drawable.soil_selected);

        selectedMap.add("25");
        treeSelected = true;
        tree.setImageResource(R.drawable.tree_selected);

        callMarkerApi();
    }

    View.OnClickListener mCLickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId())
            {
                case R.id.issue:
                    if (issueSelected)
                    {
                        reportSite="";
                        issueSelected = false;
                        issue.setImageResource(R.drawable.issue);
                    } else
                    {
                        reportSite="REPORT_A_SITE";
                        issueSelected = true;
                        issue.setImageResource(R.drawable.issue_selected);
                    }
                    callMarkerApi();
                    break;
                case R.id.coral:
                    if (coralSelected)
                    {
                        selectedMap.remove("24");
                        coralSelected = false;
                        coral.setImageResource(R.drawable.coral);
                    } else
                    {
                        selectedMap.add("24");
                        coralSelected = true;
                        coral.setImageResource(R.drawable.coral_selected);
                    }
                    callMarkerApi();
                    break;
                case R.id.water:
                    if (waterSelected)
                    {
                        selectedMap.remove("21");
                        waterSelected = false;
                        water.setImageResource(R.drawable.water);
                    } else
                    {
                        selectedMap.add("21");
                        waterSelected = true;
                        water.setImageResource(R.drawable.water_selected);
                    }
                    callMarkerApi();
                    break;
                case R.id.sparrow:
                    if (sparrowSelected)
                    {
                        selectedMap.remove("23");
                        sparrowSelected = false;
                        sparrow.setImageResource(R.drawable.bird);
                    } else
                    {
                        selectedMap.add("23");
                        sparrowSelected = true;
                        sparrow.setImageResource(R.drawable.bird_selected);
                    }
                    callMarkerApi();
                    break;
                case R.id.soil:
                    if (soilSelected)
                    {
                        selectedMap.remove("22");
                        soilSelected = false;
                        soil.setImageResource(R.drawable.soil);
                    } else
                    {
                        selectedMap.add("22");
                        soilSelected = true;
                        soil.setImageResource(R.drawable.soil_selected);
                    }
                    callMarkerApi();
                    break;
                case R.id.tree:
                    if (treeSelected)
                    {
                        selectedMap.remove("25");
                        treeSelected = false;
                        tree.setImageResource(R.drawable.tree);
                    }
                    else
                    {
                        selectedMap.add("25");
                        treeSelected = true;
                        tree.setImageResource(R.drawable.tree_selected);
                    }
                    callMarkerApi();
                    break;
                case R.id.rai:
                    mActivty.callReportIssue();
                    break;
                case R.id.jap:
                    mActivty.callJoinProgram();
                    break;
                case R.id.fromDate:
                    dateDialog(fromDate);
                    break;
                case R.id.toDate:
                    dateDialog(toDate);
                    break;
                case R.id.record_voice:
                    Intent i = new Intent(context,AudioRecordingActivity.class);
                    startActivityForResult(i,Constants.REQUEST_RECORD);
                    break;
            }

        }
    };


    private void callMarkerApi()
    {
        String fromStr=fromDate.getText().toString().trim();
        String toStr=toDate.getText().toString().trim();
        Date fromDate=null,toDate=null;
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, -31);
        Date datePrevious = calendar.getTime();
        try {
            fromDate=dateformat.parse(fromStr);
            fromDateServer=dateServer.format(fromDate);
            toDate=dateformat.parse(toStr);
            toDateServer=dateServer.format(toDate);
        }
        catch (ParseException e) {
            e.printStackTrace();
        }
        if(fromDate.compareTo(toDate)>0)
        {
            GlobalAlertDialogs.createAlertSingle(mActivty,"From date cannot be bigger than To date","OK",false);
        }
        else
        {
            if (Constants.isNetworkAvailable(context))
            {
                if(fromDate.compareTo(datePrevious)<0)
                    Constants.ShowToast(context,"Econotes App may be hang due to heavy data load !");
                String data = createMarkerRequestData();
                new HomePageMarkerTask().execute(data);
            } else
                Constants.ShowNetworkError(context);
        }
        /*if(selectedMap.size()==0 && TextUtils.isEmpty(reportSite))
        {
            if (Constants.isNetworkAvailable(context)) {
                String data = createReportIssueRequestData();
                new ReportIssueTask().execute(data);
            } else
                Constants.ShowNetworkError(context);
        }
        else
        {
            String fromStr=fromDate.getText().toString().trim();
            String toStr=toDate.getText().toString().trim();
            Date fromDate=null,toDate=null;
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DAY_OF_MONTH, -31);
            Date datePrevious = calendar.getTime();
            try {
                fromDate=dateformat.parse(fromStr);
                fromDateServer=dateServer.format(fromDate);
                toDate=dateformat.parse(toStr);
                toDateServer=dateServer.format(toDate);
            }
            catch (ParseException e) {
                e.printStackTrace();
            }
            if(fromDate.compareTo(toDate)>0)
            {
                GlobalAlertDialogs.createAlertSingle(mActivty,"From date cannot be bigger than To date","OK",false);
            }
            else
            {
                if (Constants.isNetworkAvailable(context))
                {
                    if(fromDate.compareTo(datePrevious)<0)
                        Constants.ShowToast(context,"Econotes App may be hang due to heavy data load !");
                    String data = createMarkerRequestData();
                    new HomePageMarkerTask().execute(data);
                } else
                    Constants.ShowNetworkError(context);
            }
        }*/
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Constants.hideProgressDialog();
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.setInfoWindowAdapter(new CustomInfoWindowAdapter());
        /*if (Constants.isNetworkAvailable(context)) {
            String data = createReportIssueRequestData();
            new ReportIssueTask().execute(data);
        } else
            Constants.ShowNetworkError(context);*/
        setSelected();
    }


    private class ReportIssueTask extends AsyncTask<String, Void, String> {

        protected void onPreExecute() {
            Constants.showProgressDialog(context);
        }

        @Override
        protected String doInBackground(String... params) {
            String url = Constants.GLOBAL_URL;
            HttpCall httpCall = new HttpCall();
            String resp = httpCall.executeHttpPostRequest(url, params[0]);
            return resp;
        }

        protected void onPostExecute(String resp) {
            if (resp != null) {
                try {
                    JSONObject jo = new JSONObject(resp);
                    boolean status = jo.getBoolean("result");
                    if (status)
                    {
                        Constants.hideProgressDialog();
                        reportList = new JsonParser().parseReportIssueList(resp);
                        if (reportList != null && reportList.size() > 0)
                        {
                            mMap.clear();
                            markers.clear();
                            for (int i = 0; i < reportList.size(); i++)
                            {
                                ReportIssueModel model = reportList.get(i);
                                double latitude = model.getLatitude();
                                double longitude = model.getLongitude();
                                LatLng temp = new LatLng(latitude, longitude);
                                MarkerOptions options = new MarkerOptions().position(temp).title("Description:" + model.getDescription()).icon(blueIcon);
                                final Marker newMarker = mMap.addMarker(options);
                                if (model.getImageList() != null && model.getImageList().size() > 0)
                                    markers.put(newMarker.getId(), model.getImageList().get(0));
                                else
                                    markers.put(newMarker.getId(),"");
                            }
                        }
                    } else {
                        Constants.hideProgressDialog();
                        GlobalAlertDialogs.createAlertSingle(mActivty, jo.optString("msg"), "OK", false);
                    }
                } catch (Exception e) {
                    Constants.hideProgressDialog();
                    GlobalAlertDialogs.createAlertSingle(mActivty, Constants.SERVER_EXCEPTION_MSG, "OK", false);
                }
            } else {
                Constants.hideProgressDialog();
                GlobalAlertDialogs.createAlertSingle(mActivty, "Timeout From Server.Please try again.", "OK", false);
            }
            LatLng center = new LatLng(27.70731,76.16173);
            mMap.moveCamera(CameraUpdateFactory.newLatLng(center));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(5));
            if(reportList!=null && reportList.size()>0)
                mActivty.setMyPurchaseCount(reportList.size());
            else
                mActivty.setMyPurchaseCount(0);
        }
    }

    private String createReportIssueRequestData() {
        String data = "";
        try {
            data += URLEncoder.encode("action", "UTF-8")
                    + "=" + URLEncoder.encode("ImageWithDetail", "UTF-8");

            data += "&" + URLEncoder.encode("passphase", "UTF-8") + "="
                    + URLEncoder.encode(Constants.PASSPHRASE, "UTF-8");

            data += "&" + URLEncoder.encode("auth", "UTF-8") + "="
                    + URLEncoder.encode(Constants.AUTHKEY, "UTF-8");

            data += "&" + URLEncoder.encode("userid", "UTF-8") + "="
                    + URLEncoder.encode(AppUtil.getUserId(context), "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return data;
    }


    private class CustomInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

        private View view;

        public CustomInfoWindowAdapter() {
            view = LayoutInflater.from(context).inflate(R.layout.custom_info_window,
                    null);
        }

        @Override
        public View getInfoContents(Marker marker) {

            if (HomeFragment.this.marker != null
                    && HomeFragment.this.marker.isInfoWindowShown()) {
                HomeFragment.this.marker.hideInfoWindow();
                HomeFragment.this.marker.showInfoWindow();
            }
            return null;
        }

        @Override
        public View getInfoWindow(final Marker marker) {
            HomeFragment.this.marker = marker;
            String url = null;
            if (marker.getId() != null && markers != null && markers.size() > 0)
            {
                if (markers.get(marker.getId()) != null &&
                        markers.get(marker.getId()) != null) {
                    url = markers.get(marker.getId());
                }
            }
            final ImageView image = ((ImageView) view.findViewById(R.id.badge));
            if (!TextUtils.isEmpty(url)) {
                ImageLoader.getInstance().displayImage(url, image, Constants.options, new SimpleImageLoadingListener() {
                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        super.onLoadingComplete(imageUri, view, loadedImage);
                        getInfoContents(marker);
                    }
                });
            } else
                image.setImageResource(R.mipmap.ic_launcher);

            final String title = marker.getTitle();
            final TextView titleUi = ((TextView) view.findViewById(R.id.title));
            if (title != null) {
                titleUi.setText(Html.fromHtml(title));
            } else {
                titleUi.setText("");
            }
            return view;
        }
    }


    private String createMarkerRequestData() {
        String data = "";
        try {
            data += URLEncoder.encode("action", "UTF-8")
                    + "=" + URLEncoder.encode("homePageResult", "UTF-8");

            data += "&" + URLEncoder.encode("passphase", "UTF-8") + "="
                    + URLEncoder.encode(Constants.PASSPHRASE, "UTF-8");

            data += "&" + URLEncoder.encode("auth", "UTF-8") + "="
                    + URLEncoder.encode(Constants.AUTHKEY, "UTF-8");

            data += "&" + URLEncoder.encode("user_email", "UTF-8") + "="
                    + URLEncoder.encode(AppUtil.getUserEmail(context), "UTF-8");

            data += "&" + URLEncoder.encode("start_date", "UTF-8") + "="
                    + URLEncoder.encode(fromDateServer, "UTF-8");

            data += "&" + URLEncoder.encode("end_date", "UTF-8") + "="
                    + URLEncoder.encode(toDateServer, "UTF-8");

            String category="";
            for(int i=0;i<selectedMap.size();i++)
            {
                category=category+selectedMap.get(i)+",";
            }
            if(!TextUtils.isEmpty(category))
                category=category.substring(0,category.length()-1);

            data += "&" + URLEncoder.encode("category_type", "UTF-8") + "="
                    + URLEncoder.encode(category, "UTF-8");

            if(!TextUtils.isEmpty(reportSite))
            {
                data += "&" + URLEncoder.encode("report_a_problem", "UTF-8") + "="
                        + URLEncoder.encode(reportSite, "UTF-8");
            }

            Log.e("Himanshu"+category,"Dixit Category"+data);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return data;
    }

    private class HomePageMarkerTask extends AsyncTask<String, Void, String> {

        protected void onPreExecute() {
            Constants.showProgressDialog(context);
        }

        @Override
        protected String doInBackground(String... params) {
            String url = Constants.GLOBAL_URL;
            HttpCall httpCall = new HttpCall();
            String resp = httpCall.executeHttpPostRequest(url, params[0]);
            return resp;
        }

        protected void onPostExecute(String resp) {
            if (resp != null) {
                try {
                    JSONObject jo = new JSONObject(resp);
                    boolean status = jo.getBoolean("result");
                    if (status) {
                        Constants.hideProgressDialog();
                        ArrayList<HomePageMarkerModel> markerList = new JsonParser().parseHomePageMarkerList(resp);
                        if (markerList != null && markerList.size() > 0)
                        {
                            mMap.clear();
                            mMap.clear();
                            putMarkerOnMap(markerList);
                        }
                    } else {
                        Constants.hideProgressDialog();
                        GlobalAlertDialogs.createAlertSingle(mActivty, jo.optString("msg"), "OK", false);
                    }
                } catch (Exception e) {
                    Constants.hideProgressDialog();
                    GlobalAlertDialogs.createAlertSingle(mActivty, Constants.SERVER_EXCEPTION_MSG, "OK", false);
                }
            } else {
                Constants.hideProgressDialog();
                GlobalAlertDialogs.createAlertSingle(mActivty, "Timeout From Server.Please try again.", "OK", false);
            }
            LatLng center = new LatLng(27.70731,76.16173);
            mMap.moveCamera(CameraUpdateFactory.newLatLng(center));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(4));
        }
    }

    private void putMarkerOnMap(ArrayList<HomePageMarkerModel> markerList)
    {
        for (int i = 0; i < markerList.size(); i++)
        {
            HomePageMarkerModel model = markerList.get(i);
            if(model.getTest_ID().equals("23"))
            {
                if(model.getSurvey_result3().equalsIgnoreCase("mobile"))
                {
                    String latStr=model.getLocation1();
                    String longStr=model.getLocation2();
                    double latitude=Constants.getDoubleValue(latStr);
                    double longitude=Constants.getDoubleValue(longStr);
                    LatLng temp = new LatLng(latitude, longitude);
                    MarkerOptions options = new MarkerOptions().position(temp).title("Spot:"+model.getSurvey_result3()).icon(bird_marker);
                    final Marker newMarker = mMap.addMarker(options);
                    if (!TextUtils.isEmpty(model.getSite_Pic1()))
                        markers.put(newMarker.getId(),"http://econotes.in/login/dashboard/upload/"+model.getSite_Pic1());
                    else
                        markers.put(newMarker.getId(),"");
                }
                else
                {
                    String latStr=model.getLocation1();
                    String longStr=model.getLocation2();
                    double latitude=Constants.getDoubleValue(latStr);
                    double longitude=Constants.getDoubleValue(longStr);
                    LatLng temp = new LatLng(latitude, longitude);
                    MarkerOptions options = new MarkerOptions().position(temp).title("Spot:"+model.getSurvey_result3()).icon(tower_red);
                    final Marker newMarker = mMap.addMarker(options);
                    if (!TextUtils.isEmpty(model.getSite_Pic1()))
                        markers.put(newMarker.getId(),"http://econotes.in/login/dashboard/upload/"+ model.getSite_Pic1());
                    else
                        markers.put(newMarker.getId(),"");
                }
            }
            else if(model.getTest_ID().equals("24"))
            {
                if(model.getSurvey_result1().equalsIgnoreCase("Damaged")||model.getSurvey_result1().equalsIgnoreCase("Dead"))
                {
                    String latStr=model.getLocation1();
                    String longStr=model.getLocation2();
                    double latitude=Constants.getDoubleValue(latStr);
                    double longitude=Constants.getDoubleValue(longStr);
                    LatLng temp = new LatLng(latitude, longitude);
                    MarkerOptions options = new MarkerOptions().position(temp).title("Status of Corel:"+model.getSurvey_result1()).icon(coral_red);
                    final Marker newMarker = mMap.addMarker(options);
                    if (!TextUtils.isEmpty(model.getSite_Pic1()))
                        markers.put(newMarker.getId(),"http://econotes.in/login/dashboard/upload/"+model.getSite_Pic1());
                    else
                        markers.put(newMarker.getId(),"");
                }
                else if(model.getSurvey_result1().equalsIgnoreCase("Healthy"))
                {
                    String latStr=model.getLocation1();
                    String longStr=model.getLocation2();
                    double latitude=Constants.getDoubleValue(latStr);
                    double longitude=Constants.getDoubleValue(longStr);
                    LatLng temp = new LatLng(latitude, longitude);
                    MarkerOptions options = new MarkerOptions().position(temp).title("Status of Corel:"+model.getSurvey_result1()).icon(coral_green);
                    final Marker newMarker = mMap.addMarker(options);
                    if (!TextUtils.isEmpty(model.getSite_Pic1()))
                        markers.put(newMarker.getId(),"http://econotes.in/login/dashboard/upload/"+model.getSite_Pic1());
                    else
                        markers.put(newMarker.getId(),"");
                }
                else
                {
                    String latStr=model.getLocation1();
                    String longStr=model.getLocation2();
                    double latitude=Constants.getDoubleValue(latStr);
                    double longitude=Constants.getDoubleValue(longStr);
                    LatLng temp = new LatLng(latitude, longitude);
                    MarkerOptions options = new MarkerOptions().position(temp).title("Status of Corel:"+model.getSurvey_result1()).icon(coral_yellow);
                    final Marker newMarker = mMap.addMarker(options);
                    if (!TextUtils.isEmpty(model.getSite_Pic1()))
                        markers.put(newMarker.getId(),"http://econotes.in/login/dashboard/upload/"+model.getSite_Pic1());
                    else
                        markers.put(newMarker.getId(),"");
                }
            }
            else if(model.getTest_ID().equals("22"))
            {
                String latStr=model.getLocation1();
                String longStr=model.getLocation2();
                double latitude=Constants.getDoubleValue(latStr);
                double longitude=Constants.getDoubleValue(longStr);
                LatLng temp = new LatLng(latitude, longitude);
                String desc="pH"+model.getTest_result2_para1()+"\nPhosphorus:"+model.getTest_result2_para2()+"\nNitrogen:"+model.getTest_result2_para3()+"\nPotash:"+model.getTest_result2_para4();
                MarkerOptions options = new MarkerOptions().position(temp).title(desc).icon(soil_marker);
                final Marker newMarker = mMap.addMarker(options);
                if (!TextUtils.isEmpty(model.getSite_Pic1()))
                    markers.put(newMarker.getId(),"http://econotes.in/login/dashboard/upload/"+model.getSite_Pic1());
                else
                    markers.put(newMarker.getId(),"");
            }
            else if(model.getTest_ID().equals("25"))
            {
                if(model.getSurvey_result2().equalsIgnoreCase("Sick"))
                {
                    String latStr=model.getLocation1();
                    String longStr=model.getLocation2();
                    double latitude=Constants.getDoubleValue(latStr);
                    double longitude=Constants.getDoubleValue(longStr);
                    LatLng temp = new LatLng(latitude, longitude);
                    MarkerOptions options = new MarkerOptions().position(temp).title("Status of Tree:" + model.getSurvey_result2()).icon(tree_yellow);
                    final Marker newMarker = mMap.addMarker(options);
                    if (!TextUtils.isEmpty(model.getSite_Pic1()))
                        markers.put(newMarker.getId(),"http://econotes.in/login/dashboard/upload/"+model.getSite_Pic1());
                    else
                        markers.put(newMarker.getId(),"");
                }
                else if(model.getSurvey_result2().equalsIgnoreCase("Chopped"))
                {
                    String latStr=model.getLocation1();
                    String longStr=model.getLocation2();
                    double latitude=Constants.getDoubleValue(latStr);
                    double longitude=Constants.getDoubleValue(longStr);
                    LatLng temp = new LatLng(latitude, longitude);
                    MarkerOptions options = new MarkerOptions().position(temp).title("Status of Tree:" + model.getSurvey_result2()).icon(tree_red);
                    final Marker newMarker = mMap.addMarker(options);
                    if (!TextUtils.isEmpty(model.getSite_Pic1()))
                        markers.put(newMarker.getId(),"http://econotes.in/login/dashboard/upload/"+model.getSite_Pic1());
                    else
                        markers.put(newMarker.getId(),"");
                }
                else
                {
                    String latStr=model.getLocation1();
                    String longStr=model.getLocation2();
                    double latitude=Constants.getDoubleValue(latStr);
                    double longitude=Constants.getDoubleValue(longStr);
                    LatLng temp = new LatLng(latitude, longitude);
                    MarkerOptions options = new MarkerOptions().position(temp).title("Status of Tree:"+model.getSurvey_result2()).icon(tree_green);
                    final Marker newMarker = mMap.addMarker(options);
                    if (!TextUtils.isEmpty(model.getSite_Pic1()))
                        markers.put(newMarker.getId(),"http://econotes.in/login/dashboard/upload/"+model.getSite_Pic1());
                    else
                        markers.put(newMarker.getId(),"");
                }
            }
            else if(model.getTest_ID().equals("21"))
            {
                Double phValue=Constants.getDoubleValue(model.getTest_result_para3());
                String latStr=model.getLocation1();
                String longStr=model.getLocation2();
                double latitude=Constants.getDoubleValue(latStr);
                double longitude=Constants.getDoubleValue(longStr);
                LatLng temp = new LatLng(latitude, longitude);
                if(phValue<3.9)
                {
                    String desc="<tr><td bgcolor=\"#CC0000\" colspan=\"2\"><strong style=\"color:#FFFFFF\"> not suitable for any domestic use</strong></td></tr>";
                    desc=desc+"\nColor"+model.getTest_result_para1()+"\nOdour:"+model.getTest_result_para2()+"\npH:"+model.getTest_result_para3()+"\nTurbidity:"+model.getTest_result_para4()+"\nCloromines:"+model.getTest_result_para5()+"\nChloride:"+model.getTest_result_para6()+"\nFlouride:"+model.getTest_result_para7()+"\nIron:"+model.getTest_result_para8()+"\nNitrate:"+model.getTest_result_para9()+"\nHardness:"+model.getTest_result_para10();
                    MarkerOptions options = new MarkerOptions().position(temp).title(desc).icon(water_red);
                    final Marker newMarker = mMap.addMarker(options);
                    if (!TextUtils.isEmpty(model.getSite_Pic1()))
                        markers.put(newMarker.getId(), model.getSite_Pic1());
                    else
                        markers.put(newMarker.getId(), "");
                }
                else if(phValue>=4.0 && phValue<6.4)
                {
                    String desc="<tr><td bgcolor=\"#FFCC00\" colspan=\"2\"><strong style=\"color:#666666\"> suitable for bathing and washing,<br>do not drink</strong></td></tr>";
                    desc=desc+"\nColor"+model.getTest_result_para1()+"\nOdour:"+model.getTest_result_para2()+"\npH:"+model.getTest_result_para3()+"\nTurbidity:"+model.getTest_result_para4()+"\nCloromines:"+model.getTest_result_para5()+"\nChloride:"+model.getTest_result_para6()+"\nFlouride:"+model.getTest_result_para7()+"\nIron:"+model.getTest_result_para8()+"\nNitrate:"+model.getTest_result_para9()+"\nHardness:"+model.getTest_result_para10();
                    MarkerOptions options = new MarkerOptions().position(temp).title(desc).icon(water_yellow);
                    final Marker newMarker = mMap.addMarker(options);
                    if (!TextUtils.isEmpty(model.getSite_Pic1()))
                        markers.put(newMarker.getId(), model.getSite_Pic1());
                    else
                        markers.put(newMarker.getId(), "");
                }
                else
                {
                    String desc="<tr><td bgcolor=\"#009900\" colspan=\"2\"><strong style=\"color:#FFFFFF\"> drink with caution</strong></td></tr>";
                    desc=desc+"\nColor"+model.getTest_result_para1()+"\nOdour:"+model.getTest_result_para2()+"\npH:"+model.getTest_result_para3()+"\nTurbidity:"+model.getTest_result_para4()+"\nCloromines:"+model.getTest_result_para5()+"\nChloride:"+model.getTest_result_para6()+"\nFlouride:"+model.getTest_result_para7()+"\nIron:"+model.getTest_result_para8()+"\nNitrate:"+model.getTest_result_para9()+"\nHardness:"+model.getTest_result_para10();
                    MarkerOptions options = new MarkerOptions().position(temp).title(desc).icon(water_green);
                    final Marker newMarker = mMap.addMarker(options);
                    if (!TextUtils.isEmpty(model.getSite_Pic1()))
                        markers.put(newMarker.getId(), model.getSite_Pic1());
                    else
                        markers.put(newMarker.getId(), "");
                }
            }
            else if(model.getTest_ID().equalsIgnoreCase("REPORT_A_SITE"))
            {
                String desc="Description: "+model.getSite_descr()+"\n"+model.getCateg();
                if(model.getTest_result_para12().equalsIgnoreCase("Resolved"))
                {
                    String latStr=model.getLocation1();
                    String longStr=model.getLocation2();
                    double latitude=Constants.getDoubleValue(latStr);
                    double longitude=Constants.getDoubleValue(longStr);
                    LatLng temp = new LatLng(latitude, longitude);
                    MarkerOptions options = new MarkerOptions().position(temp).title(desc).icon(report_green);
                    final Marker newMarker = mMap.addMarker(options);
                    if (!TextUtils.isEmpty(model.getSite_Pic1()))
                        markers.put(newMarker.getId(),"http://econotes.in/login/dashboard/upload/"+model.getSite_Pic1());
                    else
                        markers.put(newMarker.getId(),"");
                }
                else if(model.getTest_result_para12().equalsIgnoreCase("Not"))
                {
                    String latStr=model.getLocation1();
                    String longStr=model.getLocation2();
                    double latitude=Constants.getDoubleValue(latStr);
                    double longitude=Constants.getDoubleValue(longStr);
                    LatLng temp = new LatLng(latitude, longitude);
                    MarkerOptions options = new MarkerOptions().position(temp).title(desc).icon(report_yellow);
                    final Marker newMarker = mMap.addMarker(options);
                    if (!TextUtils.isEmpty(model.getSite_Pic1()))
                        markers.put(newMarker.getId(),"http://econotes.in/login/dashboard/upload/"+model.getSite_Pic1());
                    else
                        markers.put(newMarker.getId(),"");
                }
                else if(model.getTest_result_para12().equalsIgnoreCase("Worsened"))
                {
                    String latStr=model.getLocation1();
                    String longStr=model.getLocation2();
                    double latitude=Constants.getDoubleValue(latStr);
                    double longitude=Constants.getDoubleValue(longStr);
                    LatLng temp = new LatLng(latitude, longitude);
                    MarkerOptions options = new MarkerOptions().position(temp).title(desc).icon(report_red);
                    final Marker newMarker = mMap.addMarker(options);
                    if (!TextUtils.isEmpty(model.getSite_Pic1()))
                        markers.put(newMarker.getId(),"http://econotes.in/login/dashboard/upload/"+model.getSite_Pic1());
                    else
                        markers.put(newMarker.getId(),"");
                }
                else
                {
                    String latStr=model.getLocation1();
                    String longStr=model.getLocation2();
                    double latitude=Constants.getDoubleValue(latStr);
                    double longitude=Constants.getDoubleValue(longStr);
                    LatLng temp = new LatLng(latitude, longitude);
                    MarkerOptions options = new MarkerOptions().position(temp).title(desc).icon(report_blue);
                    final Marker newMarker = mMap.addMarker(options);
                    if (!TextUtils.isEmpty(model.getSite_Pic1()))
                        markers.put(newMarker.getId(),"http://econotes.in/login/dashboard/upload/"+model.getSite_Pic1());
                    else
                        markers.put(newMarker.getId(),"");
                }
            }
        }
    }


    private void dateDialog(final TextView tv)
    {
        final Dialog dialog = new Dialog(context);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_date);
        dialog.setTitle("SELECT DATE");
        final DatePicker dialogDate = (DatePicker) dialog.findViewById(R.id.datePicker1);
        final Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        dialogDate.init(year, month, day, null);
        String dateStr=tv.getText().toString().trim();
        if (!TextUtils.isEmpty(dateStr)) {
                Date date = null;
                try {
                    date = dateformat.parse(dateStr);
                } catch (ParseException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                Calendar cl = Calendar.getInstance();
                if (date != null)
                    cl.setTimeInMillis(date.getTime());
                dialogDate.updateDate(cl.get(Calendar.YEAR), cl.get(Calendar.MONTH), cl.get(Calendar.DATE));
        }
        TextView btnConfirm = (TextView) dialog.findViewById(R.id.dialog_date_confirm);
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogDate.clearFocus();
                GregorianCalendar newGregCal = new GregorianCalendar(dialogDate.getYear(), dialogDate.getMonth(), dialogDate.getDayOfMonth());
                Date dt = new Date(newGregCal.getTimeInMillis());
                tv.setText(dateformat.format(dt));
                dialog.dismiss();
            }

        });
        TextView btnCancel = (TextView) dialog.findViewById(R.id.dialog_date_cancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }


    @Override
    public void onActivityResult(int requestCode, int resuleCode, Intent data) {
        super.onActivityResult(requestCode, resuleCode, data);

        if(requestCode==Constants.REQUEST_RECORD)
        {
            if (resuleCode == Activity.RESULT_OK)
            {
                String fileName=data.getStringExtra(AudioPlaybackActivity.FileNameArg);
                if(!TextUtils.isEmpty(fileName))
                {
                    new SubmitAudioTask().execute(fileName);
                }
            }
        }
    }

    private class SubmitAudioTask extends AsyncTask<String, Void, String> {


        protected void onPreExecute()
        {
            Constants.showProgressDialog(context);
        }

        @Override
        protected String doInBackground(String... params)
        {
            HttpCall httpCall = new HttpCall();
            String resp = httpCall.doAudioSubmit(Constants.GLOBAL_URL,AppUtil.getUserId(context), params[0]);
            return resp;
        }

        protected void onPostExecute(String resp) {
            if (resp != null) {
                try {
                    JSONObject jo = new JSONObject(resp);
                    boolean status = jo.getBoolean("result");
                    if (status)
                    {
                        Constants.hideProgressDialog();
                        GlobalAlertDialogs.createAlertSingle(mActivty, jo.optString("msg"), "OK", false);
                    }
                    else
                    {
                        Constants.hideProgressDialog();
                        GlobalAlertDialogs.createAlertSingle(mActivty, jo.optString("msg"), "OK", false);
                    }
                } catch (Exception e) {
                    Constants.hideProgressDialog();
                    GlobalAlertDialogs.createAlertSingle(mActivty, Constants.SERVER_EXCEPTION_MSG, "OK", false);
                }
            } else {
                Constants.hideProgressDialog();
                GlobalAlertDialogs.createAlertSingle(mActivty, "Timeout From Server.Please try again.", "OK", false);
            }
        }
    }
}