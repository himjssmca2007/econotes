package com.econote.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.econote.CorelSurveyActivity;
import com.econote.IndoorAirActivity;
import com.econote.LocationJoinProgramActivity;
import com.econote.MainActivity;
import com.econote.PicUploadJoinProgramActivity;
import com.econote.ProgramListActivity;
import com.econote.R;
import com.econote.SoundCheckActivity;
import com.econote.SparrowTrackerActivity;
import com.econote.TreeGuardianActivity;
import com.econote.model.JoinProgramModel;
import com.econote.model.ProgramModel;
import com.econote.model.ReportIssueModel;
import com.econote.services.NotificationService;
import com.econote.utils.AppUtil;
import com.econote.utils.Constants;
import com.econote.utils.GlobalAlertDialogs;
import com.econote.utils.HttpCall;

import org.json.JSONObject;

import java.net.URLEncoder;

public class JoinUsFragment extends Fragment {
    private Context context;
    private MainActivity mActivty;
    private LinearLayout code_panel,program_panel,payment_panel,location_panel,pic_panel,save_panel;
    private TextView title_code,title_program,title_payment,title_location,title_pic,title_save;
    private TextView action_code_prepaid,action_code_without,action_program,action_payment_prepaid,action_location,action_pic,action_save;
    private JoinProgramModel model;
    private EditText prepaid_code;
    public static JoinUsFragment newInstance()
    {
        JoinUsFragment fragment = new JoinUsFragment();
        return fragment;
    }

    public JoinUsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getActivity();
        mActivty = (MainActivity) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this match_frag
        View view = inflater.inflate(R.layout.fragment_join_us, container, false);
        prepaid_code=(EditText)view.findViewById(R.id.prepaid_code);

        code_panel=(LinearLayout)view.findViewById(R.id.code_panel);
        program_panel=(LinearLayout)view.findViewById(R.id.program_panel);
        payment_panel=(LinearLayout)view.findViewById(R.id.payment_panel);
        location_panel=(LinearLayout)view.findViewById(R.id.location_panel);
        pic_panel=(LinearLayout)view.findViewById(R.id.pic_panel);
        save_panel=(LinearLayout)view.findViewById(R.id.save_panel);

        title_code=(TextView)view.findViewById(R.id.title_code);
        title_program=(TextView)view.findViewById(R.id.title_program);
        title_payment=(TextView)view.findViewById(R.id.title_payment);
        title_location=(TextView)view.findViewById(R.id.title_location);
        title_pic=(TextView)view.findViewById(R.id.title_pic);
        title_save=(TextView)view.findViewById(R.id.title_save);

        action_code_prepaid=(TextView)view.findViewById(R.id.action_code_prepaid);
        action_code_without=(TextView)view.findViewById(R.id.action_code_without);
        action_program=(TextView)view.findViewById(R.id.action_program);
        action_payment_prepaid=(TextView)view.findViewById(R.id.action_payment_prepaid);
        action_location=(TextView)view.findViewById(R.id.action_location);
        action_pic=(TextView)view.findViewById(R.id.action_pic);
        action_save=(TextView)view.findViewById(R.id.action_save);

        if(getArguments()!=null && getArguments().getParcelable("program_model")!=null)
        {
            ReportIssueModel programModel=getArguments().getParcelable("program_model");
            model=new JoinProgramModel();
            model.setModuleID(programModel.getTest_ID());
            model.setTitle(programModel.getTitle());
            model.setRef_ID(programModel.getId());
            if(programModel.getImageList()!=null && programModel.getImageList().size()>0)
                model.setImageList(programModel.getImageList());
            model.setLatitude(programModel.getLatitude());
            model.setLongitude(programModel.getLongitude());
            action_code_prepaid.setOnClickListener(null);
            action_code_without.setOnClickListener(null);
            action_code_prepaid.setVisibility(View.GONE);
            action_code_without.setVisibility(View.GONE);
            action_program.setVisibility(View.GONE);
            prepaid_code.setVisibility(View.GONE);
            prepaid_code.setText("");
            payment_panel.setVisibility(View.GONE);
            title_code.setText("Reference Code\n\n" + model.getRef_ID());
            title_program.setText("You have selected:\n\n"+model.getTitle());
            action_location.setOnClickListener(mClickListener);
            action_pic.setOnClickListener(mClickListener);
            action_save.setOnClickListener(mClickListener);
        }
        else
        {
            action_code_prepaid.setOnClickListener(mClickListener);
            action_code_without.setOnClickListener(mClickListener);
        }
        return view;
    }


    View.OnClickListener mClickListener=new View.OnClickListener() {
        @Override
        public void onClick(View v)
        {
            switch (v.getId())
            {
                case R.id.action_code_prepaid:
                    if(prepaid_code.getText().toString().trim().length()==0)
                    {
                        GlobalAlertDialogs.createAlertSingle(mActivty,"Please enter the Pre-Paid code.", "OK", false);
                    }
                    else {
                        if (Constants.isNetworkAvailable(context)) {
                            String[] params = new String[2];
                            params[0] = Constants.GLOBAL_URL;
                            params[1] = createVerifyCodeRequestData(AppUtil.getUserEmail(context),prepaid_code.getText().toString().trim());
                            new VerifyCodeTask().execute(params);
                        } else
                            Constants.ShowNetworkError(context);
                    }
                    break;
                case R.id.action_code_without:
                    Intent iProgram=new Intent(context,ProgramListActivity.class);
                    startActivityForResult(iProgram,Constants.REQUEST_PROGRAM);
                    break;
                case R.id.action_location:
                    if(model!=null)
                    {
                        Intent i=new Intent(context,LocationJoinProgramActivity.class);
                        i.putExtra("model",model);
                        startActivityForResult(i,Constants.REQUEST_LOCATION);
                    }
                    break;
                case R.id.action_pic:
                    if(model!=null)
                    {
                        Intent i=new Intent(context,PicUploadJoinProgramActivity.class);
                        i.putExtra("model",model);
                        startActivityForResult(i,Constants.REQUEST_PIC);
                    }
                    break;
                case R.id.action_save:
                    if(model!=null)
                    {
                        if (model.getModuleID().equalsIgnoreCase("23") || model.getModuleID().equalsIgnoreCase("24") || model.getModuleID().equalsIgnoreCase("25") || model.getModuleID().equalsIgnoreCase("27") || model.getModuleID().equalsIgnoreCase("28")) {
                            if (model.getModuleID().equalsIgnoreCase("23")) {
                                Intent survey = new Intent(context, SparrowTrackerActivity.class);
                                survey.putExtra("model", model);
                                startActivityForResult(survey, Constants.REQUEST_JOIN_SUBMIT);
                            } else if (model.getModuleID().equalsIgnoreCase("24")) {
                                Intent corel = new Intent(context, CorelSurveyActivity.class);
                                corel.putExtra("model", model);
                                startActivityForResult(corel, Constants.REQUEST_JOIN_SUBMIT);
                            } else if (model.getModuleID().equalsIgnoreCase("25")) {
                                Intent tree = new Intent(context, TreeGuardianActivity.class);
                                tree.putExtra("model", model);
                                startActivityForResult(tree, Constants.REQUEST_JOIN_SUBMIT);
                            } else if (model.getModuleID().equalsIgnoreCase("27")) {
                                Intent indoor = new Intent(context, IndoorAirActivity.class);
                                indoor.putExtra("model", model);
                                startActivityForResult(indoor, Constants.REQUEST_JOIN_SUBMIT);
                            } else if (model.getModuleID().equalsIgnoreCase("28")) {
                                Intent indoor = new Intent(context,SoundCheckActivity.class);
                                indoor.putExtra("model",model);
                                startActivityForResult(indoor,Constants.REQUEST_JOIN_SUBMIT);
                            }
                        }
                        else {
                            if (Constants.isNetworkAvailable(context))
                            {
                                new SubmitProgramTask().execute(model);
                            }
                            else
                                Constants.ShowNetworkError(context);
                        }
                    }
                    break;
            }
        }
    };


    private class VerifyCodeTask extends AsyncTask<String, Void, String> {

        protected void onPreExecute()
        {
            Constants.showProgressDialog(context);
        }

        @Override
        protected String doInBackground(String... params)
        {
            HttpCall httpCall = new HttpCall();
            String resp = httpCall.executeHttpPostRequest(params[0],params[1]);
            return resp;
        }

        protected void onPostExecute(String resp) {
            if (resp != null) {
                try {
                    JSONObject jo = new JSONObject(resp);
                    boolean status = jo.getBoolean("result");
                    if (status)
                    {
                        Constants.hideProgressDialog();
                        JSONObject joData=jo.optJSONObject("data");
                        if(joData!=null)
                        {
                            model=new JoinProgramModel();
                            model.setModuleID(joData.optString("moduleID", ""));
                            model.setTitle(joData.optString("title", ""));
                            model.setType(joData.optString("type", ""));
                            model.setRef_ID(joData.optString("Ref_ID", ""));
                            action_code_prepaid.setOnClickListener(null);
                            action_code_without.setOnClickListener(null);
                            action_code_prepaid.setVisibility(View.GONE);
                            action_code_without.setVisibility(View.GONE);
                            action_program.setVisibility(View.GONE);
                            prepaid_code.setVisibility(View.GONE);
                            prepaid_code.setText("");
                            payment_panel.setVisibility(View.GONE);
                            title_code.setText("Reference Code\n\n" + model.getRef_ID());
                            title_program.setText("You have selected:\n\n" + model.getTitle());
                            action_location.setOnClickListener(mClickListener);
                            action_pic.setOnClickListener(mClickListener);
                            action_save.setOnClickListener(mClickListener);
                        }
                    }
                    else
                    {
                        Constants.hideProgressDialog();
                        GlobalAlertDialogs.createAlertSingle(mActivty, jo.optString("msg"), "OK", false);
                    }
                } catch (Exception e) {
                    Constants.hideProgressDialog();
                    GlobalAlertDialogs.createAlertSingle(mActivty, Constants.SERVER_EXCEPTION_MSG, "OK", false);
                }
            } else {
                Constants.hideProgressDialog();
                GlobalAlertDialogs.createAlertSingle(mActivty, "Timeout From Server.Please try again.", "OK", false);
            }
        }
    }

    private String createVerifyCodeRequestData(String email,String code)
    {
        String data = "";
        try {
            data += URLEncoder.encode("email", "UTF-8")
                    + "=" + URLEncoder.encode(email, "UTF-8");

            data += "&" + URLEncoder.encode("passphase", "UTF-8") + "="
                    + URLEncoder.encode(Constants.PASSPHRASE, "UTF-8");

            data += "&" + URLEncoder.encode("auth", "UTF-8") + "="
                    + URLEncoder.encode(Constants.AUTHKEY, "UTF-8");

            data += "&" + URLEncoder.encode("action", "UTF-8") + "="
                    + URLEncoder.encode("verifyReferenceCode", "UTF-8");

            data += "&" + URLEncoder.encode("referenceCode", "UTF-8") + "="
                    + URLEncoder.encode(code, "UTF-8");

            Log.e("Himanshu", "Dixit code  Data" + data);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return data;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==Constants.REQUEST_PROGRAM)
        {
            if(resultCode== Activity.RESULT_OK)
            {
                ProgramModel selectedProgram = data.getParcelableExtra("model");
                if(Constants.isNetworkAvailable(context))
                {
                    new SelectProgramTask().execute(selectedProgram);
                }
                else
                    Constants.ShowNetworkError(context);
            }
        }
        else if(requestCode==Constants.REQUEST_LOCATION)
        {
            if(resultCode== Activity.RESULT_OK)
            {
                model = data.getParcelableExtra("model");
                action_location.setVisibility(View.GONE);
                title_location.setText("Location of Sample:\n\nYour Location Coordinates is: " + model.getLatitude() + "," + model.getLongitude());
                if (Constants.isNetworkAvailable(context))
                {
                    new SubmitLocationImages().execute(model);
                }
                else
                    Constants.ShowNetworkError(context);
            }
        }
        else if(requestCode==Constants.REQUEST_PIC)
        {
            if(resultCode== Activity.RESULT_OK)
            {
                model = data.getParcelableExtra("model");
                action_pic.setVisibility(View.GONE);
                title_pic.setText("Click\n\nSite Pic has been uploaded");
                if (Constants.isNetworkAvailable(context))
                {
                    new SubmitLocationImages().execute(model);
                }
                else
                    Constants.ShowNetworkError(context);
            }
        }
        else if(requestCode==Constants.REQUEST_JOIN_SUBMIT)
        {
            if(resultCode== Activity.RESULT_OK)
            {
                String message = data.getStringExtra("message");
                createAlertSingle(message);
            }
        }
    }


    private String createSelectProgramRequestData(String email,ProgramModel model)
    {
        String data = "";
        try {
            data += URLEncoder.encode("email", "UTF-8")
                    + "=" + URLEncoder.encode(email, "UTF-8");

            data += "&" + URLEncoder.encode("passphase", "UTF-8") + "="
                    + URLEncoder.encode(Constants.PASSPHRASE, "UTF-8");

            data += "&" + URLEncoder.encode("auth", "UTF-8") + "="
                    + URLEncoder.encode(Constants.AUTHKEY, "UTF-8");

            data += "&" + URLEncoder.encode("action", "UTF-8") + "="
                    + URLEncoder.encode("selectProgramType", "UTF-8");

            data += "&" + URLEncoder.encode("testId", "UTF-8") + "="
                    + URLEncoder.encode(model.getId(), "UTF-8");

            int programCost=0;
            try
            {
                if(!TextUtils.isEmpty(model.getCost()))
                    programCost=Integer.valueOf(model.getCost());
            }
            catch (NumberFormatException e)
            {

            }
            if(programCost==0)
            {
                data += "&" + URLEncoder.encode("paidStatus", "UTF-8") + "="
                        + URLEncoder.encode("FREE_MOBILE", "UTF-8");
            }
            else
            {
                data += "&" + URLEncoder.encode("paidStatus", "UTF-8") + "="
                        + URLEncoder.encode("PAH_MOBILE", "UTF-8");
            }
            Log.e("Himanshu", "Dixit Select Program Data"+ data);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return data;
    }

    private class SelectProgramTask extends AsyncTask<ProgramModel, Void, String> {

        ProgramModel selectedProgram;
        protected void onPreExecute()
        {
            Constants.showProgressDialog(context);
        }

        @Override
        protected String doInBackground(ProgramModel... params)
        {
            selectedProgram=params[0];
            String url=Constants.GLOBAL_URL;
            String data=createSelectProgramRequestData(AppUtil.getUserEmail(context),selectedProgram);
            HttpCall httpCall = new HttpCall();
            String resp = httpCall.executeHttpPostRequest(url,data);
            return resp;
        }

        protected void onPostExecute(String resp) {
            if (resp != null) {
                try {
                    JSONObject jo = new JSONObject(resp);
                    boolean status = jo.getBoolean("result");
                    if (status)
                    {
                        Constants.hideProgressDialog();
                        JSONObject joData=jo.optJSONObject("data");
                        if(joData!=null)
                        {
                            model=new JoinProgramModel();
                            model.setModuleID(selectedProgram.getId());
                            model.setTitle(selectedProgram.getTitle());
                            model.setType(selectedProgram.getCategory());
                            model.setRef_ID(joData.optString("Ref_ID", ""));
                            action_code_prepaid.setOnClickListener(null);
                            action_code_without.setOnClickListener(null);
                            action_code_prepaid.setVisibility(View.GONE);
                            action_code_without.setVisibility(View.GONE);
                            action_program.setVisibility(View.GONE);
                            prepaid_code.setVisibility(View.GONE);
                            prepaid_code.setText("");
                            payment_panel.setVisibility(View.GONE);
                            title_code.setText("Reference Code\n\n" + model.getRef_ID());
                            title_program.setText("You have selected:\n\n"+model.getTitle());
                            action_location.setOnClickListener(mClickListener);
                            action_pic.setOnClickListener(mClickListener);
                            action_save.setOnClickListener(mClickListener);
                        }
                    }
                    else
                    {
                        Constants.hideProgressDialog();
                        GlobalAlertDialogs.createAlertSingle(mActivty, jo.optString("msg"), "OK", false);
                    }
                } catch (Exception e) {
                    Constants.hideProgressDialog();
                    GlobalAlertDialogs.createAlertSingle(mActivty, Constants.SERVER_EXCEPTION_MSG, "OK", false);
                }
            } else {
                Constants.hideProgressDialog();
                GlobalAlertDialogs.createAlertSingle(mActivty, "Timeout From Server.Please try again.", "OK", false);
            }
        }
    }


    private class SubmitProgramTask extends AsyncTask<JoinProgramModel, Void, String> {

        JoinProgramModel joinProgram;
        protected void onPreExecute()
        {
            Constants.showProgressDialog(context);
        }

        @Override
        protected String doInBackground(JoinProgramModel... params)
        {
            joinProgram=params[0];
            HttpCall httpCall = new HttpCall();
            String resp = httpCall.doJoinProgramSubmit(Constants.GLOBAL_URL, joinProgram);
            return resp;
        }

        protected void onPostExecute(String resp) {
            if (resp != null) {
                try {
                    JSONObject jo = new JSONObject(resp);
                    boolean status = jo.getBoolean("result");
                    if (status)
                    {
                        Constants.hideProgressDialog();
                        createAlertSingle(jo.optString("msg"));
                    }
                    else
                    {
                        Constants.hideProgressDialog();
                        GlobalAlertDialogs.createAlertSingle(mActivty, jo.optString("msg"), "OK", false);
                    }
                } catch (Exception e) {
                    Constants.hideProgressDialog();
                    GlobalAlertDialogs.createAlertSingle(mActivty, Constants.SERVER_EXCEPTION_MSG, "OK", false);
                }
            } else {
                Constants.hideProgressDialog();
                GlobalAlertDialogs.createAlertSingle(mActivty, "Timeout From Server.Please try again.", "OK", false);
            }
        }
    }


    private class SubmitLocationImages extends AsyncTask<JoinProgramModel, Void, String> {

        JoinProgramModel joinProgram;
        protected void onPreExecute()
        {
            Constants.showProgressDialog(context);
        }

        @Override
        protected String doInBackground(JoinProgramModel... params)
        {
            joinProgram=params[0];
            HttpCall httpCall = new HttpCall();
            String resp = httpCall.doJoinProgramSubmit(Constants.GLOBAL_URL,joinProgram);
            return resp;
        }

        protected void onPostExecute(String resp) {
            if (resp != null) {
                try {
                    JSONObject jo = new JSONObject(resp);
                    boolean status = jo.getBoolean("result");
                    if (status)
                    {
                        Constants.hideProgressDialog();
                        GlobalAlertDialogs.createAlertSingle(mActivty, jo.optString("msg"), "OK", false);
                    }
                    else
                    {
                        Constants.hideProgressDialog();
                        GlobalAlertDialogs.createAlertSingle(mActivty, jo.optString("msg"), "OK", false);
                    }
                } catch (Exception e) {
                    Constants.hideProgressDialog();
                    GlobalAlertDialogs.createAlertSingle(mActivty, Constants.SERVER_EXCEPTION_MSG, "OK", false);
                }
            } else {
                Constants.hideProgressDialog();
                GlobalAlertDialogs.createAlertSingle(mActivty, "Timeout From Server.Please try again.", "OK", false);
            }
        }
    }


    public void createAlertSingle(String message) {
        final Dialog dialog = new Dialog(context);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.alert_dialog_single);
        TextView msg = (TextView) dialog.findViewById(R.id.msg);
        msg.setText(message);
        Button btnConfirm = (Button) dialog.findViewById(R.id.confirm);
        btnConfirm.setText("OK");
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                mActivty.callHome();
            }
        });
        dialog.show();
    }

}
