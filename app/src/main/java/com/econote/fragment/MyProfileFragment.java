package com.econote.fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.econote.MainActivity;
import com.econote.R;
import com.econote.adapter.ReportIssueListingAdapter;
import com.econote.customviews.WorkaroundMapFragment;
import com.econote.json.JsonParser;
import com.econote.model.ReportIssueModel;
import com.econote.model.UserModel;
import com.econote.services.NotificationService;
import com.econote.utils.AppUtil;
import com.econote.utils.Constants;
import com.econote.utils.GlobalAlertDialogs;
import com.econote.utils.HttpCall;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.ArrayList;

public class MyProfileFragment extends Fragment{
    private Context context;
    private MainActivity mActivty;
    private EditText full_name,profession,employment,dob,qualification,city,address;
    private TextView save_now;

    // TODO: Rename and change types and number of parameters
    public static MyProfileFragment newInstance() {
        MyProfileFragment fragment = new MyProfileFragment();
        return fragment;
    }

    public MyProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getActivity();
        mActivty = (MainActivity) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this match_frag
        View view = inflater.inflate(R.layout.fragment_my_profile, container, false);
        full_name=(EditText)view.findViewById(R.id.full_name);
        profession=(EditText)view.findViewById(R.id.profession);
        employment=(EditText)view.findViewById(R.id.employment);
        dob=(EditText)view.findViewById(R.id.dob);
        qualification=(EditText)view.findViewById(R.id.qualification);
        city=(EditText)view.findViewById(R.id.city);
        address=(EditText)view.findViewById(R.id.address);

        save_now=(TextView)view.findViewById(R.id.save_now);
        save_now.setOnClickListener(mCLickListener);

        if (Constants.isNetworkAvailable(context)) {
            String data = createUserRequestData(AppUtil.getUserId(context));
            new GetUserTask().execute(data);
        } else
            Constants.ShowNetworkError(context);
        return view;
    }

    View.OnClickListener mCLickListener=new View.OnClickListener() {
        @Override
        public void onClick(View v)
        {
            switch (v.getId())
            {
                case R.id.save_now:
                    if(full_name.getText().toString().trim().length()==0)
                    {
                        GlobalAlertDialogs.createAlertSingle(mActivty,"Please enter full name.","OK",false);
                    }
                    else if(qualification.getText().toString().trim().length()==0)
                    {
                        GlobalAlertDialogs.createAlertSingle(mActivty,"Please enter your qualification.","OK",false);
                    }
                    else if(city.getText().toString().trim().length()==0)
                    {
                        GlobalAlertDialogs.createAlertSingle(mActivty,"Please enter your city.","OK",false);
                    }
                    else if(address.getText().toString().trim().length()==0)
                    {
                        GlobalAlertDialogs.createAlertSingle(mActivty,"Please enter your city.","OK",false);
                    }
                    else
                    {
                        if(Constants.isNetworkAvailable(context))
                        {
                            new SubmitProfileTask().execute();
                        }
                        else
                            Constants.ShowNetworkError(context);
                    }
                    break;
            }
        }
    };


    private String createUserRequestData(String id) {
        String data = "";
        try {
            data += URLEncoder.encode("userid", "UTF-8")
                    + "=" + URLEncoder.encode(id, "UTF-8");

            data += "&" + URLEncoder.encode("passphase", "UTF-8") + "="
                    + URLEncoder.encode(Constants.PASSPHRASE, "UTF-8");

            data += "&" + URLEncoder.encode("auth", "UTF-8") + "="
                    + URLEncoder.encode(Constants.AUTHKEY, "UTF-8");

            data += "&" + URLEncoder.encode("action", "UTF-8") + "="
                    + URLEncoder.encode("getUserDetail", "UTF-8");

            Log.e("Himanshu", "Dixit User Data" + data);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return data;
    }

    private class GetUserTask extends AsyncTask<String, Void, String> {

        protected void onPreExecute() {
            Constants.showProgressDialog(context);
        }

        @Override
        protected String doInBackground(String... params) {
            String url = Constants.GLOBAL_URL;
            HttpCall httpCall = new HttpCall();
            String resp = httpCall.executeHttpPostRequest(url, params[0]);
            return resp;
        }

        protected void onPostExecute(String resp) {
            if (resp != null) {
                try {
                    JSONObject jo = new JSONObject(resp);
                    boolean status = jo.getBoolean("result");
                    if (status) {
                        Constants.hideProgressDialog();
                        UserModel model = new JsonParser().parseUserModel(resp);
                        if (model != null)
                        {
                            full_name.setText(model.getUname());
                            profession.setText(model.getProfession());
                            employment.setText(model.getPresent_employment());
                            dob.setText(model.getDate_of_birth());
                            qualification.setText(model.getQualification());
                            city.setText(model.getCity());
                            address.setText(model.getAddress());
                        }
                        else {
                            GlobalAlertDialogs.createAlertSingle(mActivty, "Some problem occured in getting user info", "OK", false);
                        }
                    } else {
                        Constants.hideProgressDialog();
                        GlobalAlertDialogs.createAlertSingle(mActivty, jo.optString("msg"), "OK", false);
                    }
                } catch (Exception e) {
                    Constants.hideProgressDialog();
                    GlobalAlertDialogs.createAlertSingle(mActivty, Constants.SERVER_EXCEPTION_MSG, "OK", false);
                }
            } else {
                Constants.hideProgressDialog();
                GlobalAlertDialogs.createAlertSingle(mActivty, "Timeout From Server.Please try again.", "OK", false);
            }
        }
    }


    private class SubmitProfileTask extends AsyncTask<String, Void, String> {

        protected void onPreExecute()
        {
            Constants.showProgressDialog(context);
        }

        @Override
        protected String doInBackground(String... params)
        {
            String url = Constants.GLOBAL_URL;
            String data=createProfileRequestData();
            HttpCall httpCall = new HttpCall();
            String resp = httpCall.executeHttpPostRequest(url,data);
            return resp;
        }

        protected void onPostExecute(String resp) {
            if (resp != null) {
                try {
                    JSONObject jo = new JSONObject(resp);
                    boolean status = jo.getBoolean("result");
                    if (status)
                    {
                        Constants.hideProgressDialog();
                        createAlertSingle(jo.optString("msg"));
                    }
                    else
                    {
                        Constants.hideProgressDialog();
                        GlobalAlertDialogs.createAlertSingle(mActivty, jo.optString("msg"), "OK", false);
                    }
                } catch (Exception e) {
                    Constants.hideProgressDialog();
                    GlobalAlertDialogs.createAlertSingle(mActivty, Constants.SERVER_EXCEPTION_MSG, "OK", false);
                }
            } else {
                Constants.hideProgressDialog();
                GlobalAlertDialogs.createAlertSingle(mActivty, "Timeout From Server.Please try again.", "OK", false);
            }
        }
    }

    private String createProfileRequestData()
    {
        String data = "";
        try {
            data += URLEncoder.encode("action", "UTF-8")
                    + "=" + URLEncoder.encode("updateUserDetail", "UTF-8");

            data += "&" + URLEncoder.encode("passphase", "UTF-8") + "="
                    + URLEncoder.encode(Constants.PASSPHRASE, "UTF-8");

            data += "&" + URLEncoder.encode("auth", "UTF-8") + "="
                    + URLEncoder.encode(Constants.AUTHKEY, "UTF-8");

            data += "&" + URLEncoder.encode("userid", "UTF-8") + "="
                    + URLEncoder.encode(AppUtil.getUserId(context), "UTF-8");

            data += "&" + URLEncoder.encode("city", "UTF-8") + "="
                    + URLEncoder.encode(city.getText().toString().trim(), "UTF-8");

            data += "&" + URLEncoder.encode("name", "UTF-8") + "="
                    + URLEncoder.encode(full_name.getText().toString().trim(), "UTF-8");

            data += "&" + URLEncoder.encode("email", "UTF-8") + "="
                    + URLEncoder.encode(AppUtil.getUserEmail(context), "UTF-8");

            data += "&" + URLEncoder.encode("profession", "UTF-8") + "="
                    + URLEncoder.encode(profession.getText().toString().trim(), "UTF-8");

            data += "&" + URLEncoder.encode("present_employment", "UTF-8") + "="
                    + URLEncoder.encode(employment.getText().toString().trim(), "UTF-8");

            data += "&" + URLEncoder.encode("date_of_birth", "UTF-8") + "="
                    + URLEncoder.encode(dob.getText().toString().trim(), "UTF-8");

            data += "&" + URLEncoder.encode("qualification", "UTF-8") + "="
                    + URLEncoder.encode(qualification.getText().toString().trim(), "UTF-8");

            data += "&" + URLEncoder.encode("address", "UTF-8") + "="
                    + URLEncoder.encode(address.getText().toString().trim(), "UTF-8");

            System.out.println("Submit Data:"+data);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return data;
    }




    public void createAlertSingle(String message) {
        final Dialog dialog = new Dialog(context);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.alert_dialog_single);
        TextView msg = (TextView) dialog.findViewById(R.id.msg);
        msg.setText(message);
        Button btnConfirm = (Button) dialog.findViewById(R.id.confirm);
        btnConfirm.setText("OK");
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                mActivty.callHome();
            }
        });
        dialog.show();
    }

}
