package com.econote.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.econote.MainActivity;
import com.econote.R;
import com.econote.adapter.ReportIssueListingAdapter;
import com.econote.json.JsonParser;
import com.econote.model.ReportIssueModel;
import com.econote.utils.AppUtil;
import com.econote.utils.Constants;
import com.econote.utils.GlobalAlertDialogs;
import com.econote.utils.HttpCall;

import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Hashtable;

import su.j2e.rvjoiner.JoinableAdapter;
import su.j2e.rvjoiner.JoinableLayout;
import su.j2e.rvjoiner.RvJoiner;

public class PurchaseFragment extends Fragment {
    private Context context;
    private MainActivity mActivty;
    private ArrayList<ReportIssueModel> reportList;
    private RecyclerView issueListView;
    //private StaggeredGridLayoutManager gridLayoutManagerVertical;
    private ArrayList<ReportIssueModel> joinList;
    private ArrayList<ReportIssueModel> reportIssueList;
    /**
     * Use this factory method to create a new instance of
     * this match_frag using the provided parameters.
     *
     * @return A new instance of match_frag NewsDetailFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PurchaseFragment newInstance() {
        PurchaseFragment fragment = new PurchaseFragment();
        return fragment;
    }

    public PurchaseFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getActivity();
        mActivty = (MainActivity) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this match_frag
        View view = inflater.inflate(R.layout.fragment_purchase, container, false);
        issueListView = (RecyclerView)view.findViewById(R.id.issue_list);
        //gridLayoutManagerVertical = new StaggeredGridLayoutManager(2,1);
        issueListView.setLayoutManager(new LinearLayoutManager(context));
        if(Constants.isNetworkAvailable(context))
        {
            String data=createReportIssueRequestData();
            new ReportIssueTask().execute(data);
        }
        else
            Constants.ShowNetworkError(context);
        return view;
    }


    private class ReportIssueTask extends AsyncTask<String, Void, String> {

        protected void onPreExecute()
        {
            Constants.showProgressDialog(context);
        }

        @Override
        protected String doInBackground(String... params)
        {
            String url = Constants.GLOBAL_URL;
            HttpCall httpCall = new HttpCall();
            String resp = httpCall.executeHttpPostRequest(url,params[0]);
            return resp;
        }

        protected void onPostExecute(String resp) {
            if (resp != null) {
                try {
                    JSONObject jo = new JSONObject(resp);
                    boolean status = jo.getBoolean("result");
                    if (status)
                    {
                        Constants.hideProgressDialog();
                        reportList=new JsonParser().parseReportIssueList(resp);
                        if(reportList!=null && reportList.size()>0)
                        {
                            joinList=new ArrayList<>();
                            reportIssueList=new ArrayList<>();
                            for(int i=0;i<reportList.size();i++)
                            {
                                ReportIssueModel model=reportList.get(i);
                                if(model.getTest_ID().equalsIgnoreCase("REPORT_A_SITE"))
                                    reportIssueList.add(model);
                                else
                                    joinList.add(model);
                            }
                            RvJoiner rvJoiner = new RvJoiner();
                            rvJoiner.add(new JoinableLayout(R.layout.join_header));
                            rvJoiner.add(new JoinableAdapter(new ReportIssueListingAdapter(context,mActivty,joinList)));
                            rvJoiner.add(new JoinableLayout(R.layout.report_header));
                            rvJoiner.add(new JoinableAdapter(new ReportIssueListingAdapter(context,mActivty,reportIssueList)));
                            issueListView.setAdapter(rvJoiner.getAdapter());
                        }
                    }
                    else
                    {
                        Constants.hideProgressDialog();
                        GlobalAlertDialogs.createAlertSingle(mActivty, jo.optString("msg"), "OK", false);
                    }
                } catch (Exception e)
                {
                    e.printStackTrace();
                    Constants.hideProgressDialog();
                    GlobalAlertDialogs.createAlertSingle(mActivty, Constants.SERVER_EXCEPTION_MSG, "OK", false);
                }
            } else {
                Constants.hideProgressDialog();
                GlobalAlertDialogs.createAlertSingle(mActivty, "Timeout From Server.Please try again.", "OK", false);
            }
            if(reportList!=null && reportList.size()>0)
                mActivty.setMyPurchaseCount(reportList.size());
            else
                mActivty.setMyPurchaseCount(0);
        }
    }

    private String createReportIssueRequestData()
    {
        String data = "";
        try {
            data += URLEncoder.encode("action", "UTF-8")
                    + "=" + URLEncoder.encode("ImageWithDetail", "UTF-8");

            data += "&" + URLEncoder.encode("passphase", "UTF-8") + "="
                    + URLEncoder.encode(Constants.PASSPHRASE, "UTF-8");

            data += "&" + URLEncoder.encode("auth", "UTF-8") + "="
                    + URLEncoder.encode(Constants.AUTHKEY, "UTF-8");

            data += "&" + URLEncoder.encode("userid", "UTF-8") + "="
                    + URLEncoder.encode(AppUtil.getUserId(context), "UTF-8");
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return data;
    }
}
