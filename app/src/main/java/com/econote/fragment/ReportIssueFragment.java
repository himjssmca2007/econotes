package com.econote.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.econote.FreezeLocationActivity;
import com.econote.MainActivity;
import com.econote.R;
import com.econote.RegisterActivity;
import com.econote.ResolvedIssueActivity;
import com.econote.UploadIssuePicActivity;
import com.econote.model.ReportIssueModel;
import com.econote.services.NotificationService;
import com.econote.utils.AppUtil;
import com.econote.utils.Constants;

public class ReportIssueFragment extends Fragment {
    private Context context;
    private MainActivity mActivty;
    private TextView freeze_location,submit_issue;
    private ImageView upload_pic_image,freeze_location_image;
    private ReportIssueModel model;
    /**
     * Use this factory method to create a new instance of
     * this match_frag using the provided parameters.
     *
     * @return A new instance of match_frag NewsDetailFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ReportIssueFragment newInstance()
    {
        ReportIssueFragment fragment = new ReportIssueFragment();
        return fragment;
    }

    public ReportIssueFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getActivity();
        mActivty = (MainActivity) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this match_frag
        View view = inflater.inflate(R.layout.fragment_report_issue, container, false);
        freeze_location_image=(ImageView)view.findViewById(R.id.freeze_location_image);
        upload_pic_image=(ImageView)view.findViewById(R.id.upload_pic_image);
        freeze_location=(TextView)view.findViewById(R.id.freeze_location);
        submit_issue=(TextView)view.findViewById(R.id.submit_issue);
        freeze_location.setOnClickListener(mClickListener);
        return view;
    }


    View.OnClickListener mClickListener=new View.OnClickListener()
    {
        @Override
        public void onClick(View v) {

            switch (v.getId())
            {
                case R.id.freeze_location:
                    Intent i1=new Intent(context,FreezeLocationActivity.class);
                    startActivityForResult(i1, Constants.REQUEST_LOCATION);
                    break;

                case R.id.submit_issue:
                    if(model!=null)
                    {
                        Intent i2 = new Intent(context, ResolvedIssueActivity.class);
                        i2.putExtra("model", model);
                        startActivity(i2);
                    }
                    break;
            }

        }
    };


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if(requestCode== Constants.REQUEST_LOCATION)
        {
            if(resultCode== Activity.RESULT_OK)
            {
                model=data.getParcelableExtra("model");
                submit_issue.setOnClickListener(mClickListener);
                freeze_location_image.setImageResource(R.drawable.freeze_location_checked);
                upload_pic_image.setImageResource(R.drawable.image_upload_checked);
            }
        }
    }

}
