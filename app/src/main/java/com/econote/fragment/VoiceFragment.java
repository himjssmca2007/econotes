package com.econote.fragment;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.econote.MainActivity;
import com.econote.R;
import com.econote.adapter.ReportIssueListingAdapter;
import com.econote.adapter.VoiceListingAdapter;
import com.econote.customviews.DividerItemDecoration;
import com.econote.json.JsonParser;
import com.econote.model.ReportIssueModel;
import com.econote.model.VoiceModel;
import com.econote.utils.AppUtil;
import com.econote.utils.Constants;
import com.econote.utils.GlobalAlertDialogs;
import com.econote.utils.HttpCall;

import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.ArrayList;

import su.j2e.rvjoiner.JoinableAdapter;
import su.j2e.rvjoiner.JoinableLayout;
import su.j2e.rvjoiner.RvJoiner;

public class VoiceFragment extends Fragment {
    private Context context;
    private MainActivity mActivty;
    private RecyclerView voice_list;
    //private StaggeredGridLayoutManager gridLayoutManagerVertical;
    private ArrayList<VoiceModel> voiceList;
    /**
     * Use this factory method to create a new instance of
     * this match_frag using the provided parameters.
     *
     * @return A new instance of match_frag NewsDetailFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static VoiceFragment newInstance() {
        VoiceFragment fragment = new VoiceFragment();
        return fragment;
    }

    public VoiceFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getActivity();
        mActivty = (MainActivity) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this match_frag
        View view = inflater.inflate(R.layout.fragment_voice, container, false);
        voice_list = (RecyclerView)view.findViewById(R.id.voice_list);
        //gridLayoutManagerVertical = new StaggeredGridLayoutManager(2,1);
        voice_list.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false));
        RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(context, DividerItemDecoration.VERTICAL_LIST);
        voice_list.addItemDecoration(itemDecoration);
        if(Constants.isNetworkAvailable(context))
        {
            String data=createVoiceRequestData();
            new VoiceListTask().execute(data);
        }
        else
            Constants.ShowNetworkError(context);
        return view;
    }


    private class VoiceListTask extends AsyncTask<String, Void, String> {

        protected void onPreExecute()
        {
            Constants.showProgressDialog(context);
        }

        @Override
        protected String doInBackground(String... params)
        {
            String url = Constants.GLOBAL_URL;
            HttpCall httpCall = new HttpCall();
            String resp = httpCall.executeHttpPostRequest(url,params[0]);
            return resp;
        }

        protected void onPostExecute(String resp) {
            if (resp != null) {
                try {
                    JSONObject jo = new JSONObject(resp);
                    boolean status = jo.getBoolean("result");
                    if (status)
                    {
                        Constants.hideProgressDialog();
                        voiceList=new JsonParser().parseVoiceList(resp);
                        if(voiceList!=null && voiceList.size()>0)
                        {
                            VoiceListingAdapter adapter=new VoiceListingAdapter(context,voiceList);
                            voice_list.setAdapter(adapter);
                        }
                    }
                    else
                    {
                        Constants.hideProgressDialog();
                        GlobalAlertDialogs.createAlertSingle(mActivty, jo.optString("msg"), "OK", false);
                    }
                } catch (Exception e)
                {
                    e.printStackTrace();
                    Constants.hideProgressDialog();
                    GlobalAlertDialogs.createAlertSingle(mActivty, Constants.SERVER_EXCEPTION_MSG, "OK", false);
                }
            } else {
                Constants.hideProgressDialog();
                GlobalAlertDialogs.createAlertSingle(mActivty, "Timeout From Server.Please try again.", "OK", false);
            }
        }
    }

    private String createVoiceRequestData()
    {
        String data = "";
        try {
            data += URLEncoder.encode("action", "UTF-8")
                    + "=" + URLEncoder.encode("getVoiceList", "UTF-8");

            data += "&" + URLEncoder.encode("passphase", "UTF-8") + "="
                    + URLEncoder.encode(Constants.PASSPHRASE, "UTF-8");

            data += "&" + URLEncoder.encode("auth", "UTF-8") + "="
                    + URLEncoder.encode(Constants.AUTHKEY, "UTF-8");

            data += "&" + URLEncoder.encode("user_id", "UTF-8") + "="
                    + URLEncoder.encode(AppUtil.getUserId(context), "UTF-8");
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return data;
    }
}
