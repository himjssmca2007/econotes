package com.econote.json;


import android.text.TextUtils;

import com.econote.model.HomePageMarkerModel;
import com.econote.model.IssueCategoryModel;
import com.econote.model.NotificationModel;
import com.econote.model.ProgramModel;
import com.econote.model.ReportIssueModel;
import com.econote.model.UserModel;
import com.econote.model.VoiceModel;
import com.econote.utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;

public class JsonParser {

    public ArrayList<IssueCategoryModel> parseIssueCategory(String str) {
        if (str != null && str.length() > 0)
        {
            ArrayList<IssueCategoryModel> issueList=new ArrayList<>();
            try
            {
                JSONObject jsonMain = new JSONObject(str);
                JSONArray joData=jsonMain.optJSONArray("data");
                if(joData!=null && joData.length()>0)
                {
                    for(int i=0;i<joData.length();i++)
                    {
                        JSONObject joItem=joData.optJSONObject(i);
                        if (joItem != null)
                        {
                            IssueCategoryModel model = new IssueCategoryModel();
                            model.setId(getStringFromJSON(joItem, "id"));
                            model.setTitle(getStringFromJSON(joItem, "title"));
                            model.setDetail(getStringFromJSON(joItem, "detail"));
                            model.setPhotos(getStringFromJSON(joItem, "photos"));
                            issueList.add(model);
                        }
                    }
                }
                return issueList;
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return null;
    }


    public void parseNotificationList(String str) {
        if (str != null && str.length() > 0)
        {
            ArrayList<NotificationModel> messageList=new ArrayList<>();
            ArrayList<NotificationModel> hazardList=new ArrayList<>();
            ArrayList<NotificationModel> reminderList=new ArrayList<>();
            ArrayList<NotificationModel> activityList=new ArrayList<>();
            ArrayList<NotificationModel> singleList=new ArrayList<>();
            try
            {
                JSONObject jsonMain = new JSONObject(str);
                JSONArray joData=jsonMain.optJSONArray("data");
                if(joData!=null && joData.length()>0)
                {
                    for(int i=0;i<joData.length();i++)
                    {
                        JSONObject joItem=joData.optJSONObject(i);
                        if (joItem != null)
                        {
                            String category=getStringFromJSON(joItem, "category");
                            if(!TextUtils.isEmpty(category))
                            {
                                if(category.equalsIgnoreCase("MESSAGE"))
                                {
                                    NotificationModel model = new NotificationModel();
                                    model.setId(getStringFromJSON(joItem, "id"));
                                    model.setFor_user_name(getStringFromJSON(joItem, "for_user_name"));
                                    model.setBy_user_name(getStringFromJSON(joItem, "by_user_name"));
                                    model.setLi_class(getStringFromJSON(joItem, "li_class"));
                                    model.setI_class(getStringFromJSON(joItem, "i_class"));
                                    model.setStrong_heading(getStringFromJSON(joItem, "strong_heading"));
                                    model.setDetail(getStringFromJSON(joItem, "detail"));
                                    model.setSpan_class(getStringFromJSON(joItem, "span_class"));
                                    model.setTime_of_notify(getStringFromJSON(joItem, "time_of_notify"));
                                    model.setIs_Read(getStringFromJSON(joItem, "is_Read"));
                                    model.setCategory(category);
                                    messageList.add(model);
                                    singleList.add(model);
                                }
                                else if(category.equalsIgnoreCase("HAZARD"))
                                {
                                    NotificationModel model = new NotificationModel();
                                    model.setId(getStringFromJSON(joItem, "id"));
                                    model.setFor_user_name(getStringFromJSON(joItem, "for_user_name"));
                                    model.setBy_user_name(getStringFromJSON(joItem, "by_user_name"));
                                    model.setLi_class(getStringFromJSON(joItem, "li_class"));
                                    model.setI_class(getStringFromJSON(joItem, "i_class"));
                                    model.setStrong_heading(getStringFromJSON(joItem, "strong_heading"));
                                    model.setDetail(getStringFromJSON(joItem, "detail"));
                                    model.setSpan_class(getStringFromJSON(joItem, "span_class"));
                                    model.setTime_of_notify(getStringFromJSON(joItem, "time_of_notify"));
                                    model.setIs_Read(getStringFromJSON(joItem, "is_Read"));
                                    model.setCategory(category);
                                    hazardList.add(model);
                                    singleList.add(model);
                                }
                                else if(category.equalsIgnoreCase("REMINDER"))
                                {
                                    NotificationModel model = new NotificationModel();
                                    model.setId(getStringFromJSON(joItem, "id"));
                                    model.setFor_user_name(getStringFromJSON(joItem, "for_user_name"));
                                    model.setBy_user_name(getStringFromJSON(joItem, "by_user_name"));
                                    model.setLi_class(getStringFromJSON(joItem, "li_class"));
                                    model.setI_class(getStringFromJSON(joItem, "i_class"));
                                    model.setStrong_heading(getStringFromJSON(joItem, "strong_heading"));
                                    model.setDetail(getStringFromJSON(joItem, "detail"));
                                    model.setSpan_class(getStringFromJSON(joItem, "span_class"));
                                    model.setTime_of_notify(getStringFromJSON(joItem, "time_of_notify"));
                                    model.setIs_Read(getStringFromJSON(joItem, "is_Read"));
                                    model.setCategory(category);
                                    reminderList.add(model);
                                    singleList.add(model);
                                }
                                else if(category.equalsIgnoreCase("ACTIVITY"))
                                {
                                    NotificationModel model = new NotificationModel();
                                    model.setId(getStringFromJSON(joItem, "id"));
                                    model.setFor_user_name(getStringFromJSON(joItem, "for_user_name"));
                                    model.setBy_user_name(getStringFromJSON(joItem, "by_user_name"));
                                    model.setLi_class(getStringFromJSON(joItem, "li_class"));
                                    model.setI_class(getStringFromJSON(joItem, "i_class"));
                                    model.setStrong_heading(getStringFromJSON(joItem, "strong_heading"));
                                    model.setDetail(getStringFromJSON(joItem, "detail"));
                                    model.setSpan_class(getStringFromJSON(joItem, "span_class"));
                                    model.setTime_of_notify(getStringFromJSON(joItem, "time_of_notify"));
                                    model.setIs_Read(getStringFromJSON(joItem, "is_Read"));
                                    model.setCategory(category);
                                    activityList.add(model);
                                    singleList.add(model);
                                }
                            }
                        }
                    }
                }
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            Constants.notificationMap=new HashMap<>();
            Constants.notificationMap.put(Constants.KEY_MESSAGE,messageList);
            Constants.notificationMap.put(Constants.KEY_HAZARD,hazardList);
            Constants.notificationMap.put(Constants.KEY_REMINDER,reminderList);
            Constants.notificationMap.put(Constants.KEY_ACTIVITY,activityList);
            Constants.notificationList=new ArrayList<>();
            Constants.notificationList.addAll(singleList);
        }
    }

    public UserModel parseUserModel(String str) {
        if (str != null && str.length() > 0)
        {
            try
            {
                JSONObject jsonMain = new JSONObject(str);
                JSONArray joDataArray=jsonMain.optJSONArray("data");
                if(joDataArray!=null && joDataArray.length()>0)
                {
                    JSONObject joData=joDataArray.optJSONObject(0);
                    if (joData != null) {
                        UserModel model = new UserModel();
                        model.setUser_id(getStringFromJSON(joData,"uno"));
                        model.setEmail(getStringFromJSON(joData,"email"));
                        model.setMobileno(getStringFromJSON(joData,"mobileno"));
                        model.setUname(getStringFromJSON(joData,"uname"));
                        model.setCity(getStringFromJSON(joData,"city"));
                        model.setProfession(getStringFromJSON(joData,"profession"));
                        model.setPresent_employment(getStringFromJSON(joData,"present_employment"));
                        model.setDate_of_birth(getStringFromJSON(joData,"date_of_birth"));
                        model.setQualification(getStringFromJSON(joData,"qualification"));
                        model.setAddress(getStringFromJSON(joData,"address"));
                        return model;
                    }
                }
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return null;
    }

    public ArrayList<ReportIssueModel> parseReportIssueList(String str) {
        if (str != null && str.length() > 0)
        {
            ArrayList<ReportIssueModel> issueList=new ArrayList<>();
            try
            {
                JSONObject jsonMain = new JSONObject(str);
                JSONArray joData=jsonMain.optJSONArray("data");
                if(joData!=null && joData.length()>0)
                {
                    for(int i=0;i<joData.length();i++)
                    {
                        JSONObject joItem=joData.optJSONObject(i);
                        if (joItem != null)
                        {
                            ReportIssueModel model = new ReportIssueModel();
                            model.setId(getStringFromJSON(joItem,"Ref_ID"));
                            model.setTitle(getStringFromJSON(joItem,"title"));
                            model.setDescription(getStringFromJSON(joItem,"site_descr"));
                            String photos=getStringFromJSON(joItem,"Site_Pic1");
                            if(!TextUtils.isEmpty(photos))
                            {
                                ArrayList<String> tempList=Constants.commaSeparatedToArrayList(photos);
                                model.setImageList(tempList);
                            }
                            String latStr=getStringFromJSON(joItem, "Location1");
                            if(Constants.isDouble(latStr))
                                model.setLatitude(Double.parseDouble(latStr));
                            else
                                model.setLatitude(0);

                            String longStr=getStringFromJSON(joItem, "Location2");
                            if(Constants.isDouble(longStr))
                                model.setLongitude(Double.parseDouble(longStr));
                            else
                                model.setLongitude(0);
                            String dateStr=getStringFromJSON(joItem,"timestamp");
                            if(!TextUtils.isEmpty(dateStr))
                            {
                                model.setTimestamp(Constants.formatDateGlobal(dateStr));
                            }
                            model.setSurvey_ID(getStringFromJSON(joItem,"Survey_ID"));
                            model.setTest_ID(getStringFromJSON(joItem,"Test_ID"));
                            issueList.add(model);
                        }
                    }
                }
                return issueList;
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return null;
    }


    public ArrayList<HomePageMarkerModel> parseHomePageMarkerList(String str) {
        if (str != null && str.length() > 0)
        {
            ArrayList<HomePageMarkerModel> issueList=new ArrayList<>();
            try
            {
                JSONObject jsonMain = new JSONObject(str);
                JSONArray joData=jsonMain.optJSONArray("data");
                if(joData!=null && joData.length()>0)
                {
                    for(int i=0;i<joData.length();i++)
                    {
                        JSONObject joItem=joData.optJSONObject(i);
                        if (joItem != null)
                        {
                            HomePageMarkerModel model = new HomePageMarkerModel();
                            model.setRef_ID(getStringFromJSON(joItem, "Ref_ID"));
                            model.setLogin_ID(getStringFromJSON(joItem, "Login_ID"));
                            model.setStatus(getStringFromJSON(joItem, "Status"));
                            model.setType(getStringFromJSON(joItem, "type"));
                            model.setParameters(getStringFromJSON(joItem, "parameters"));
                            model.setSurvey_ID(getStringFromJSON(joItem, "Survey_ID"));
                            model.setTest_ID(getStringFromJSON(joItem, "Test_ID"));
                            model.setPayment(getStringFromJSON(joItem, "Payment"));
                            model.setLocation1(getStringFromJSON(joItem, "Location1"));
                            model.setLocation2(getStringFromJSON(joItem, "Location2"));
                            String pic=getStringFromJSON(joItem, "Site_Pic1");
                            if(!TextUtils.isEmpty(pic))
                                pic=pic.trim();
                            model.setSite_Pic1(pic);
                            model.setSite_descr(getStringFromJSON(joItem, "site_descr"));
                            model.setTest_result_para15(getStringFromJSON(joItem, "test_result_para15"));
                            model.setTest_result_para14(getStringFromJSON(joItem, "test_result_para14"));
                            model.setTest_result_para13(getStringFromJSON(joItem, "test_result_para13"));
                            model.setTest_result_para12(getStringFromJSON(joItem, "test_result_para12"));
                            model.setTest_result_para11(getStringFromJSON(joItem, "test_result_para11"));
                            model.setTest_result_para10(getStringFromJSON(joItem, "test_result_para10"));
                            model.setTest_result_para9(getStringFromJSON(joItem, "test_result_para9"));
                            model.setTest_result_para8(getStringFromJSON(joItem, "test_result_para8"));
                            model.setTest_result_para7(getStringFromJSON(joItem, "test_result_para7"));
                            model.setTest_result_para6(getStringFromJSON(joItem, "test_result_para6"));
                            model.setTest_result_para5(getStringFromJSON(joItem, "test_result_para5"));
                            model.setTest_result_para4(getStringFromJSON(joItem, "test_result_para4"));
                            model.setTest_result_para3(getStringFromJSON(joItem, "test_result_para3"));
                            model.setTest_result_para2(getStringFromJSON(joItem, "test_result_para2"));
                            model.setTest_result_para1(getStringFromJSON(joItem, "test_result_para1"));
                            model.setTest_result2_para1(getStringFromJSON(joItem, "test_result2_para1"));
                            model.setTest_result2_para2(getStringFromJSON(joItem, "test_result2_para2"));
                            model.setTest_result2_para3(getStringFromJSON(joItem, "test_result2_para3"));
                            model.setTest_result2_para4(getStringFromJSON(joItem, "test_result2_para4"));
                            model.setTest_result2_para5(getStringFromJSON(joItem, "test_result2_para5"));
                            model.setSurvey_result1(getStringFromJSON(joItem, "survey_result1"));
                            model.setSurvey_result2(getStringFromJSON(joItem, "survey_result2"));
                            model.setSurvey_result3(getStringFromJSON(joItem, "survey_result3"));
                            model.setSurvey_result4(getStringFromJSON(joItem, "survey_result4"));
                            model.setSurvey_result5(getStringFromJSON(joItem, "survey_result5"));
                            model.setTimestamp(getStringFromJSON(joItem, "timestamp"));
                            model.setSt(getStringFromJSON(joItem, "st"));
                            model.setCateg(getStringFromJSON(joItem, "categ"));
                            issueList.add(model);
                        }
                    }
                }
                return issueList;
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return null;
    }


    public ArrayList<ProgramModel> parseProgramList(String str) {
        if (str != null && str.length() > 0)
        {
            ArrayList<ProgramModel> programList=new ArrayList<>();
            try
            {
                JSONObject jsonMain = new JSONObject(str);
                JSONArray joData=jsonMain.optJSONArray("data");
                if(joData!=null && joData.length()>0)
                {
                    for(int i=0;i<joData.length();i++)
                    {
                        JSONObject joItem=joData.optJSONObject(i);
                        if (joItem != null)
                        {
                            ProgramModel model = new ProgramModel();
                            model.setId(getStringFromJSON(joItem, "id"));
                            model.setTitle(getStringFromJSON(joItem, "title"));
                            model.setOwner(getStringFromJSON(joItem, "owner"));
                            model.setCost(getStringFromJSON(joItem, "cost"));
                            model.setCurrency(getStringFromJSON(joItem, "currency"));
                            model.setDetail(getStringFromJSON(joItem, "detail"));
                            model.setLang(getStringFromJSON(joItem, "lang"));
                            model.setLatt(getStringFromJSON(joItem, "latt"));
                            model.setRadius(getStringFromJSON(joItem, "radius"));
                            model.setUpdated(getStringFromJSON(joItem, "updated"));
                            model.setUpdated2(getStringFromJSON(joItem, "updated2"));
                            String photos=getStringFromJSON(joItem, "photos");
                            if(!TextUtils.isEmpty(photos))
                            {
                                ArrayList<String> tempList=Constants.commaSeparatedToArrayList(photos);
                                model.setPhotos(tempList);
                            }
                            model.setCategory(getStringFromJSON(joItem, "category"));
                            model.setLocation_list(getStringFromJSON(joItem, "location_list"));
                            model.setMobileapp_pic(getStringFromJSON(joItem, "mobileapp_pic"));
                            programList.add(model);
                        }
                    }
                }
                return programList;
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return null;
    }


    public ArrayList<VoiceModel> parseVoiceList(String str) {
        if (str != null && str.length() > 0)
        {
            ArrayList<VoiceModel> voiceList=new ArrayList<>();
            try
            {
                JSONObject jsonMain = new JSONObject(str);
                JSONArray joData=jsonMain.optJSONArray("data");
                if(joData!=null && joData.length()>0)
                {
                    for(int i=0;i<joData.length();i++)
                    {
                        JSONObject joItem=joData.optJSONObject(i);
                        if (joItem != null)
                        {
                            VoiceModel model = new VoiceModel();
                            model.setId(getStringFromJSON(joItem,"id"));
                            model.setName(getStringFromJSON(joItem,"voice_clip_name"));
                            voiceList.add(model);
                        }
                    }
                }
                return voiceList;
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return null;
    }

    public String getStringFromJSON(JSONObject json, String key) {
        String value = ""; // Blank string by default.
        try {
            value = json.getString(key);
            if (value.equals("null") || value == null)
                value = "";
            return value;
        } catch (JSONException exp) {
            exp.getMessage();
        }
        return value;  // this wil return BLANk string if object is not prasent.
    }



}
