package com.econote.model;

/**
 * Created by Sudh on 9/11/2016.
 */
public class HomePageMarkerModel
{


    private String user_id;
    private String Ref_ID;
    private String Login_ID;
    private String Status;
    private String type;
    private String parameters;
    private String Survey_ID;
    private String Test_ID;
    private String Payment;
    private String Location1;
    private String Location2;
    private String Site_Pic1;
    private String site_descr;
    private String test_result_para14;
    private String test_result_para15;
    private String test_result_para13;
    private String test_result_para12;
    private String test_result_para11;
    private String test_result_para10;
    private String test_result_para9;
    private String test_result_para8;
    private String test_result_para7;
    private String test_result_para6;
    private String test_result_para5;
    private String test_result_para4;
    private String test_result_para3;
    private String test_result_para2;
    private String test_result_para1;
    private String survey_result1;
    private String survey_result2;
    private String survey_result3;
    private String survey_result4;
    private String survey_result5;
    private String test_result2_para1;
    private String test_result2_para2;
    private String test_result2_para3;
    private String test_result2_para4;
    private String test_result2_para5;
    private String timestamp;
    private String st;
    private String categ;

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getRef_ID() {
        return Ref_ID;
    }

    public void setRef_ID(String ref_ID) {
        Ref_ID = ref_ID;
    }

    public String getLogin_ID() {
        return Login_ID;
    }

    public void setLogin_ID(String login_ID) {
        Login_ID = login_ID;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getParameters() {
        return parameters;
    }

    public void setParameters(String parameters) {
        this.parameters = parameters;
    }

    public String getSurvey_ID() {
        return Survey_ID;
    }

    public void setSurvey_ID(String survey_ID) {
        Survey_ID = survey_ID;
    }

    public String getTest_ID() {
        return Test_ID;
    }

    public void setTest_ID(String test_ID) {
        Test_ID = test_ID;
    }

    public String getPayment() {
        return Payment;
    }

    public void setPayment(String payment) {
        Payment = payment;
    }

    public String getLocation1() {
        return Location1;
    }

    public void setLocation1(String location1) {
        Location1 = location1;
    }

    public String getLocation2() {
        return Location2;
    }

    public void setLocation2(String location2) {
        Location2 = location2;
    }

    public String getSite_Pic1() {
        return Site_Pic1;
    }

    public void setSite_Pic1(String site_Pic1) {
        Site_Pic1 = site_Pic1;
    }

    public String getSite_descr() {
        return site_descr;
    }

    public void setSite_descr(String site_descr) {
        this.site_descr = site_descr;
    }

    public String getTest_result_para14() {
        return test_result_para14;
    }

    public void setTest_result_para14(String test_result_para14) {
        this.test_result_para14 = test_result_para14;
    }

    public String getTest_result_para15() {
        return test_result_para15;
    }

    public void setTest_result_para15(String test_result_para15) {
        this.test_result_para15 = test_result_para15;
    }

    public String getTest_result_para13() {
        return test_result_para13;
    }

    public void setTest_result_para13(String test_result_para13) {
        this.test_result_para13 = test_result_para13;
    }

    public String getTest_result_para12() {
        return test_result_para12;
    }

    public void setTest_result_para12(String test_result_para12) {
        this.test_result_para12 = test_result_para12;
    }

    public String getTest_result_para11() {
        return test_result_para11;
    }

    public void setTest_result_para11(String test_result_para11) {
        this.test_result_para11 = test_result_para11;
    }

    public String getTest_result_para10() {
        return test_result_para10;
    }

    public void setTest_result_para10(String test_result_para10) {
        this.test_result_para10 = test_result_para10;
    }

    public String getTest_result_para9() {
        return test_result_para9;
    }

    public void setTest_result_para9(String test_result_para9) {
        this.test_result_para9 = test_result_para9;
    }

    public String getTest_result_para8() {
        return test_result_para8;
    }

    public void setTest_result_para8(String test_result_para8) {
        this.test_result_para8 = test_result_para8;
    }

    public String getTest_result_para7() {
        return test_result_para7;
    }

    public void setTest_result_para7(String test_result_para7) {
        this.test_result_para7 = test_result_para7;
    }

    public String getTest_result_para6() {
        return test_result_para6;
    }

    public void setTest_result_para6(String test_result_para6) {
        this.test_result_para6 = test_result_para6;
    }

    public String getTest_result_para5() {
        return test_result_para5;
    }

    public void setTest_result_para5(String test_result_para5) {
        this.test_result_para5 = test_result_para5;
    }

    public String getTest_result_para4() {
        return test_result_para4;
    }

    public void setTest_result_para4(String test_result_para4) {
        this.test_result_para4 = test_result_para4;
    }

    public String getTest_result_para3() {
        return test_result_para3;
    }

    public void setTest_result_para3(String test_result_para3) {
        this.test_result_para3 = test_result_para3;
    }

    public String getTest_result_para2() {
        return test_result_para2;
    }

    public void setTest_result_para2(String test_result_para2) {
        this.test_result_para2 = test_result_para2;
    }

    public String getTest_result_para1() {
        return test_result_para1;
    }

    public void setTest_result_para1(String test_result_para1) {
        this.test_result_para1 = test_result_para1;
    }

    public String getSurvey_result1() {
        return survey_result1;
    }

    public void setSurvey_result1(String survey_result1) {
        this.survey_result1 = survey_result1;
    }

    public String getSurvey_result2() {
        return survey_result2;
    }

    public void setSurvey_result2(String survey_result2) {
        this.survey_result2 = survey_result2;
    }

    public String getSurvey_result3() {
        return survey_result3;
    }

    public void setSurvey_result3(String survey_result3) {
        this.survey_result3 = survey_result3;
    }

    public String getSurvey_result4() {
        return survey_result4;
    }

    public void setSurvey_result4(String survey_result4) {
        this.survey_result4 = survey_result4;
    }

    public String getSurvey_result5() {
        return survey_result5;
    }

    public void setSurvey_result5(String survey_result5) {
        this.survey_result5 = survey_result5;
    }

    public String getTest_result2_para1() {
        return test_result2_para1;
    }

    public void setTest_result2_para1(String test_result2_para1) {
        this.test_result2_para1 = test_result2_para1;
    }

    public String getTest_result2_para2() {
        return test_result2_para2;
    }

    public void setTest_result2_para2(String test_result2_para2) {
        this.test_result2_para2 = test_result2_para2;
    }

    public String getTest_result2_para3() {
        return test_result2_para3;
    }

    public void setTest_result2_para3(String test_result2_para3) {
        this.test_result2_para3 = test_result2_para3;
    }

    public String getTest_result2_para4() {
        return test_result2_para4;
    }

    public void setTest_result2_para4(String test_result2_para4) {
        this.test_result2_para4 = test_result2_para4;
    }

    public String getTest_result2_para5() {
        return test_result2_para5;
    }

    public void setTest_result2_para5(String test_result2_para5) {
        this.test_result2_para5 = test_result2_para5;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getSt() {
        return st;
    }

    public void setSt(String st) {
        this.st = st;
    }

    public String getCateg() {
        return categ;
    }

    public void setCateg(String categ) {
        this.categ = categ;
    }
}
