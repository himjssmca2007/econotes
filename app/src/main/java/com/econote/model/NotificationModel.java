package com.econote.model;

/**
 * Created by Sudh on 9/11/2016.
 */
public class NotificationModel
{
    private String id;
    private String for_user_name;
    private String by_user_name;
    private String li_class;
    private String i_class;
    private String strong_heading;
    private String detail;
    private String span_class;
    private String time_of_notify;
    private String is_Read;
    private String category;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFor_user_name() {
        return for_user_name;
    }

    public void setFor_user_name(String for_user_name) {
        this.for_user_name = for_user_name;
    }

    public String getBy_user_name() {
        return by_user_name;
    }

    public void setBy_user_name(String by_user_name) {
        this.by_user_name = by_user_name;
    }

    public String getLi_class() {
        return li_class;
    }

    public void setLi_class(String li_class) {
        this.li_class = li_class;
    }

    public String getI_class() {
        return i_class;
    }

    public void setI_class(String i_class) {
        this.i_class = i_class;
    }

    public String getStrong_heading() {
        return strong_heading;
    }

    public void setStrong_heading(String strong_heading) {
        this.strong_heading = strong_heading;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getSpan_class() {
        return span_class;
    }

    public void setSpan_class(String span_class) {
        this.span_class = span_class;
    }

    public String getTime_of_notify() {
        return time_of_notify;
    }

    public void setTime_of_notify(String time_of_notify) {
        this.time_of_notify = time_of_notify;
    }

    public String getIs_Read() {
        return is_Read;
    }

    public void setIs_Read(String is_Read) {
        this.is_Read = is_Read;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
