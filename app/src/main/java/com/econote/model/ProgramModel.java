package com.econote.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Sudh on 9/3/2016.
 */
public class ProgramModel implements Parcelable
{
    private String id;
    private String title;
    private String owner;
    private String cost;
    private String currency;
    private String detail;
    private String lang;
    private String latt;
    private String radius;
    private String updated;
    private String updated2;
    private ArrayList<String> photos;
    private String category;
    private String location_list;
    private String mobileapp_pic;
    public boolean selected;

    public ProgramModel()
    {

    }

    public String getMobileapp_pic() {
        return mobileapp_pic;
    }

    public void setMobileapp_pic(String mobileapp_pic) {
        this.mobileapp_pic = mobileapp_pic;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getLatt() {
        return latt;
    }

    public void setLatt(String latt) {
        this.latt = latt;
    }

    public String getRadius() {
        return radius;
    }

    public void setRadius(String radius) {
        this.radius = radius;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public String getUpdated2() {
        return updated2;
    }

    public void setUpdated2(String updated2) {
        this.updated2 = updated2;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getLocation_list() {
        return location_list;
    }

    public void setLocation_list(String location_list) {
        this.location_list = location_list;
    }

    public ArrayList<String> getPhotos() {
        return photos;
    }

    public void setPhotos(ArrayList<String> photos) {
        this.photos = photos;
    }


    @Override
    public void writeToParcel(Parcel dest, int flags) {
        // TODO Auto-generated method stub
        dest.writeString(id);
        dest.writeString(title);
        dest.writeString(owner);
        dest.writeString(cost);
        dest.writeString(currency);
        dest.writeString(detail);
        dest.writeString(lang);
        dest.writeString(latt);
        dest.writeString(radius);
        dest.writeString(updated);
        dest.writeString(updated2);
        dest.writeString(category);
        dest.writeString(location_list);
        dest.writeList(photos);
        dest.writeString(mobileapp_pic);
    }


    public ProgramModel(Parcel source) {
        super();
        this.id=source.readString();
        this.title=source.readString();
        this.owner=source.readString();
        this.cost=source.readString();
        this.currency=source.readString();
        this.detail=source.readString();
        this.lang=source.readString();
        this.latt=source.readString();
        this.radius=source.readString();
        this.updated=source.readString();
        this.updated2=source.readString();
        this.category=source.readString();
        this.location_list=source.readString();
        this.photos=source.readArrayList(ProgramModel.class.getClassLoader());
        this.mobileapp_pic=source.readString();
    }



    public static final Parcelable.Creator<ProgramModel> CREATOR = new Parcelable.Creator<ProgramModel>() {

        @Override
        public ProgramModel createFromParcel(Parcel source) {
            return new ProgramModel(source);
        }

        @Override
        public ProgramModel[] newArray(int size) {
            return new ProgramModel[size];
        }
    };

    @Override
    public int describeContents() {
        // TODO Auto-generated method stub
        return 0;
    }
}
