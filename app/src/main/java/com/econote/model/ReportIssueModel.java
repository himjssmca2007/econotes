package com.econote.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Sudh on 9/3/2016.
 */
public class ReportIssueModel implements Parcelable
{
    private String id;
    private double latitude;
    private double longitude;
    private String description;
    private String selectedCategory;
    private ArrayList<String> imageList;
    private String title;
    private String timestamp;
    private String Survey_ID;
    private String Test_ID;

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ReportIssueModel()
    {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSelectedCategory() {
        return selectedCategory;
    }

    public void setSelectedCategory(String selectedCategory) {
        this.selectedCategory = selectedCategory;
    }

    public ArrayList<String> getImageList() {
        return imageList;
    }

    public void setImageList(ArrayList<String> imageList) {
        this.imageList = imageList;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getSurvey_ID() {
        return Survey_ID;
    }

    public void setSurvey_ID(String survey_ID) {
        Survey_ID = survey_ID;
    }

    public String getTest_ID() {
        return Test_ID;
    }

    public void setTest_ID(String test_ID) {
        Test_ID = test_ID;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        // TODO Auto-generated method stub
        dest.writeString(id);
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
        dest.writeString(description);
        dest.writeString(selectedCategory);
        dest.writeString(title);
        dest.writeList(imageList);
        dest.writeString(timestamp);
        dest.writeString(Survey_ID);
        dest.writeString(Test_ID);
    }

    @SuppressWarnings("unchecked")
    public ReportIssueModel(Parcel source) {
        super();
        this.id=source.readString();
        this.latitude=source.readDouble();
        this.longitude=source.readDouble();
        this.description=source.readString();
        this.selectedCategory=source.readString();
        this.title=source.readString();
        this.imageList=source.readArrayList(ReportIssueModel.class.getClassLoader());
        this.timestamp=source.readString();
        this.Survey_ID=source.readString();
        this.Test_ID=source.readString();
    }



    public static final Parcelable.Creator<ReportIssueModel> CREATOR = new Parcelable.Creator<ReportIssueModel>() {

        @Override
        public ReportIssueModel createFromParcel(Parcel source) {
            return new ReportIssueModel(source);
        }

        @Override
        public ReportIssueModel[] newArray(int size) {
            return new ReportIssueModel[size];
        }
    };

    @Override
    public int describeContents() {
        // TODO Auto-generated method stub
        return 0;
    }
}
