package com.econote.model;

/**
 * Created by Sudh on 9/3/2016.
 */
public class VoiceModel
{
    private String id;
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }




}
