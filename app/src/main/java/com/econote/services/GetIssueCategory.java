package com.econote.services;

import org.json.JSONObject;
import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;

import com.econote.json.JsonParser;
import com.econote.model.IssueCategoryModel;
import com.econote.utils.Constants;
import com.econote.utils.HttpCall;

import java.util.ArrayList;

public class GetIssueCategory extends IntentService {
	private String url;
	private boolean success;
	private String response_message;
	private String data;
	public GetIssueCategory() {
		super("GetIssueCategory");
	}

	@Override
	protected void onHandleIntent(Intent intent) {		
		ResultReceiver receiver = (ResultReceiver) intent.getParcelableExtra("receiver");	
		url=intent.getStringExtra("url");
		data = intent.getStringExtra("data");
		String resp=new HttpCall().executeHttpPostRequest(url,data);
		if (resp != null) 
		{			
			try
			{			
			JSONObject jo = new JSONObject(resp);
			if (jo.optBoolean("result", false))
			{
				ArrayList<IssueCategoryModel> tempList=new JsonParser().parseIssueCategory(resp);
				if(tempList!=null && tempList.size()>0)
				{
					Constants.categoryList=tempList;
					success=true;
				}
				else
				{
					success=false;
					response_message=Constants.SERVER_EXCEPTION_MSG;
				}
			} 
			else 
			{
				success=false;	
				response_message=jo.optString("msg",Constants.SERVER_EXCEPTION_MSG);
			}
			}
			catch(Exception e)
			{
				success=false;	
				response_message= Constants.SERVER_EXCEPTION_MSG;
			}
		}
		else
		{
			success=false;	
			response_message="Timeout From Server.Please try again.";
		}
		
		Bundle resultData = new Bundle();
		resultData.putBoolean("success", success);
		resultData.putString("response_message",response_message);
		receiver.send(Constants.REQUEST_ISSUE_CATEGORY,resultData);
	}

}