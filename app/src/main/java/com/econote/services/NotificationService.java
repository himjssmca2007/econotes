package com.econote.services;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import com.econote.json.JsonParser;
import com.econote.utils.Constants;
import com.econote.utils.HttpCall;
import org.json.JSONObject;
import java.net.URLEncoder;

public class NotificationService extends IntentService {
    private String data;
    private String email;

    public NotificationService() {
        super("NotificationService");
    }

    @Override
    protected void onHandleIntent(Intent intent)
    {
        email = intent.getStringExtra("email");
        data=createNotificationRequestData(email);
        if(Constants.isNetworkAvailable(getApplicationContext()))
        {
            String resp = new HttpCall().executeHttpPostRequest(Constants.GLOBAL_URL, data);
            if (resp != null) {
                try {
                    JSONObject jo = new JSONObject(resp);
                    if (jo.optBoolean("result", false)) {
                        new JsonParser().parseNotificationList(resp);
                        Intent intentCount = new Intent(Constants.INTENT_FILTER_NOTIFICATION);
                        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intentCount);
                    }
                } catch (Exception e) {

                }
            }
        }
    }

    private String createNotificationRequestData(String email) {
        String data = "";
        try {
            data += URLEncoder.encode("user_email", "UTF-8")
                    + "=" + URLEncoder.encode(email, "UTF-8");

            data += "&" + URLEncoder.encode("passphase", "UTF-8") + "="
                    + URLEncoder.encode(Constants.PASSPHRASE, "UTF-8");

            data += "&" + URLEncoder.encode("auth", "UTF-8") + "="
                    + URLEncoder.encode(Constants.AUTHKEY, "UTF-8");

            data += "&" + URLEncoder.encode("action", "UTF-8") + "="
                    + URLEncoder.encode("getNotificationCount", "UTF-8");

            Log.e("Himanshu", "Dixit Notification Data" + data);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return data;
    }
}