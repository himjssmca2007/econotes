package com.econote.utils;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings.Secure;
import android.support.v4.app.ActivityCompat;
import android.test.mock.MockPackageManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.Toast;

import com.econote.R;
import com.econote.model.IssueCategoryModel;
import com.econote.model.NotificationModel;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Constants {


	public static String SERVER_EXCEPTION_MSG = "There is some problem in response from server.";
	public static ProgressDialog pd;
	public static final int REQUEST_CODE_PERMISSION = 2;
	public static final int REQUEST_LOCATION_SERVICE = 2;
	public static String PASSPHRASE = "6526a16d478ae81e370c20f239619072";
	public static String AUTHKEY = "bccc05b73096906c46ab73fd61a986af";
	public static String GLOBAL_URL = "http://econotes.in/MobileApi/index.php";
	public static SQLiteDatabase db;
	public static final int DEFAULT_HTTP_CONNECT_TIMEOUT = 5 * 1000; // milliseconds
	public static final int DEFAULT_HTTP_READ_TIMEOUT = 20 * 1000; // milliseconds


	public static final int MAX_AUDIO_TIME =1000*60*2;


	public static ArrayList<IssueCategoryModel> categoryList;
	public static HashMap<String, ArrayList<NotificationModel>> notificationMap;
	public static ArrayList<NotificationModel> notificationList;
	public static final String KEY_MESSAGE = "message";
	public static final String KEY_HAZARD = "hazard";
	public static final String KEY_REMINDER = "reminder";
	public static final String KEY_ACTIVITY = "activity";

	public static String INTENT_FILTER_NOTIFICATION = "com.econote.notification.NotificationBroadcast";


	public static DisplayImageOptions options = new DisplayImageOptions.Builder()
			.showImageOnLoading(R.drawable.ic_loading) // resource or drawable
			.showImageForEmptyUri(R.drawable.ic_loading) // resource or drawable
			.showImageOnFail(R.drawable.ic_loading) // resource or drawable
			.resetViewBeforeLoading(true)  // default
			.cacheInMemory(true) // default
			.cacheOnDisk(true) // default
			.considerExifParams(false) // default
			.imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2) // default
			.bitmapConfig(Bitmap.Config.ARGB_8888) // default
			.build();


	public static final int REQUEST_ISSUE_CATEGORY = 100;
	public static final int REQUEST_LOCATION = 101;
	public static final int REQUEST_PIC = 102;
	public static final int REQUEST_PROGRAM = 103;
	public static final int REQUEST_PROGRAM_DETAIL = 104;
	public static final int REQUEST_JOIN_SUBMIT = 105;
	public static final int REQUEST_RECORD = 106;

	public static String[] mPermission = {Manifest.permission.INTERNET,
			Manifest.permission.ACCESS_FINE_LOCATION,
			Manifest.permission.ACCESS_COARSE_LOCATION,
			Manifest.permission.WRITE_EXTERNAL_STORAGE,
			Manifest.permission.READ_EXTERNAL_STORAGE,
			Manifest.permission.GET_ACCOUNTS,
			Manifest.permission.READ_CONTACTS,
			Manifest.permission.CAMERA,
			Manifest.permission.ACCESS_NETWORK_STATE,
	};

	public static boolean checkMarshmallowPermission(Context con, Activity activity) {
		try {
			if (ActivityCompat.checkSelfPermission(con, Constants.mPermission[0])
					!= PackageManager.PERMISSION_GRANTED ||
					ActivityCompat.checkSelfPermission(con, Constants.mPermission[1])
							!= PackageManager.PERMISSION_GRANTED ||
					ActivityCompat.checkSelfPermission(con, Constants.mPermission[2])
							!= PackageManager.PERMISSION_GRANTED ||
					ActivityCompat.checkSelfPermission(con, Constants.mPermission[3])
							!= PackageManager.PERMISSION_GRANTED ||
					ActivityCompat.checkSelfPermission(con, Constants.mPermission[4])
							!= PackageManager.PERMISSION_GRANTED ||
					ActivityCompat.checkSelfPermission(con, Constants.mPermission[5])
							!= PackageManager.PERMISSION_GRANTED ||
					ActivityCompat.checkSelfPermission(con, Constants.mPermission[6])
							!= PackageManager.PERMISSION_GRANTED ||
					ActivityCompat.checkSelfPermission(con, Constants.mPermission[7])
							!= PackageManager.PERMISSION_GRANTED ||
					ActivityCompat.checkSelfPermission(con, Constants.mPermission[8])
							!= PackageManager.PERMISSION_GRANTED) {

				ActivityCompat.requestPermissions(activity,
						Constants.mPermission, Constants.REQUEST_CODE_PERMISSION);
				return false;
				// If any permission aboe not allowed by user, this condition will execute every tim, else your else part will work
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}

	public static boolean checkMarshmallowPermissionGranted(int requestCode, String[] permissions, int[] grantResults) {
		boolean permission_bool = false;
		if (requestCode == Constants.REQUEST_CODE_PERMISSION) {
			if (grantResults.length == 9 &&
					grantResults[0] == PackageManager.PERMISSION_GRANTED &&
					grantResults[1] == PackageManager.PERMISSION_GRANTED &&
					grantResults[2] == PackageManager.PERMISSION_GRANTED &&
					grantResults[3] == PackageManager.PERMISSION_GRANTED &&
					grantResults[4] == PackageManager.PERMISSION_GRANTED &&
					grantResults[5] == PackageManager.PERMISSION_GRANTED &&
					grantResults[6] == PackageManager.PERMISSION_GRANTED &&
					grantResults[7] == PackageManager.PERMISSION_GRANTED &&
					grantResults[8] == PackageManager.PERMISSION_GRANTED) {
				// Success Stuff here
				permission_bool = true;
			} else
				permission_bool = false;

		}
		return permission_bool;
	}

	public static String getUUID(Context context) {
		return Secure.getString(context.getContentResolver(), Secure.ANDROID_ID);
	}


	public static boolean isNetworkAvailable(Context con) {
		try {
			ConnectivityManager cm = (ConnectivityManager) con
					.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo networkInfo = cm.getActiveNetworkInfo();

			if (networkInfo != null && networkInfo.isConnected()) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public static void showProgressDialog(Context con) {
		hideProgressDialog();
		pd = ProgressDialog.show(con, null, "Please wait...");
		pd.setCancelable(false);
	}

	public static void hideProgressDialog() {
		if (pd != null && pd.isShowing()) {
			pd.dismiss();
			pd = null;
		}
	}

	public static void ShowNetworkError(Context con) {
		GlobalAlertDialogs.createAlertSingle((Activity) con, "Please check your internet connection!", "OK", false);
	}

	public static void ShowNetworkErrorToast(Context con) {
		Toast.makeText(con, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
	}

	public static void ShowToast(Context con, String message) {
		Toast.makeText(con, message, Toast.LENGTH_SHORT).show();
	}

	public static boolean validateEmail(String strEmail) {
		Matcher matcher;
		String EMAIL_PATTERN = "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" + "\\@"
				+ "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" + "(" + "\\."
				+ "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" + ")+";
		Pattern pattern = Pattern.compile(EMAIL_PATTERN);
		matcher = pattern.matcher(strEmail);
		return matcher.matches();
	}

	public static boolean validatePassword(String password) {
		Pattern pswNamePtrn = Pattern.compile("((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,15})");
		Matcher mtch = pswNamePtrn.matcher(password);
		if (mtch.matches()) {
			return true;
		}
		return false;
	}


	public static class Config {
		public static final boolean DEVELOPER_MODE = false;
	}


	public static String printDifference(String str) {
		// milliseconds
		SimpleDateFormat sdparse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		sdparse.setTimeZone(TimeZone.getDefault());
		Date startDate = null;
		try {
			startDate = sdparse.parse(str);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Date endDate = new Date();
		long different = endDate.getTime() - startDate.getTime();
		long secondsInMilli = 1000;
		long minutesInMilli = secondsInMilli * 60;
		long hoursInMilli = minutesInMilli * 60;
		long daysInMilli = hoursInMilli * 24;
		long monthsInMilli = daysInMilli * 30;
		long yearsinMilli = daysInMilli * 365;

		long elapsedYear = different / yearsinMilli;
		if (elapsedYear > 0)
			return elapsedYear + " year(s) ago";
		different = different % yearsinMilli;

		long elapsedMonth = different / monthsInMilli;
		if (elapsedMonth > 0)
			return elapsedMonth + " month(s) ago";
		different = different % monthsInMilli;

		long elapsedDays = different / daysInMilli;
		if (elapsedDays > 0)
			return elapsedDays + " day(s) ago";
		different = different % daysInMilli;

		long elapsedHours = different / hoursInMilli;
		if (elapsedHours > 0)
			return elapsedHours + " hour(s) ago";
		different = different % hoursInMilli;

		long elapsedMinutes = different / minutesInMilli;
		if (elapsedMinutes > 0)
			return elapsedMinutes + " minute(s) ago";
		different = different % minutesInMilli;

		long elapsedSeconds = different / secondsInMilli;
		if (elapsedSeconds > 0)
			return elapsedSeconds + " second(s) ago";
		else
			return "Just now";
	}


	public static int convertDpToPixel(Context context, float dp) {
		DisplayMetrics metrics = context.getResources().getDisplayMetrics();
		float px = dp * (metrics.densityDpi / 160f);
		return (int) px;
	}

	public static ArrayList<String> commaSeparatedToArrayList(String commaSeparated) {
		if (!TextUtils.isEmpty(commaSeparated)) {
			String delimeter = ",";
			String[] temp = commaSeparated.split(delimeter);
			if (temp != null && temp.length > 0) {
				ArrayList<String> tempList = new ArrayList<String>(Arrays.asList(temp));
				return tempList;
			}
		}
		return null;
	}


	public static boolean isDouble(String str) {
		try {
			Double.parseDouble(str);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}


	/**********************************
	 * COMPRESS IMAGE
	 *************************************************/
	public static String compressImage(String imageUri, Context context) {

		String filePath = getRealPathFromURI(imageUri, context);
		Bitmap scaledBitmap = null;

		BitmapFactory.Options options = new BitmapFactory.Options();

//      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
//      you try the use the bitmap here, you will get null.
		options.inJustDecodeBounds = true;
		Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

		int actualHeight = options.outHeight;
		int actualWidth = options.outWidth;

//      max Height and width values of the compressed image is taken as 816x612

		float maxHeight = 816.0f;
		float maxWidth = 612.0f;
		float imgRatio = actualWidth / actualHeight;
		float maxRatio = maxWidth / maxHeight;

//      width and height values are set maintaining the aspect ratio of the image

		if (actualHeight > maxHeight || actualWidth > maxWidth) {
			if (imgRatio < maxRatio) {
				imgRatio = maxHeight / actualHeight;
				actualWidth = (int) (imgRatio * actualWidth);
				actualHeight = (int) maxHeight;
			} else if (imgRatio > maxRatio) {
				imgRatio = maxWidth / actualWidth;
				actualHeight = (int) (imgRatio * actualHeight);
				actualWidth = (int) maxWidth;
			} else {
				actualHeight = (int) maxHeight;
				actualWidth = (int) maxWidth;

			}
		}

//      setting inSampleSize value allows to load a scaled down version of the original image

		options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

//      inJustDecodeBounds set to false to load the actual bitmap
		options.inJustDecodeBounds = false;

//      this options allow android to claim the bitmap memory if it runs low on memory
		options.inPurgeable = true;
		options.inInputShareable = true;
		options.inTempStorage = new byte[16 * 1024];

		try {
//          load the bitmap from its path
			bmp = BitmapFactory.decodeFile(filePath, options);
		} catch (OutOfMemoryError exception) {
			exception.printStackTrace();

		}
		try {
			scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
		} catch (OutOfMemoryError exception) {
			exception.printStackTrace();
		}

		float ratioX = actualWidth / (float) options.outWidth;
		float ratioY = actualHeight / (float) options.outHeight;
		float middleX = actualWidth / 2.0f;
		float middleY = actualHeight / 2.0f;

		Matrix scaleMatrix = new Matrix();
		scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

		Canvas canvas = new Canvas(scaledBitmap);
		canvas.setMatrix(scaleMatrix);
		canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

//      check the rotation of the image and display it properly
		ExifInterface exif;
		try {
			exif = new ExifInterface(filePath);

			int orientation = exif.getAttributeInt(
					ExifInterface.TAG_ORIENTATION, 0);
			Log.d("EXIF", "Exif: " + orientation);
			Matrix matrix = new Matrix();
			if (orientation == 6) {
				matrix.postRotate(90);
				Log.d("EXIF", "Exif: " + orientation);
			} else if (orientation == 3) {
				matrix.postRotate(180);
				Log.d("EXIF", "Exif: " + orientation);
			} else if (orientation == 8) {
				matrix.postRotate(270);
				Log.d("EXIF", "Exif: " + orientation);
			}
			scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
					scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
					true);
		} catch (IOException e) {
			e.printStackTrace();
		}

		FileOutputStream out = null;
		String filename = getFilename(context);
		try {
			out = new FileOutputStream(filename);

//          write the compressed bitmap at the destination specified by filename.
			scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		return filename;

	}

	public static String getFilename(Context context) {
		File file = new File(Environment.getExternalStorageDirectory().getPath(), context.getPackageName() + "/Images");
		if (!file.exists()) {
			file.mkdirs();
		}
		String uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
		return uriSting;

	}

	private static String getRealPathFromURI(String contentURI, Context context) {
		Uri contentUri = Uri.parse(contentURI);
		Cursor cursor = context.getContentResolver().query(contentUri, null, null, null, null);
		if (cursor == null) {
			return contentUri.getPath();
		} else {
			cursor.moveToFirst();
			int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
			return cursor.getString(index);
		}
	}

	public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {
			final int heightRatio = Math.round((float) height / (float) reqHeight);
			final int widthRatio = Math.round((float) width / (float) reqWidth);
			inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
		}
		final float totalPixels = width * height;
		final float totalReqPixelsCap = reqWidth * reqHeight * 2;
		while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
			inSampleSize++;
		}

		return inSampleSize;
	}

	public static void deleteFilename() {
		File file = new File(Environment.getExternalStorageDirectory().getPath(), "temp.jpg");
		if (file.exists()) {
			file.delete();
		}
	}

	public static final String md5(final String s) {
		final String MD5 = "MD5";
		try {
			// Create MD5 Hash
			MessageDigest digest = MessageDigest.getInstance(MD5);
			digest.update(s.getBytes());
			byte messageDigest[] = digest.digest();

			// Create Hex String
			StringBuilder hexString = new StringBuilder();
			for (byte aMessageDigest : messageDigest) {
				String h = Integer.toHexString(0xFF & aMessageDigest);
				while (h.length() < 2)
					h = "0" + h;
				hexString.append(h);
			}
			return hexString.toString();

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return "";
	}

	public static double getDoubleValue(String str) {
		if (TextUtils.isEmpty(str))
			return 0;
		try {
			return Double.valueOf(str);
		} catch (NumberFormatException e) {

		}
		return 0;
	}

	public static String formatDateGlobal(String str) {
		if (TextUtils.isEmpty(str))
			return "";
		SimpleDateFormat sdParse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		sdParse.setTimeZone(TimeZone.getTimeZone("UTC"));
		Date date = null;
		try {
			date = sdParse.parse(str);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
		SimpleDateFormat sdFormat = new SimpleDateFormat("EEE, dd MMM yyyy hh:mm a");
		sdFormat.setTimeZone(TimeZone.getDefault());
		return (sdFormat.format(date));
	}
}
