package com.econote.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.econote.Application.ApplicationClass;
import com.econote.LoginActivity;
import com.econote.R;
import com.google.android.gms.analytics.HitBuilders;

public class GlobalTracker {


    public static void setScreen(String screeName) {
        ApplicationClass.tracker.setScreenName(screeName);
        ApplicationClass.tracker.send(new HitBuilders.ScreenViewBuilder().build());
    }
}
