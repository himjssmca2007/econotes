package com.econote.utils;

import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;

import com.econote.model.JoinProgramModel;
import com.econote.model.ReportIssueModel;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;


public class HttpCall {

    String res = null;

    public String callJsnGet(String urlString) {
        Log.e("Himanshu", "Dixit Call Url" + urlString);
        StringBuffer chaine = new StringBuffer("");
        try {
            urlString = urlString.replaceAll(" ", "%20");
            URL url = new URL(urlString);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestProperty("User-Agent", "");
            connection.setRequestMethod("GET");
            connection.setDoInput(true);
            connection.connect();
            InputStream inputStream = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(inputStream));
            String line = "";
            while ((line = rd.readLine()) != null) {
                chaine.append(line);
            }

        } catch (IOException e) {
            // writing exception to log
            e.printStackTrace();
            return null;
        }
        Log.e("Himanshu", "Dixit Response String" + chaine.toString());
        return chaine.toString();

    }


    public String callJsnPost(String urlString) {
        Log.e("Himanshu", "Dixit Call Url" + urlString);
        StringBuffer chaine = new StringBuffer("");
        try {
            urlString = urlString.replaceAll(" ", "%20");
            URL url = new URL(urlString);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestProperty("User-Agent", "");
            connection.setRequestMethod("POST");
            connection.setDoInput(true);
            connection.connect();
            InputStream inputStream = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(inputStream));
            String line = "";
            while ((line = rd.readLine()) != null) {
                chaine.append(line);
            }

        } catch (IOException e) {
            // writing exception to log
            e.printStackTrace();
            return null;
        }
        return chaine.toString();
    }

    public String callJsnDelete(String urlString) {
        StringBuffer chaine = new StringBuffer("");
        try {
            urlString = urlString.replaceAll(" ", "%20");
            URL url = new URL(urlString);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestProperty("User-Agent", "");
            connection.setRequestMethod("DELETE");
            connection.setDoInput(true);
            connection.connect();
            InputStream inputStream = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(inputStream));
            String line = "";
            while ((line = rd.readLine()) != null) {
                chaine.append(line);
            }

        } catch (IOException e) {
            // writing exception to log
            e.printStackTrace();
            return null;
        }
        return chaine.toString();

    }



    public String executeHttpPostRequest(String server_url, String data) {
        String result = "";
        try {
            URL url = new URL(server_url.replaceAll(" ", "%20"));
            Log.e("Himanshu", "Dixit Api Url" + url);
            URLConnection connection = url.openConnection();

            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setUseCaches(false);
            connection.addRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setConnectTimeout(Constants.DEFAULT_HTTP_CONNECT_TIMEOUT);
            connection.setReadTimeout(Constants.DEFAULT_HTTP_READ_TIMEOUT);
            // Send the POST data
            DataOutputStream dataOut = new DataOutputStream(connection.getOutputStream());
            dataOut.writeBytes(data);
            dataOut.flush();
            dataOut.close();

            // get the response from the server and store it in result
            DataInputStream dataIn = new DataInputStream(connection.getInputStream());
            String inputLine;
            while ((inputLine = dataIn.readLine()) != null) {
                result += inputLine;
            }
            dataIn.close();
        } catch (IOException e) {
            e.printStackTrace();
            result = null;
        }
        Log.e("Himanshu","Dixit Api Response"+result);
        return result;
    }



    public String doIssueUpload(String url_update,ReportIssueModel model,String userId)
    {
        String response = null;
        HttpURLConnection conn = null;
        DataOutputStream dos = null;
        InputStream inputStream = null;
        String existingFileName = "";
        FileInputStream fileInputStream = null;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        int maxBufferSize = 1024;
        String urlString = url_update.replaceAll(" ", "%20");
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        long size = 0, total_bytes_read = 0;

        try {
            //------------------ CLIENT REQUEST

            // open a URL connection to the Servlet
            URL url = new URL(urlString);
            conn = (HttpURLConnection) url.openConnection();
            // Allow Inputs
            conn.setDoInput(true);
            // Allow Outputs
            conn.setDoOutput(true);
            // Don't use a cached copy.
            conn.setUseCaches(false);
            // Use a post method.
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Connection", "Keep-Alive");
            conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
            dos = new DataOutputStream(conn.getOutputStream());

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"action\"" + lineEnd + lineEnd);
            dos.writeBytes("uploadImageWithDetail"+lineEnd);

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"passphase\"" + lineEnd + lineEnd);
            dos.writeBytes(Constants.PASSPHRASE+lineEnd);

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"auth\"" + lineEnd + lineEnd);
            dos.writeBytes(Constants.AUTHKEY+lineEnd);

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"email\"" + lineEnd + lineEnd);
            dos.writeBytes(userId+lineEnd);

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"description\"" + lineEnd + lineEnd);
            dos.writeBytes(model.getDescription()+lineEnd);

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"cetegory_id\"" + lineEnd + lineEnd);
            dos.writeBytes(model.getSelectedCategory()+lineEnd);

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"lat\"" + lineEnd + lineEnd);
            dos.writeBytes(String.valueOf(model.getLatitude())+lineEnd);

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"long\"" + lineEnd + lineEnd);
            dos.writeBytes(String.valueOf(model.getLongitude())+lineEnd);

            ArrayList<String> media_file=model.getImageList();
            for (int i = 0; i < media_file.size(); i++)
            {
                if (!TextUtils.isEmpty(media_file.get(i))) {
                    existingFileName = media_file.get(i).substring(media_file.get(i).lastIndexOf('/') + 1, media_file.get(i).length());
                    File file = new File(media_file.get(i));
                    size = file.length();
                    fileInputStream = new FileInputStream(file);
                }

                dos.writeBytes(twoHyphens + boundary + lineEnd);
                dos.writeBytes("Content-Disposition:form-data; name=\"image"+i+"\";filename=\"" + existingFileName + "\"" + lineEnd);
                dos.writeBytes(lineEnd);
                // create a buffer of maximum size
                if (fileInputStream != null) {
                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    buffer = new byte[bufferSize];
                    // read file and write it into form...
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                    while (bytesRead > 0) {
                        total_bytes_read += bytesRead;
                        dos.write(buffer, 0, bufferSize);
                        bytesAvailable = fileInputStream.available();
                        bufferSize = Math.min(bytesAvailable, maxBufferSize);
                        bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                    }
                }
                // send multipart form data necesssary after file data...
                dos.writeBytes(lineEnd);
                dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
            }
            try
            {
                inputStream = conn.getInputStream();
            }
            catch (Exception e)
            {
                if(inputStream==null)
                    inputStream=conn.getErrorStream();
            }
            response = convertStreamToString(inputStream);
            if (fileInputStream != null)
                fileInputStream.close();
            dos.flush();
            dos.close();
            Log.e("Himanshu","Dixit Upload API Response"+response);
            return response;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }


    public String doIssueStatusUpdate(String url_update,String userId,String id,String status,String description,ArrayList<String> images)
    {
        String response = null;
        HttpURLConnection conn = null;
        DataOutputStream dos = null;
        InputStream inputStream = null;
        String existingFileName = "";
        FileInputStream fileInputStream = null;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        int maxBufferSize = 1024;
        String urlString = url_update.replaceAll(" ", "%20");
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        long size = 0, total_bytes_read = 0;

        try {
            //------------------ CLIENT REQUEST

            // open a URL connection to the Servlet
            URL url = new URL(urlString);
            conn = (HttpURLConnection) url.openConnection();
            // Allow Inputs
            conn.setDoInput(true);
            // Allow Outputs
            conn.setDoOutput(true);
            // Don't use a cached copy.
            conn.setUseCaches(false);
            // Use a post method.
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Connection", "Keep-Alive");
            conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
            dos = new DataOutputStream(conn.getOutputStream());

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"action\"" + lineEnd + lineEnd);
            dos.writeBytes("uploadReportStatus"+lineEnd);

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"id\"" + lineEnd + lineEnd);
            dos.writeBytes(id+lineEnd);

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"passphase\"" + lineEnd + lineEnd);
            dos.writeBytes(Constants.PASSPHRASE+lineEnd);

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"auth\"" + lineEnd + lineEnd);
            dos.writeBytes(Constants.AUTHKEY+lineEnd);


            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"description\"" + lineEnd + lineEnd);
            dos.writeBytes(description+lineEnd);

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"status\"" + lineEnd + lineEnd);
            dos.writeBytes(status+lineEnd);

            ArrayList<String> media_file=images;
            for (int i = 0; i < media_file.size(); i++)
            {
                if (!TextUtils.isEmpty(media_file.get(i))) {
                    existingFileName = media_file.get(i).substring(media_file.get(i).lastIndexOf('/') + 1, media_file.get(i).length());
                    File file = new File(media_file.get(i));
                    size = file.length();
                    fileInputStream = new FileInputStream(file);
                }

                dos.writeBytes(twoHyphens + boundary + lineEnd);
                dos.writeBytes("Content-Disposition:form-data; name=\"image"+i+"\";filename=\"" + existingFileName + "\"" + lineEnd);
                dos.writeBytes(lineEnd);
                // create a buffer of maximum size
                if (fileInputStream != null) {
                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    buffer = new byte[bufferSize];
                    // read file and write it into form...
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                    while (bytesRead > 0) {
                        total_bytes_read += bytesRead;
                        dos.write(buffer, 0, bufferSize);
                        bytesAvailable = fileInputStream.available();
                        bufferSize = Math.min(bytesAvailable, maxBufferSize);
                        bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                    }
                }
                // send multipart form data necesssary after file data...
                dos.writeBytes(lineEnd);
                dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
            }
            try
            {
                inputStream = conn.getInputStream();
            }
            catch (Exception e)
            {
                if(inputStream==null)
                    inputStream=conn.getErrorStream();
            }
            response = convertStreamToString(inputStream);
            if (fileInputStream != null)
                fileInputStream.close();
            dos.flush();
            dos.close();
            Log.e("Himanshu","Dixit Upload API Response"+response);
            return response;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }


    public String doJoinProgramSubmit(String url_update,JoinProgramModel model)
    {
        String response = null;
        HttpURLConnection conn = null;
        DataOutputStream dos = null;
        InputStream inputStream = null;
        String existingFileName = "";
        FileInputStream fileInputStream = null;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        int maxBufferSize = 1024;
        String urlString = url_update.replaceAll(" ", "%20");
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        long size = 0, total_bytes_read = 0;

        try {
            //------------------ CLIENT REQUEST

            // open a URL connection to the Servlet
            URL url = new URL(urlString);
            conn = (HttpURLConnection) url.openConnection();
            // Allow Inputs
            conn.setDoInput(true);
            // Allow Outputs
            conn.setDoOutput(true);
            // Don't use a cached copy.
            conn.setUseCaches(false);
            // Use a post method.
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Connection", "Keep-Alive");
            conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
            dos = new DataOutputStream(conn.getOutputStream());

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"action\"" + lineEnd + lineEnd);
            dos.writeBytes("updateProgramData"+lineEnd);

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"ref_Id\"" + lineEnd + lineEnd);
            dos.writeBytes(model.getRef_ID()+lineEnd);

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"passphase\"" + lineEnd + lineEnd);
            dos.writeBytes(Constants.PASSPHRASE+lineEnd);

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"auth\"" + lineEnd + lineEnd);
            dos.writeBytes(Constants.AUTHKEY+lineEnd);


            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"lat\"" + lineEnd + lineEnd);
            dos.writeBytes(""+model.getLatitude()+lineEnd);

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"long\"" + lineEnd + lineEnd);
            dos.writeBytes(""+model.getLongitude()+lineEnd);

            if(model.getImageList()!=null && model.getImageList().size()>0) {
                ArrayList<String> media_file = model.getImageList();
                for (int i = 0; i < media_file.size(); i++) {
                    if (!TextUtils.isEmpty(media_file.get(i))) {
                        existingFileName = media_file.get(i).substring(media_file.get(i).lastIndexOf('/') + 1, media_file.get(i).length());
                        File file = new File(media_file.get(i));
                        size = file.length();
                        fileInputStream = new FileInputStream(file);
                    }

                    dos.writeBytes(twoHyphens + boundary + lineEnd);
                    dos.writeBytes("Content-Disposition:form-data; name=\"image" + i + "\";filename=\"" + existingFileName + "\"" + lineEnd);
                    dos.writeBytes(lineEnd);
                    // create a buffer of maximum size
                    if (fileInputStream != null) {
                        bytesAvailable = fileInputStream.available();
                        bufferSize = Math.min(bytesAvailable, maxBufferSize);
                        buffer = new byte[bufferSize];
                        // read file and write it into form...
                        bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                        while (bytesRead > 0) {
                            total_bytes_read += bytesRead;
                            dos.write(buffer, 0, bufferSize);
                            bytesAvailable = fileInputStream.available();
                            bufferSize = Math.min(bytesAvailable, maxBufferSize);
                            bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                        }
                    }
                    // send multipart form data necesssary after file data...
                    dos.writeBytes(lineEnd);
                    dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
                }
            }
            try
            {
                inputStream = conn.getInputStream();
            }
            catch (Exception e)
            {
                if(inputStream==null)
                    inputStream=conn.getErrorStream();
            }
            response = convertStreamToString(inputStream);
            if (fileInputStream != null)
                fileInputStream.close();
            dos.flush();
            dos.close();
            Log.e("Himanshu","Dixit Join Program Submit API Response"+response);
            return response;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }


    public String doJoinProgramSubmitTracker(String url_update,JoinProgramModel model,String selectedValue)
    {
        String response = null;
        HttpURLConnection conn = null;
        DataOutputStream dos = null;
        InputStream inputStream = null;
        String existingFileName = "";
        FileInputStream fileInputStream = null;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        int maxBufferSize = 1024;
        String urlString = url_update.replaceAll(" ", "%20");
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        long size = 0, total_bytes_read = 0;

        try {
            //------------------ CLIENT REQUEST

            // open a URL connection to the Servlet
            URL url = new URL(urlString);
            conn = (HttpURLConnection) url.openConnection();
            // Allow Inputs
            conn.setDoInput(true);
            // Allow Outputs
            conn.setDoOutput(true);
            // Don't use a cached copy.
            conn.setUseCaches(false);
            // Use a post method.
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Connection", "Keep-Alive");
            conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
            dos = new DataOutputStream(conn.getOutputStream());

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"action\"" + lineEnd + lineEnd);
            dos.writeBytes("updateProgramData"+lineEnd);

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"ref_Id\"" + lineEnd + lineEnd);
            dos.writeBytes(model.getRef_ID()+lineEnd);

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"passphase\"" + lineEnd + lineEnd);
            dos.writeBytes(Constants.PASSPHRASE+lineEnd);

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"auth\"" + lineEnd + lineEnd);
            dos.writeBytes(Constants.AUTHKEY+lineEnd);


            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"lat\"" + lineEnd + lineEnd);
            dos.writeBytes(""+model.getLatitude()+lineEnd);

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"long\"" + lineEnd + lineEnd);
            dos.writeBytes(""+model.getLongitude()+lineEnd);

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"survey_result3\"" + lineEnd + lineEnd);
            dos.writeBytes(selectedValue+lineEnd);

            if(model.getImageList()!=null && model.getImageList().size()>0) {
                ArrayList<String> media_file = model.getImageList();
                for (int i = 0; i < media_file.size(); i++) {
                    if (!TextUtils.isEmpty(media_file.get(i))) {
                        existingFileName = media_file.get(i).substring(media_file.get(i).lastIndexOf('/') + 1, media_file.get(i).length());
                        File file = new File(media_file.get(i));
                        size = file.length();
                        fileInputStream = new FileInputStream(file);
                    }

                    dos.writeBytes(twoHyphens + boundary + lineEnd);
                    dos.writeBytes("Content-Disposition:form-data; name=\"image" + i + "\";filename=\"" + existingFileName + "\"" + lineEnd);
                    dos.writeBytes(lineEnd);
                    // create a buffer of maximum size
                    if (fileInputStream != null) {
                        bytesAvailable = fileInputStream.available();
                        bufferSize = Math.min(bytesAvailable, maxBufferSize);
                        buffer = new byte[bufferSize];
                        // read file and write it into form...
                        bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                        while (bytesRead > 0) {
                            total_bytes_read += bytesRead;
                            dos.write(buffer, 0, bufferSize);
                            bytesAvailable = fileInputStream.available();
                            bufferSize = Math.min(bytesAvailable, maxBufferSize);
                            bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                        }
                    }
                    // send multipart form data necesssary after file data...
                    dos.writeBytes(lineEnd);
                    dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
                }
            }
            try
            {
                inputStream = conn.getInputStream();
            }
            catch (Exception e)
            {
                if(inputStream==null)
                    inputStream=conn.getErrorStream();
            }
            response = convertStreamToString(inputStream);
            if (fileInputStream != null)
                fileInputStream.close();
            dos.flush();
            dos.close();
            Log.e("Himanshu","Dixit Join Program Submit API Response"+response);
            return response;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }


    public String doJoinProgramSubmitCorel(String url_update,JoinProgramModel model,String selectedValue)
    {
        String response = null;
        HttpURLConnection conn = null;
        DataOutputStream dos = null;
        InputStream inputStream = null;
        String existingFileName = "";
        FileInputStream fileInputStream = null;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        int maxBufferSize = 1024;
        String urlString = url_update.replaceAll(" ", "%20");
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        long size = 0, total_bytes_read = 0;

        try {
            //------------------ CLIENT REQUEST

            // open a URL connection to the Servlet
            URL url = new URL(urlString);
            conn = (HttpURLConnection) url.openConnection();
            // Allow Inputs
            conn.setDoInput(true);
            // Allow Outputs
            conn.setDoOutput(true);
            // Don't use a cached copy.
            conn.setUseCaches(false);
            // Use a post method.
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Connection", "Keep-Alive");
            conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
            dos = new DataOutputStream(conn.getOutputStream());

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"action\"" + lineEnd + lineEnd);
            dos.writeBytes("updateProgramData"+lineEnd);

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"ref_Id\"" + lineEnd + lineEnd);
            dos.writeBytes(model.getRef_ID()+lineEnd);

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"passphase\"" + lineEnd + lineEnd);
            dos.writeBytes(Constants.PASSPHRASE+lineEnd);

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"auth\"" + lineEnd + lineEnd);
            dos.writeBytes(Constants.AUTHKEY+lineEnd);


            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"lat\"" + lineEnd + lineEnd);
            dos.writeBytes(""+model.getLatitude()+lineEnd);

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"long\"" + lineEnd + lineEnd);
            dos.writeBytes(""+model.getLongitude()+lineEnd);

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"survey_result1\"" + lineEnd + lineEnd);
            dos.writeBytes(selectedValue+lineEnd);

            if(model.getImageList()!=null && model.getImageList().size()>0) {
                ArrayList<String> media_file = model.getImageList();
                for (int i = 0; i < media_file.size(); i++) {
                    if (!TextUtils.isEmpty(media_file.get(i))) {
                        existingFileName = media_file.get(i).substring(media_file.get(i).lastIndexOf('/') + 1, media_file.get(i).length());
                        File file = new File(media_file.get(i));
                        size = file.length();
                        fileInputStream = new FileInputStream(file);
                    }

                    dos.writeBytes(twoHyphens + boundary + lineEnd);
                    dos.writeBytes("Content-Disposition:form-data; name=\"image" + i + "\";filename=\"" + existingFileName + "\"" + lineEnd);
                    dos.writeBytes(lineEnd);
                    // create a buffer of maximum size
                    if (fileInputStream != null) {
                        bytesAvailable = fileInputStream.available();
                        bufferSize = Math.min(bytesAvailable, maxBufferSize);
                        buffer = new byte[bufferSize];
                        // read file and write it into form...
                        bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                        while (bytesRead > 0) {
                            total_bytes_read += bytesRead;
                            dos.write(buffer, 0, bufferSize);
                            bytesAvailable = fileInputStream.available();
                            bufferSize = Math.min(bytesAvailable, maxBufferSize);
                            bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                        }
                    }
                    // send multipart form data necesssary after file data...
                    dos.writeBytes(lineEnd);
                    dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
                }
            }
            try
            {
                inputStream = conn.getInputStream();
            }
            catch (Exception e)
            {
                if(inputStream==null)
                    inputStream=conn.getErrorStream();
            }
            response = convertStreamToString(inputStream);
            if (fileInputStream != null)
                fileInputStream.close();
            dos.flush();
            dos.close();
            Log.e("Himanshu","Dixit Join Program Submit API Response"+response);
            return response;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }


    public String doJoinProgramSubmitTree(String url_update,JoinProgramModel model,String selectedValue)
    {
        String response = null;
        HttpURLConnection conn = null;
        DataOutputStream dos = null;
        InputStream inputStream = null;
        String existingFileName = "";
        FileInputStream fileInputStream = null;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        int maxBufferSize = 1024;
        String urlString = url_update.replaceAll(" ", "%20");
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        long size = 0, total_bytes_read = 0;

        try {
            //------------------ CLIENT REQUEST

            // open a URL connection to the Servlet
            URL url = new URL(urlString);
            conn = (HttpURLConnection) url.openConnection();
            // Allow Inputs
            conn.setDoInput(true);
            // Allow Outputs
            conn.setDoOutput(true);
            // Don't use a cached copy.
            conn.setUseCaches(false);
            // Use a post method.
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Connection", "Keep-Alive");
            conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
            dos = new DataOutputStream(conn.getOutputStream());

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"action\"" + lineEnd + lineEnd);
            dos.writeBytes("updateProgramData"+lineEnd);

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"ref_Id\"" + lineEnd + lineEnd);
            dos.writeBytes(model.getRef_ID()+lineEnd);

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"passphase\"" + lineEnd + lineEnd);
            dos.writeBytes(Constants.PASSPHRASE+lineEnd);

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"auth\"" + lineEnd + lineEnd);
            dos.writeBytes(Constants.AUTHKEY+lineEnd);


            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"lat\"" + lineEnd + lineEnd);
            dos.writeBytes(""+model.getLatitude()+lineEnd);

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"long\"" + lineEnd + lineEnd);
            dos.writeBytes(""+model.getLongitude()+lineEnd);

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"survey_result2\"" + lineEnd + lineEnd);
            dos.writeBytes(selectedValue+lineEnd);

            if(model.getImageList()!=null && model.getImageList().size()>0) {
                ArrayList<String> media_file = model.getImageList();
                for (int i = 0; i < media_file.size(); i++) {
                    if (!TextUtils.isEmpty(media_file.get(i))) {
                        existingFileName = media_file.get(i).substring(media_file.get(i).lastIndexOf('/') + 1, media_file.get(i).length());
                        File file = new File(media_file.get(i));
                        size = file.length();
                        fileInputStream = new FileInputStream(file);
                    }

                    dos.writeBytes(twoHyphens + boundary + lineEnd);
                    dos.writeBytes("Content-Disposition:form-data; name=\"image" + i + "\";filename=\"" + existingFileName + "\"" + lineEnd);
                    dos.writeBytes(lineEnd);
                    // create a buffer of maximum size
                    if (fileInputStream != null) {
                        bytesAvailable = fileInputStream.available();
                        bufferSize = Math.min(bytesAvailable, maxBufferSize);
                        buffer = new byte[bufferSize];
                        // read file and write it into form...
                        bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                        while (bytesRead > 0) {
                            total_bytes_read += bytesRead;
                            dos.write(buffer, 0, bufferSize);
                            bytesAvailable = fileInputStream.available();
                            bufferSize = Math.min(bytesAvailable, maxBufferSize);
                            bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                        }
                    }
                    // send multipart form data necesssary after file data...
                    dos.writeBytes(lineEnd);
                    dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
                }
            }
            try
            {
                inputStream = conn.getInputStream();
            }
            catch (Exception e)
            {
                if(inputStream==null)
                    inputStream=conn.getErrorStream();
            }
            response = convertStreamToString(inputStream);
            if (fileInputStream != null)
                fileInputStream.close();
            dos.flush();
            dos.close();
            Log.e("Himanshu","Dixit Join Program Submit API Response"+response);
            return response;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }


    public String doJoinProgramSubmitIndoor(String url_update,JoinProgramModel model,String selectedValue)
    {
        String response = null;
        HttpURLConnection conn = null;
        DataOutputStream dos = null;
        InputStream inputStream = null;
        String existingFileName = "";
        FileInputStream fileInputStream = null;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        int maxBufferSize = 1024;
        String urlString = url_update.replaceAll(" ", "%20");
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        long size = 0, total_bytes_read = 0;

        try {
            //------------------ CLIENT REQUEST

            // open a URL connection to the Servlet
            URL url = new URL(urlString);
            conn = (HttpURLConnection) url.openConnection();
            // Allow Inputs
            conn.setDoInput(true);
            // Allow Outputs
            conn.setDoOutput(true);
            // Don't use a cached copy.
            conn.setUseCaches(false);
            // Use a post method.
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Connection", "Keep-Alive");
            conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
            dos = new DataOutputStream(conn.getOutputStream());

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"action\"" + lineEnd + lineEnd);
            dos.writeBytes("updateProgramData"+lineEnd);

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"ref_Id\"" + lineEnd + lineEnd);
            dos.writeBytes(model.getRef_ID()+lineEnd);

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"passphase\"" + lineEnd + lineEnd);
            dos.writeBytes(Constants.PASSPHRASE+lineEnd);

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"auth\"" + lineEnd + lineEnd);
            dos.writeBytes(Constants.AUTHKEY+lineEnd);


            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"lat\"" + lineEnd + lineEnd);
            dos.writeBytes(""+model.getLatitude()+lineEnd);

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"long\"" + lineEnd + lineEnd);
            dos.writeBytes(""+model.getLongitude()+lineEnd);

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"survey_result3\"" + lineEnd + lineEnd);
            dos.writeBytes(selectedValue+lineEnd);

            if(model.getImageList()!=null && model.getImageList().size()>0) {
                ArrayList<String> media_file = model.getImageList();
                for (int i = 0; i < media_file.size(); i++) {
                    if (!TextUtils.isEmpty(media_file.get(i))) {
                        existingFileName = media_file.get(i).substring(media_file.get(i).lastIndexOf('/') + 1, media_file.get(i).length());
                        File file = new File(media_file.get(i));
                        size = file.length();
                        fileInputStream = new FileInputStream(file);
                    }

                    dos.writeBytes(twoHyphens + boundary + lineEnd);
                    dos.writeBytes("Content-Disposition:form-data; name=\"image" + i + "\";filename=\"" + existingFileName + "\"" + lineEnd);
                    dos.writeBytes(lineEnd);
                    // create a buffer of maximum size
                    if (fileInputStream != null) {
                        bytesAvailable = fileInputStream.available();
                        bufferSize = Math.min(bytesAvailable, maxBufferSize);
                        buffer = new byte[bufferSize];
                        // read file and write it into form...
                        bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                        while (bytesRead > 0) {
                            total_bytes_read += bytesRead;
                            dos.write(buffer, 0, bufferSize);
                            bytesAvailable = fileInputStream.available();
                            bufferSize = Math.min(bytesAvailable, maxBufferSize);
                            bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                        }
                    }
                    // send multipart form data necesssary after file data...
                    dos.writeBytes(lineEnd);
                    dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
                }
            }
            try
            {
                inputStream = conn.getInputStream();
            }
            catch (Exception e)
            {
                if(inputStream==null)
                    inputStream=conn.getErrorStream();
            }
            response = convertStreamToString(inputStream);
            if (fileInputStream != null)
                fileInputStream.close();
            dos.flush();
            dos.close();
            Log.e("Himanshu","Dixit Join Program Submit API Response"+response);
            return response;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }


    public String doAudioSubmit(String url_update,String userId,String fileName)
    {
        String response = null;
        HttpURLConnection conn = null;
        DataOutputStream dos = null;
        InputStream inputStream = null;
        String existingFileName = "";
        FileInputStream fileInputStream = null;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        int maxBufferSize = 1024;
        String urlString = url_update.replaceAll(" ", "%20");
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        long size = 0, total_bytes_read = 0;

        try {
            //------------------ CLIENT REQUEST

            // open a URL connection to the Servlet
            URL url = new URL(urlString);
            conn = (HttpURLConnection) url.openConnection();
            // Allow Inputs
            conn.setDoInput(true);
            // Allow Outputs
            conn.setDoOutput(true);
            // Don't use a cached copy.
            conn.setUseCaches(false);
            // Use a post method.
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Connection", "Keep-Alive");
            conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
            dos = new DataOutputStream(conn.getOutputStream());

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"action\"" + lineEnd + lineEnd);
            dos.writeBytes("uploadVoiceData"+lineEnd);

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"user_id\"" + lineEnd + lineEnd);
            dos.writeBytes(userId+lineEnd);

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"passphase\"" + lineEnd + lineEnd);
            dos.writeBytes(Constants.PASSPHRASE+lineEnd);

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"auth\"" + lineEnd + lineEnd);
            dos.writeBytes(Constants.AUTHKEY+lineEnd);

            if (!TextUtils.isEmpty(fileName)) {
                existingFileName = fileName.substring(fileName.lastIndexOf('/') + 1,fileName.length());
                File file = new File(fileName);
                size = file.length();
                fileInputStream = new FileInputStream(file);
            }

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition:form-data; name=\"voiceclip\";filename=\"" + existingFileName + "\"" + lineEnd);
            dos.writeBytes(lineEnd);
            // create a buffer of maximum size
            if (fileInputStream != null)
            {
                bytesAvailable = fileInputStream.available();
                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                buffer = new byte[bufferSize];
                // read file and write it into form...
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                while (bytesRead > 0) {
                    total_bytes_read += bytesRead;
                    dos.write(buffer, 0, bufferSize);
                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                }
            }
            // send multipart form data necesssary after file data...
            dos.writeBytes(lineEnd);
            dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

            try
            {
                inputStream = conn.getInputStream();
            }
            catch (Exception e)
            {
                if(inputStream==null)
                    inputStream=conn.getErrorStream();
            }
            response = convertStreamToString(inputStream);
            if (fileInputStream != null)
                fileInputStream.close();
            dos.flush();
            dos.close();
            Log.e("Himanshu","Dixit Join Program Submit API Response"+response);
            return response;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }


    public String doJoinProgramSubmitSound(String url_update,JoinProgramModel model,String source,String noiseLevel,String testTime,String selectedValue)
    {
        String response = null;
        HttpURLConnection conn = null;
        DataOutputStream dos = null;
        InputStream inputStream = null;
        String existingFileName = "";
        FileInputStream fileInputStream = null;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        int maxBufferSize = 1024;
        String urlString = url_update.replaceAll(" ", "%20");
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        long size = 0, total_bytes_read = 0;

        try {
            //------------------ CLIENT REQUEST

            // open a URL connection to the Servlet
            URL url = new URL(urlString);
            conn = (HttpURLConnection) url.openConnection();
            // Allow Inputs
            conn.setDoInput(true);
            // Allow Outputs
            conn.setDoOutput(true);
            // Don't use a cached copy.
            conn.setUseCaches(false);
            // Use a post method.
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Connection", "Keep-Alive");
            conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
            dos = new DataOutputStream(conn.getOutputStream());

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"action\"" + lineEnd + lineEnd);
            dos.writeBytes("updateProgramData"+lineEnd);

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"ref_Id\"" + lineEnd + lineEnd);
            dos.writeBytes(model.getRef_ID()+lineEnd);

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"passphase\"" + lineEnd + lineEnd);
            dos.writeBytes(Constants.PASSPHRASE+lineEnd);

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"auth\"" + lineEnd + lineEnd);
            dos.writeBytes(Constants.AUTHKEY+lineEnd);


            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"lat\"" + lineEnd + lineEnd);
            dos.writeBytes(""+model.getLatitude()+lineEnd);

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"long\"" + lineEnd + lineEnd);
            dos.writeBytes(""+model.getLongitude()+lineEnd);

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"test_result_para2\"" + lineEnd + lineEnd);
            dos.writeBytes(source+lineEnd);

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"test_result_para1\"" + lineEnd + lineEnd);
            dos.writeBytes(noiseLevel+lineEnd);

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"test_result_para3\"" + lineEnd + lineEnd);
            dos.writeBytes(testTime+lineEnd);

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"test_result_para4\"" + lineEnd + lineEnd);
            dos.writeBytes(selectedValue+lineEnd);


            if(model.getImageList()!=null && model.getImageList().size()>0) {
                ArrayList<String> media_file = model.getImageList();
                for (int i = 0; i < media_file.size(); i++) {
                    if (!TextUtils.isEmpty(media_file.get(i))) {
                        existingFileName = media_file.get(i).substring(media_file.get(i).lastIndexOf('/') + 1, media_file.get(i).length());
                        File file = new File(media_file.get(i));
                        size = file.length();
                        fileInputStream = new FileInputStream(file);
                    }

                    dos.writeBytes(twoHyphens + boundary + lineEnd);
                    dos.writeBytes("Content-Disposition:form-data; name=\"image" + i + "\";filename=\"" + existingFileName + "\"" + lineEnd);
                    dos.writeBytes(lineEnd);
                    // create a buffer of maximum size
                    if (fileInputStream != null) {
                        bytesAvailable = fileInputStream.available();
                        bufferSize = Math.min(bytesAvailable, maxBufferSize);
                        buffer = new byte[bufferSize];
                        // read file and write it into form...
                        bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                        while (bytesRead > 0) {
                            total_bytes_read += bytesRead;
                            dos.write(buffer, 0, bufferSize);
                            bytesAvailable = fileInputStream.available();
                            bufferSize = Math.min(bytesAvailable, maxBufferSize);
                            bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                        }
                    }
                    // send multipart form data necesssary after file data...
                    dos.writeBytes(lineEnd);
                    dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
                }
            }
            try
            {
                inputStream = conn.getInputStream();
            }
            catch (Exception e)
            {
                if(inputStream==null)
                    inputStream=conn.getErrorStream();
            }
            response = convertStreamToString(inputStream);
            if (fileInputStream != null)
                fileInputStream.close();
            dos.flush();
            dos.close();
            Log.e("Himanshu","Dixit Join Program Submit API Response"+response);
            return response;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }


    private String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
}
