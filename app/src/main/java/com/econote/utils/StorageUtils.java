package com.econote.utils;

import android.os.Environment;

public class StorageUtils {
	private static final String AUDIO_FILE_NAME = "econoteaudiorecordtest.wav";
	private static final String VIDEO_FILE_NAME = "econotevideorecordtest.3gp";
	
	public static boolean checkExternalStorageAvailable() {
		String state = Environment.getExternalStorageState();
		if (Environment.MEDIA_MOUNTED.equals(state)) {
			return true;
	    }
		else {
			return false;
		}
	}
	
	public static String getFileName(boolean isAudio)
	{
		String storageDir = Environment.getExternalStorageDirectory().getAbsolutePath();
		return String.format("%s/%s", storageDir, (isAudio) ? AUDIO_FILE_NAME : VIDEO_FILE_NAME);
	}
}
