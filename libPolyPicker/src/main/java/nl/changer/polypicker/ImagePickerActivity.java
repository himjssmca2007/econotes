
package nl.changer.polypicker;

import java.util.HashSet;
import java.util.Set;

import nl.changer.polypicker.model.Image;
import nl.changer.polypicker.utils.ImageInternalFetcher;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class ImagePickerActivity extends AppCompatActivity{

	private static final String TAG = ImagePickerActivity.class.getSimpleName();

	/***
	 * Returns the parcelled image uris in the intent with this extra.
	 */
	public static final String EXTRA_IMAGE_URIS = "nl.changer.polypicker.extra.selected_image_uris";

	/***
	 * Integer extra to limit the number of images that can be selected. By default the user can
	 * select infinite number of images.
	 */
	public static final String EXTRA_SELECTION_LIMIT = "nl.changer.polypicker.extra.selection_limit";

	private Set<Image> mSelectedImages;
	private LinearLayout mSelectedImagesContainer;
	private TextView mSelectedImageEmptyMessage;
	private ViewPager mViewPager;
	public ImageInternalFetcher mImageFetcher;

	private Button mCancelButtonView, mDoneButtonView;

	private int mMaxSelectionsAllowed = Integer.MAX_VALUE;
	private Toolbar toolbar;
	TabLayout tabs;
	SectionsPagerAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main_pp);
		toolbar=(Toolbar)findViewById(R.id.toolbar);

		mSelectedImagesContainer = (LinearLayout) findViewById(R.id.selected_photos_container);
		mSelectedImageEmptyMessage = (TextView) findViewById(R.id.selected_photos_empty);
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mCancelButtonView = (Button) findViewById(R.id.action_btn_cancel);
		mDoneButtonView = (Button) findViewById(R.id.action_btn_done);

		mSelectedImages = new HashSet<Image>();
		mImageFetcher = new ImageInternalFetcher(this, 500);
		mCancelButtonView.setOnClickListener(onFinishGettingImages);
		mDoneButtonView.setOnClickListener(onFinishGettingImages);

		mMaxSelectionsAllowed = getIntent().getIntExtra(EXTRA_SELECTION_LIMIT, Integer.MAX_VALUE);

		setupActionBar(toolbar);
		tabs = (TabLayout)findViewById(R.id.mc_tabs);
		adapter = new SectionsPagerAdapter(getSupportFragmentManager());
		mViewPager.setAdapter(adapter);
		tabs.setupWithViewPager(mViewPager);
		tabs.post(new Runnable() {
			@Override
			public void run() {
				pageChangeListener.onPageSelected(0);
			}
		});
	}

	/**
	 * Sets up the action bar, adding view page indicator.
	 ***/
	private void setupActionBar(Toolbar toolbar)
	{
		toolbar.setTitle("Choose Images");
		setSupportActionBar(toolbar);
		ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setDisplayShowHomeEnabled(true);
		actionBar.setDisplayShowTitleEnabled(true);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				this.finish();
				return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public boolean addImage(Image image)
	{

		if (mSelectedImages.size() == mMaxSelectionsAllowed)
		{
			Toast.makeText(this, mMaxSelectionsAllowed + " images selected already", Toast.LENGTH_SHORT).show();
			return false;
		} else
		{
			if (mSelectedImages.add(image))
			{
				View rootView = LayoutInflater.from(ImagePickerActivity.this).inflate(R.layout.list_item_selected_thumbnail, null);
				ImageView thumbnail = (ImageView) rootView.findViewById(R.id.selected_photo);
				rootView.setTag(image.mUri);
				mImageFetcher.loadImage(image.mUri, thumbnail);
				mSelectedImagesContainer.addView(rootView, 0);

				int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 60, getResources().getDisplayMetrics());
				thumbnail.setLayoutParams(new FrameLayout.LayoutParams(px, px));

				if (mSelectedImages.size() >= 1) {
					mSelectedImagesContainer.setVisibility(View.VISIBLE);
					mSelectedImageEmptyMessage.setVisibility(View.GONE);
				}
				return true;
			}
		}

		return false;
	}

	public boolean removeImage(Image image) {
		if (mSelectedImages.remove(image)) {
			for (int i = 0; i < mSelectedImagesContainer.getChildCount(); i++) {
				View childView = mSelectedImagesContainer.getChildAt(i);
				if (childView.getTag().equals(image.mUri)) {
					mSelectedImagesContainer.removeViewAt(i);
					break;
				}
			}

			if (mSelectedImages.size() == 0) {
				mSelectedImagesContainer.setVisibility(View.GONE);
				mSelectedImageEmptyMessage.setVisibility(View.VISIBLE);
			}
			return true;
		}
		return false;
	}

	public boolean containsImage(Image image) {
		return mSelectedImages.contains(image);
	}

	private View.OnClickListener onFinishGettingImages = new View.OnClickListener() {

		@Override
		public void onClick(View view) {
			if (view.getId() == R.id.action_btn_done) {

				Uri[] uris = new Uri[mSelectedImages.size()];
				int i = 0;
				for (Image img : mSelectedImages) {
					uris[i++] = img.mUri;
				}

				Intent intent = new Intent();
				intent.putExtra(EXTRA_IMAGE_URIS, uris);
				setResult(Activity.RESULT_OK, intent);
			} else if (view.getId() == R.id.action_btn_cancel) {
				setResult(Activity.RESULT_CANCELED);
			}
			finish();
		}
	};



	class SectionsPagerAdapter extends FragmentStatePagerAdapter {
		int NumbOfTabs; // Store the number of tabs, this will also be passed when the ViewPagerAdapter is created

		// Build a Constructor and assign the passed Values to appropriate values in the class
		public SectionsPagerAdapter(FragmentManager fm) {
			super(fm);
			this.NumbOfTabs=2;
		}

		//This method return the fragment for the every position in the View Pager
		@Override
		public Fragment getItem(int position)
		{
			switch (position) {
				case 0:
					return new CameraFragment();
				case 1:
					return new GalleryFragment();
				default:
					return null;
			}
		}

		// This method return the titles for the Tabs in the Tab Strip

		@Override
		public CharSequence getPageTitle(int position) {

			switch (position) {
				case 0:
					return getString(R.string.take_photo);
				case 1:
					return getString(R.string.gallery);
			}
			return null;
		}

		// This method return the Number of tabs for the tabs Strip

		@Override
		public int getCount()
		{
			return NumbOfTabs;
		}
	}


	public ViewPager.OnPageChangeListener pageChangeListener = new ViewPager.OnPageChangeListener() {

		@Override
		public void onPageSelected(int position)
		{

		}

		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onPageScrollStateChanged(int arg0) {
			// TODO Auto-generated method stub

		}
	};
}
